# SCENARIO ( Système de téléphonie mobile )
yes
APPLICATION -> Dédié à 1 opérateur (ex : prefix : 034..., et name="telma")

operator(

    id_operator,
    prefix unique,
    operator_name unique
);

BACKOFFICE:

A propos de l'application : on peut changer l'opérateur de l'application de TELMA vers ORANGE par exemple

<ins>*INFORMATIONS AND DEFAULT COSTS*</ins>

informations(

    id_operator
);

default_costs(

    call_cost_faf, // Friends and family 0.5Ar/s
    call_cost_operator, // Vers Telma 1 Ar/s
    call_cost_other_operator, // Vers autres opérateur 3Ar/s
    call_cost_international, // A l'internationale 10Ar/s
    free_message_per_day, // Nombre de message gratuit par jour : 1
    message_cost, // 1message -> 256 caracteres : 100ar , si 257 caracteres : 100ar*2 (Tarif de 2 messages)
    internet_cost, // 1mo -> 50Ar (Si pas d'offres)
);



## ACHAT OFFRES : Divisé en 2 types (avec des limites)
Notions d'offres prioritaires comme faceboobaka si facebook , et yellow 100 si autre site

ACHAT OFFRE VIA MOBILE MONEY ou SOLDE PRINCIPALE : CheckBox
### Offres pour les appels et Messages: 
- Telma Mora : 15min d'appel vers tout opérateur - Nationale (décompté à la seconde), 1jour, 500Ariary + 20Mo (Net) ou 20 Messages
- Mora Internationale : 15min, 5000Ar

### Offres pour surfer sur le net : 
(si null: valable pendant toute la journée)
- Faceboobaka : 1go, 3jour, 500Ariary, 'www.facebook.com', null, 2 fois par semaine
- Stay In : 1go, 3jour, 500Ariary, 'www.facebook.com;www.instagram.com', null, 2 fois par semaine
- Yellow 100 : 20mo ou 20messages, 1jour, 100Ariary, '*', null, 10 fois par jour
- One Night : 40Mo, 1jour, 100Ariary, '*', 21:00 à 06:00, 4 fois par jour

offer(

    id_offer,
    offer_name, 
    cost, // En Ariary
    validity, // En jours
    call, // e.g : 15  = 900 s, décompté à la seconde -> Mm si autres opérateurs (Num FAF , tjrs 0.5Ar/s)
    internet, // En Mo
    message, // En nb_message
    allowed_website, // e.g: 'www.facebook.com;www.instagram' and for all website it is '*'
    frequency, // e.g : 10 fois par jour
    availability, // e.g: Night => 21:00 à 06:00, nullable value (if null available all the day)

);

account_offer(

    id_account_offer,
    id_account,
    id_offer,
    remaining_call, // Appel restant , e.g : 5 min = 300 s
    remaining_internet, // Internet restant , 7Mo
    remaining_message, // Nb Message restant, 7messages
    purchase_date, // Date d'achat de l'offre
    expiration_date default purchase_date::timestamp + offer.validity, // Date d'expiration de l'offre
);

=> Insertion account_offert + Débiter le solde du compte principale ou celui du Mobile money

=> NB: Frequency mbola erirteretina, ilay oe 10fois par jour, select count(*) from account_offer where id_offer='1' and id_account='1' si Inf ou égale à offer.frequency donc La personne peut acheter l'offre

## ECRAN DE SIMULATION : Divisé en 3 blocs
### Appel
- En entrée: Combobox opérateur(Telma, Orange ... ? Facultatif ou déterminé à partir du préfixe du numéro), numéro du destinataire, Durée de l'appel(en seconde), Checkbox : A l'internationale (si Coché, tarif internationale sinon tarif normale), Date de l'appel
- Notification crédit
### Message
- En entrée : Combobox opérateur(Telma, Orange ...? Facultatif ou déterminé à partir du préfixe du numéro), numéro du destinataire, Nombre de caractère, Date de l'envoie du message
- Premier message du jour (Configurable 0, 2, ou 3 ou pls...): Offert par l'opérateur, Notif : Ce message vous a été offert par l'opérateur
- Notification crédit
### Internet (Internet usage)
- En entrée : lien du site, Consommation de donnée(en mo), Date
- Si pas d'offre internet : C'est le crédit qui est soutiré
- Notification reste OFFRE

call(

    id_call,
    id_account, // The phone number who call
    recipient_phone_number, // Le numéro du destinataire
    duration, // En seconde
    date, // Date de fin de l'appel
    is_international // Boolean

);

message(

    id_message,
    id_account, // The phone number who send the message
    recipient_phone_number, // Le numéro du destinataire
    message_body, 
    
    date, // Date de l'envoie du message
);

internet_usage(

    id_internet_usage,
    id_account, // The phone number who surf on the net
    website, // Le site visité
    consommation, // En Mo
    date, // Date d'insertion de la consommation
);

=> Insertion des simulations et Décrementation account_offer ou bien le solde du compte principal(Si pas d'offres).


## ECRAN POUR LES NUMEROS FAMILLES (FAF - Friends And Family): (5 numéros max, Ce n'est pas obligatoire s'il ne sont pas du même opérateur) => Tarif famille Appel 0.5Ar/s
### Liste des Numéros familles( + Action Supprimer => Avec confirmation de la suppression)
### Ajout d'un numéro famille
- En entrée : Numéro de la personne

faf_phone_number(

    id_faf_phone_number,
    id_account,
    faf_phone_number
);

## MON COMPTE
### Rechargement du compte:
- En entrée : Montant du crédit, Date de rechargement
- Notification : Votre solde à été bien crédité de 1000 Ariary. Votre solde actuel est de 2300 Ariary.
### Consultation du solde de mon compte principale
- Votre solde principale est de : 1000 Ariary
- Votre numéro est le 0328840802
### Consultation de mes offres / Info consommation (Par offres, order by offer.expiration_date)
- Yellow 100 : Il vous reste 8Mo d'internet valable jusqu'au 10/09/2021 à 10:00
- Telma Mora : Il vous reste 2000Ariary d'appel valable jusqu'au 10/09/2021 à 20:00 

account_balance(

    id_account_balance,
    id_account,
    account_balance, // Solde du compte en Ariary
    last_update
);

## MOBILE MONEY
### Voir le solde de mon compte
### Faire un depot (à valider chez le backoffice)
- Entrée : Montant du depot, Date du dépot
- Après validation du backoffice : (Notification) Le depot de 100000Ariary sur votre compte a été effectué avec succès. Votre solde actuel est de 4500000Ariary.

mobile_money_account(

    id_mobile_money_account,
    id_account,
    account_balance // Solde du compte, NORMALISATION (Pour ne pas faire à chaque fois select)
);

mobile_money_transaction_type(
    
    id_mobile_money_transaction_type,
    mobile_money_transaction_type // Type Depot ou retrait
);


mobile_money_transaction(

    id_mobile_money_transaction,
    id_mobile_money_account,
    id_mobile_money_transaction_type, // Type Depot ou retrait
    amount, // Montant de la transaction
    transaction_date, // Date de la transaction
    is_validated default false
);

=> Insertion mobile_money_transaction -> Debiter ou crediter mobile_money_account.account_balance (Normalisation)

## Login (Utile pour acheter des offres, Voir son compte)
- Entrer son Numero et son mot de passe
- Se connecter -> Avoir un token -> Stocker dans localstorage pour que l'utilisateur ne se log pas souvent-> Durée expiration du token -> 30jours.

account (

    id_account,
    phone_number unique,
    password
);

account_token(

    id_account_token,
    token,
    expiration_date default current_timestamp::timestamp + integer '30'
);


## BACKOFFICE
- Gestion des éléments necessaire : OFFER(CRUD)
- Tableau statistiques :
    . COMBOBOX - CHOIX ANNEE
    . 1 Graphe (pour les appels /mois/an): Appel Global + Appel internationales + Appel vers autres opérateurs + Appel vers mon opérateur => (Nombre d'appels passés et Durée total des appels par mois)
    . 1 Graphe (pour les messages /mois/an) => (Nombres de messages envoyés + Nombre total de caractères dans le corps du message par mois)
    . 1 Graphe (pour la concommatuion de l'internet) => (Somme de la consommation de l'internet par mois)

- Validation depot mobile money : 
    . Liste des depots en attentes (Non validés)
    . Bouton Rejeter / Valider

## FRONT OFFICE :
- Historique des appels : Liste + Recherche Numero Appelant et appelé à la date du
