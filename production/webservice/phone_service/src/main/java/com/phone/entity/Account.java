package com.phone.entity;

import com.phone.utils.DBUtils;
import com.phone.utils.Helper;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.xml.bind.DatatypeConverter;

public class Account {

    private int idAccount;
    private String name;
    private String cin;
    private Date birthDate;
    private String phoneNumber;
    private String password;
    private String fcm_token;

    public Account(int id_account) {
        this.setIdAccount(id_account);
    }

    public Account() {

    }

    public Account(String phoneNumber, String password) {
        this.setPhoneNumber(phoneNumber);
        this.setPassword(password);
    }

    public Account(int idAccount, String name, String cin, Date birthDate, String phoneNumber, String password) {
        this.idAccount = idAccount;
        this.name = name;
        this.cin = cin;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhoneNumber() throws Exception {
        return Helper.formatNumber(phoneNumber);
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PhoneNumber extraxtPhoneNumber() throws Exception {
        return new PhoneNumber(this.phoneNumber);
    }

    @Override
    public String toString() {
        return "Account{" + "idAccount=" + idAccount + ", name=" + name + ", cin=" + cin + ", birthDate=" + birthDate + ", phoneNumber=" + phoneNumber + ", password=" + password + '}';
    }

    public static Account findById(Connection conn, int id) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Account account = null;

        try {
            ps = conn.prepareStatement("SELECT * FROM Account where id_account=?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                account = new Account();
                account.setIdAccount(rs.getInt("id_account"));
                account.setCin(rs.getString("cin"));
                account.setBirthDate(rs.getDate("birth_date"));
                account.setName(rs.getString("name"));
                account.setPhoneNumber(rs.getString("phone_number"));
                account.setPassword(rs.getString("password"));
            }
            return account;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

    }

    public static Account findById(int id) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findById(conn, id);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<Account> findAll(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Account> lsAccount = new ArrayList<>();

        try {
            ps = conn.prepareStatement("SELECT * FROM Account");
            rs = ps.executeQuery();
            while (rs.next()) {
                Account tmp = new Account();
                tmp.setIdAccount(rs.getInt("id_account"));
                tmp.setCin(rs.getString("cin"));
                tmp.setBirthDate(rs.getDate("birth_date"));
                tmp.setName(rs.getString("name"));
                tmp.setPhoneNumber(rs.getString("phone_number"));
                tmp.setPassword(rs.getString("password"));
                lsAccount.add(tmp);
            }
            return lsAccount;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

    }

    public static ArrayList<Account> findAll() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findAll(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public String signup() throws Exception {

        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.signup(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public String signup(Connection conn) throws Exception {
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        PreparedStatement ps2 = null;

        ResultSet rs = null;
        String token = null;
        try {
            conn.setAutoCommit(false);
            
            ps = conn.prepareStatement("insert into account(name, birth_date, cin, phone_number, password) values (?, ?, ?, ?, md5(?)) returning id_account");
            ps.setString(1, this.getName());
            ps.setDate(2, this.getBirthDate());
            ps.setString(3, this.getCin());
            ps.setString(4, this.phoneNumber);
            ps.setString(5, this.getPassword());
            rs = ps.executeQuery();
            rs.next();
            
            int id_account = rs.getInt("id_account");
            ps1 = conn.prepareStatement("insert into account_balance(id_account, account_balance) values (?, 0)");
            ps1.setInt(1, id_account);
            ps1.executeUpdate();
            
            ps2 = conn.prepareStatement("insert into mobile_money_account(id_account, account_balance, secret_code) values (?, 0, md5('0000'))");
            ps2.setInt(1, id_account);
            ps2.executeUpdate();
            
            token = Authentification.insertAccountToken(conn, id_account);
            
            conn.commit();
            return token;
        } catch (Exception ex) {
            conn.rollback();
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (ps1 != null) {
                ps1.close();
            }
            if (ps2 != null) {
                ps2.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    public MobileMoneyAccount findMobileMoneyAccount() throws Exception {
        return MobileMoneyAccount.findByIdAccount(this.getIdAccount());
    }

    public MobileMoneyAccount findMobileMoneyAccount(Connection conn) throws Exception {
        return MobileMoneyAccount.findByIdAccount(conn, this.getIdAccount());
    }

    public AccountBalance findAccountBalance() throws Exception {
        return AccountBalance.findByIdAccount(this.getIdAccount());
    }

    public AccountBalance findAccountBalance(Connection conn) throws Exception {
        return AccountBalance.findByIdAccount(conn, this.getIdAccount());
    }

    public ArrayList<AccountOffer> findCurrentOffers() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findCurrentOffers(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void purchaseOffer(int idOffer, boolean isWithMobileMoney, String secretCode) {
        throw new UnsupportedOperationException("Not defined yet.");

    }


    public ArrayList<AccountOffer> findCurrentOffers(Connection conn) {
        throw new UnsupportedOperationException("Not defined yet.");
    }

    public String login() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return login(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public String login(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String token = null;
        try {
            conn.setAutoCommit(false);
            ps = conn.prepareStatement("SELECT id_account FROM Account where phone_number=? and password = md5(?)");
            ps.setString(1, phoneNumber);

            ps.setString(2, this.password);

            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new Exception("L'identifiant ou le mot de passe est incorrect.");
            } else {
                int id_account = rs.getInt("id_account");
                token = Authentification.insertAccountToken(conn, id_account);
                
                if(this.getFcm_token() != null){
                    FCMUser fcm_user = new FCMUser(id_account, this.getFcm_token());
                    fcm_user.save(conn);
                }
            }
            conn.commit();
            return token;
        } catch (Exception ex) {
            conn.rollback();
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

    }

    public static void logout(String token) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            logout(token, conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static void logout(String token, Connection conn) throws Exception {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("delete from account_token where token=?");
            ps.setString(1, token);
            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }

    }

}
