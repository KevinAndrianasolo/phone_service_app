/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Pricing {
    private int id_pricing;
    private double call_cost_faf;
    private double call_cost_operator;
    private double call_cost_other_operator;
    private double call_cost_international;
    private double message_cost;
    private double internet_cost;
    private double default_cost;
    public Pricing(){
        
    }
    public Pricing(int id_pricing, double call_cost_faf, double call_cost_operator, double call_cost_other_operator, double call_cost_international, double message_cost, double internet_cost) {
        this.id_pricing = id_pricing;
        this.call_cost_faf = call_cost_faf;
        this.call_cost_operator = call_cost_operator;
        this.call_cost_other_operator = call_cost_other_operator;
        this.call_cost_international = call_cost_international;
        this.message_cost = message_cost;
        this.internet_cost = internet_cost;
    }

    public double getDefault_cost() {
        return default_cost;
    }

    public void setDefault_cost(double default_cost) {
        this.default_cost = default_cost;
    }

    public int getId_pricing() {
        return id_pricing;
    }

    public void setId_pricing(int id_pricing) {
        this.id_pricing = id_pricing;
    }

    public double getCall_cost_faf() {
        return call_cost_faf;
    }

    public void setCall_cost_faf(double call_cost_faf) {
        this.call_cost_faf = call_cost_faf;
    }

    public double getCall_cost_operator() {
        return call_cost_operator;
    }

    public void setCall_cost_operator(double call_cost_operator) {
        this.call_cost_operator = call_cost_operator;
    }

    public double getCall_cost_other_operator() {
        return call_cost_other_operator;
    }

    public void setCall_cost_other_operator(double call_cost_other_operator) {
        this.call_cost_other_operator = call_cost_other_operator;
    }

    public double getCall_cost_international() {
        return call_cost_international;
    }

    public void setCall_cost_international(double call_cost_international) {
        this.call_cost_international = call_cost_international;
    }

    public double getMessage_cost() {
        return message_cost;
    }

    public void setMessage_cost(double message_cost) {
        this.message_cost = message_cost;
    }

    public double getInternet_cost() {
        return internet_cost;
    }

    public void setInternet_cost(double internet_cost) {
        this.internet_cost = internet_cost;
    }

    @Override
    public String toString() {
        return "Pricing{" + "id_pricing=" + id_pricing + ", call_cost_faf=" + call_cost_faf + ", call_cost_operator=" + call_cost_operator + ", call_cost_other_operator=" + call_cost_other_operator + ", call_cost_international=" + call_cost_international + ", message_cost=" + message_cost + ", internet_cost=" + internet_cost + ", default_cost=" + default_cost + '}';
    }

    
     public boolean verify() throws Exception{
        if(call_cost_faf<0) throw new Exception("Appel vers FAF est invalide.");
        
        if(call_cost_operator<0) throw new Exception("Appel vers l'opérateur est invalide.");
        
        if(call_cost_other_operator<0) throw new Exception("Appel vers les autres opérateurs est invalide.");
        
        if(call_cost_international<0) throw new Exception("Appel à l'international est invalide.");
        
        if(message_cost<0) throw new Exception("Le cout du message est invalide.");
        
        if(internet_cost<0) throw new Exception("Le cout de l'internet est invalide.");
        
        if(default_cost<0) throw new Exception("Le cout par defaut est invalide.");
        return true;
    }
    public static void save(Pricing pricing) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            save(conn, pricing);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static void save(Connection conn, Pricing pricing) throws Exception {
        pricing.verify();
        PreparedStatement ps = null;
        try {
            conn.setAutoCommit(false);
            ps = conn.prepareStatement("insert into  pricing( call_cost_faf, call_cost_operator, call_cost_other_operator, call_cost_international, message_cost, internet_cost, default_cost) values (?, ?, ? ,? , ?, ?, ?)");
            ps.setDouble(1, pricing.getCall_cost_faf());
            ps.setDouble(2, pricing.getCall_cost_operator());
            ps.setDouble(3, pricing.getCall_cost_other_operator());
            ps.setDouble(4, pricing.getCall_cost_international());
            ps.setDouble(5, pricing.getMessage_cost());
            ps.setDouble(6, pricing.getInternet_cost());
            ps.setDouble(7, pricing.getDefault_cost());

            ps.executeUpdate();
            conn.commit();
        } catch (Exception ex) {
            conn.rollback();
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
            
    public static Pricing findCurrentPricing() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findCurrentPricing(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static Pricing findCurrentPricing(Connection conn) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Pricing pricing = null;

        try {
            ps = conn.prepareStatement("SELECT id_pricing, call_cost_faf, call_cost_operator, call_cost_other_operator, call_cost_international, message_cost, internet_cost, default_cost FROM CurrentPricing");
            rs = ps.executeQuery();
            if (rs.next()) {
                pricing = new Pricing();
                pricing.setId_pricing(rs.getInt("id_pricing"));
                pricing.setCall_cost_faf(rs.getDouble("call_cost_faf"));
                pricing.setCall_cost_operator(rs.getDouble("call_cost_operator"));
                pricing.setCall_cost_other_operator(rs.getDouble("call_cost_other_operator"));
                pricing.setCall_cost_international(rs.getDouble("call_cost_international"));
                pricing.setMessage_cost(rs.getDouble("message_cost"));
                pricing.setInternet_cost(rs.getDouble("internet_cost"));
                pricing.setDefault_cost(rs.getDouble("default_cost"));
            }
            return pricing;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    

}
