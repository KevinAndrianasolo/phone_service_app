package com.phone.entity;

public class TransactionStatus {

    public static int PENDING = 0;
    public static int REJECTED = -1;
    public static int VALIDATED = 1;
    private int status;

    public TransactionStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescription() {
        if (this.getStatus() == PENDING) {
            return "En attente";
        } else if (this.getStatus() == REJECTED) {
            return "Refusé";
        } else {
            return "Validé";
        }
    }

}
