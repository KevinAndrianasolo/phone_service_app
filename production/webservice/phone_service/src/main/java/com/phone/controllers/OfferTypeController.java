/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.controllers;

import com.phone.entity.Offer;
import com.phone.entity.OfferType;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author admin
 */

@CrossOrigin(origins = "*")
@RestController
public class OfferTypeController {
    @GetMapping("/OfferType")
    public Response findAll(){
        try{
            List<OfferType> offer_type = OfferType.findAll();
            return ResponseBuilder.Success(offer_type);
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
}
