/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author admin
 */
public class Consumption {
    private int id_consumption;
    private int id_account;
    private int id_account_offer;
    private int id_offer_type;
    private double value;
    private int id_unit;

    public Consumption() {
    }

    public Consumption(int id_account, int id_account_offer, int id_offer_type, double value, int id_unit) {
        this.id_account = id_account;
        this.id_account_offer = id_account_offer;
        this.id_offer_type = id_offer_type;
        this.value = value;
        this.id_unit = id_unit;
    }

    public int getId_consumption() {
        return id_consumption;
    }

    public void setId_consumption(int id_consumption) {
        this.id_consumption = id_consumption;
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public int getId_account_offer() {
        return id_account_offer;
    }

    public void setId_account_offer(int id_account_offer) {
        this.id_account_offer = id_account_offer;
    }

    public int getId_offer_type() {
        return id_offer_type;
    }

    public void setId_offer_type(int id_offer_type) {
        this.id_offer_type = id_offer_type;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getId_unit() {
        return id_unit;
    }

    public void setId_unit(int id_unit) {
        this.id_unit = id_unit;
    }

    @Override
    public String toString() {
        return "Consumption{" + "id_consumption=" + id_consumption + ", id_account=" + id_account + ", id_account_offer=" + id_account_offer + ", id_offer_type=" + id_offer_type + ", value=" + value + ", id_unit=" + id_unit + '}';
    }

    

    public void save() throws Exception {

        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            this.save(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void save(Connection conn) throws Exception {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("insert into consumption(id_account, id_account_offer, id_offer_type, value, id_unit, consumption_date) values (?, ?, ?, ?, ?, CURRENT_TIMESTAMP)");
            ps.setInt(1, this.getId_account());
            ps.setInt(2, this.getId_account_offer());
            ps.setDouble(3, this.getId_offer_type());
            ps.setDouble(4, this.getValue());
            ps.setInt(5, this.getId_unit());
            ps.executeUpdate();
            
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public Account getAccount(Connection conn) throws Exception {
        return Account.findById(conn, this.getId_account());
    }
    
    public Account getAccount() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return getAccount(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
}
