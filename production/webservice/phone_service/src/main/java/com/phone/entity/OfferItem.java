/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class OfferItem {
    private int id_offer_item;
    private int id_offer;
    private int id_offer_type;
    private double value;
    private String unit;
    private String offer_type_description = "";
    public OfferItem(){
        
    }
    public OfferItem(int id_offer_item, int id_offer,int id_offer_type, double value, String unit) {
        this.id_offer_item = id_offer_item;
        this.id_offer = id_offer;
        this.id_offer_type = id_offer_type;
        this.value = value;
        this.unit = unit;
    }

    public String getOffer_type_description() {
        return offer_type_description;
    }

    public void setOffer_type_description(String offer_type_description) {
        this.offer_type_description = offer_type_description;
    }

    public int getId_offer() {
        return id_offer;
    }

    public void setId_offer(int id_offer) {
        this.id_offer = id_offer;
    }

    public int getId_offer_item() {
        return id_offer_item;
    }

    public void setId_offer_item(int id_offer_item) {
        this.id_offer_item = id_offer_item;
    }

    public int getId_offer_type() {
        return id_offer_type;
    }

    public void setId_offer_type(int id_offer_type) {
        this.id_offer_type = id_offer_type;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value){
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit){
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "OfferItem{" + "id_offer_item=" + id_offer_item + ", id_offer_type=" + id_offer_type + ", value=" + value + ", unit=" + unit + '}';
    }
    
    public boolean verify() throws Exception{
        if(id_offer_type<=0) throw new Exception("Le type de l'offre est invalide.");
        
        if(value<0) throw new Exception("La valeur de l'item offre est invalide.");
        
        if(unit==null || unit.compareTo("")==0) throw new Exception("L'unité de l'item offre est invalide.");
        
        return true;
    }
    public static ArrayList<OfferItem> findOfferItemsOf(int id_offer, Connection conn) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<OfferItem> ls = new ArrayList<OfferItem>();
        try {
            ps = conn.prepareStatement("SELECT * FROM offer_item_details where id_offer = ?");
            ps.setInt(1, id_offer);
            
            rs = ps.executeQuery();
            while (rs.next()) {
                OfferItem tmp = new OfferItem();
                tmp.setId_offer_item(rs.getInt("id_offer_item"));
                tmp.setId_offer(rs.getInt("id_offer"));
                tmp.setId_offer_type(rs.getInt("id_offer_type"));
                tmp.setValue(rs.getDouble("value"));
                tmp.setUnit(rs.getString("unit"));
                tmp.setOffer_type_description(rs.getString("offer_type_description"));
                ls.add(tmp);
            }
            return ls;

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public static void save(int id_offer, OfferItem offer_item, Connection conn) throws Exception {
        offer_item.verify();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("insert into offer_item (id_offer, id_offer_type, value, unit) values (?, ?, ? ,?)");
            ps.setInt(1, id_offer);
            ps.setInt(2, offer_item.getId_offer_type());
            ps.setDouble(3, offer_item.getValue());
            ps.setString(4, offer_item.getUnit());
            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public static void update(int id,OfferItem offer_item, Connection conn) throws Exception {
        offer_item.verify();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("update offer_item set id_offer=?, id_offer_type=?, value=?, unit=? where id_offer_item=?");
            
            ps.setInt(1, offer_item.getId_offer());
            ps.setInt(2, offer_item.getId_offer_type());
            ps.setDouble(3, offer_item.getValue());
            ps.setString(4, offer_item.getUnit());
            ps.setInt(5, offer_item.getId_offer_item());

            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public static void delete(int id_offer, Connection conn) throws Exception {
        
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("delete from offer_item where id_offer=?");
            ps.setInt(1, id_offer);

            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
}
