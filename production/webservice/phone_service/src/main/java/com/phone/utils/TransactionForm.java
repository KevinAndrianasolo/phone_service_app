package com.phone.utils;

public class TransactionForm {

    private int id_account;
    private double amount;

    public TransactionForm() {
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "TransactionForm{" + "idAccount=" + id_account + ", amount=" + amount + '}';
    }

}
