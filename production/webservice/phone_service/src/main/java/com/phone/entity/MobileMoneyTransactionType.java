/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class MobileMoneyTransactionType {
    private int id_mobile_money_transaction_type;
    private String mobile_money_transaction_type;

    public MobileMoneyTransactionType() {
    }

    public MobileMoneyTransactionType(int id_mobile_money_transaction_type, String mobile_money_transaction_type) {
        this.id_mobile_money_transaction_type = id_mobile_money_transaction_type;
        this.mobile_money_transaction_type = mobile_money_transaction_type;
    }

    public int getId_mobile_money_transaction_type() {
        return id_mobile_money_transaction_type;
    }

    public void setId_mobile_money_transaction_type(int id_mobile_money_transaction_type) {
        this.id_mobile_money_transaction_type = id_mobile_money_transaction_type;
    }

    public String getMobile_money_transaction_type() {
        return mobile_money_transaction_type;
    }

    public void setMobile_money_transaction_type(String mobile_money_transaction_type) {
        this.mobile_money_transaction_type = mobile_money_transaction_type;
    }

    @Override
    public String toString() {
        return "MobileMoneyTransactionType{" + "id_mobile_money_transaction_type=" + id_mobile_money_transaction_type + ", mobile_money_transaction_type=" + mobile_money_transaction_type + '}';
    }
    public static ArrayList<MobileMoneyTransactionType> findAll(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<MobileMoneyTransactionType> ls = new ArrayList<MobileMoneyTransactionType>();
        try {
            ps = conn.prepareStatement("SELECT * FROM mobile_money_transaction_type");
            rs = ps.executeQuery();
            while (rs.next()) {
                MobileMoneyTransactionType tmp = new MobileMoneyTransactionType();
                tmp.setId_mobile_money_transaction_type(rs.getInt("id_mobile_money_transaction_type"));
                tmp.setMobile_money_transaction_type(rs.getString("mobile_money_transaction_type"));
                ls.add(tmp);
            }
            return ls;

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static ArrayList<MobileMoneyTransactionType> findAll() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findAll(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

}
