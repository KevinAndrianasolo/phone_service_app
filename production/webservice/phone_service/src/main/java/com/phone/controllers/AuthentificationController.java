/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.controllers;

import com.phone.entity.Account;
import com.phone.entity.Admin;
import com.phone.entity.Authentification;
import com.phone.exceptions.AuthException;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author admin
 */
@CrossOrigin(origins = "*")
@RestController
public class AuthentificationController {
    @PostMapping(value="/Authentification/Account/Login", consumes="application/json")
    public Response login_account(@RequestBody Account account){
        try{
            String token = account.login();
            return ResponseBuilder.Success(token);
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
    @PostMapping(value="/Authentification/Account/Signup", consumes="application/json")
    public Response signup_account(@RequestBody Account account){
        try{
            String token = account.signup();
            return ResponseBuilder.Success(token);
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
    @PostMapping(value="/Authentification/Account/Logout", consumes="application/json")
    public Response logout_account(@RequestHeader("Authorization") String bearer_token){
        try{
            String token = bearer_token.replace("Bearer ", "");
            Account.logout(token);
            return ResponseBuilder.Success(null);
        }
        catch(AuthException e){
            return ResponseBuilder.Error(e.getError_message(), e.getStatus());
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
    @PostMapping(value="/IsValidToken/Account")
    public Response isValidAccountToken(@RequestHeader("Authorization") String bearer_token){
        try{
            String token = bearer_token.replace("Bearer ", "");
            int id_account = Authentification.findIdAccountWithToken(token);
            if(id_account == -1){
                throw new AuthException("Session expirée, accès interdit.");
            }
            return ResponseBuilder.Success(id_account);
        }
        catch(AuthException e){
            return ResponseBuilder.Error(e.getError_message(), e.getStatus());
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
    @PostMapping(value="/Authentification/Admin/Login", consumes="application/json")
    public Response login_admin(@RequestBody Admin admin){
        try{
            String token = admin.login();
            return ResponseBuilder.Success(token);
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
    @PostMapping(value="/Authentification/Admin/Logout", consumes="application/json")
    public Response logout_admin(@RequestHeader("Authorization") String bearer_token){
        try{
            String token = bearer_token.replace("Bearer ", "");
            Admin.logout(token);
            return ResponseBuilder.Success(null);
        }
        catch(AuthException e){
            return ResponseBuilder.Error(e.getError_message(), e.getStatus());
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
    @PostMapping(value="/IsValidToken/Admin")
    public Response isValidAdminToken(@RequestHeader("Authorization") String bearer_token){
        try{
            String token = bearer_token.replace("Bearer ", "");
            int id_admin = Authentification.findIdAdminWithToken(token);
            if(id_admin == -1){
                throw new AuthException("Session expirée, accès interdit.");
            }
            return ResponseBuilder.Success(id_admin);
        }
        catch(AuthException e){
            return ResponseBuilder.Error(e.getError_message(), e.getStatus());
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }

}
