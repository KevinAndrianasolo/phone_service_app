package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Offer {
    private int id_offer;
    private String offer_name;
    private double cost;
    private int validity;
    private String validity_unit;
    private int priority;
    private int count;
    private OfferPricing offer_pricing = null;
    private ArrayList<OfferItem> offer_items = new ArrayList<OfferItem>();
    public Offer(){
        
    }
    public Offer(int id_offer, String offer_name, double cost, int validity, String validity_unit, int priority) {
        this.id_offer = id_offer;
        this.offer_name = offer_name;
        this.cost = cost;
        this.validity = validity;
        this.validity_unit = validity_unit;
        this.priority = priority;
    }

    public OfferPricing getOffer_pricing() {
        return offer_pricing;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setOffer_pricing(OfferPricing offer_pricing) {
        this.offer_pricing = offer_pricing;
    }

    public ArrayList<OfferItem> getOffer_items() {
        return offer_items;
    }

    public void setOffer_items(ArrayList<OfferItem> offer_items) {
        this.offer_items = offer_items;
    }

    public int getId_offer() {
        return id_offer;
    }

    public void setId_offer(int id_offer) {
        this.id_offer = id_offer;
    }

    public String getOffer_name() {
        return offer_name;
    }

    public void setOffer_name(String offer_name){
        
        this.offer_name = offer_name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost){
        
        this.cost = cost;
    }

    public int getValidity() {
        return validity;
    }

    public void setValidity(int validity){
        
        this.validity = validity;
    }

    public String getValidity_unit() {
        return validity_unit;
    }

    public void setValidity_unit(String validity_unit){
        
        this.validity_unit = validity_unit;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority){
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Offer{" + "id_offer=" + id_offer + ", offer_name=" + offer_name + ", cost=" + cost + ", validity=" + validity + ", validity_unit=" + validity_unit + ", priority=" + priority + '}';
    }
    public boolean verify() throws Exception{
        if(offer_name==null || offer_name.compareTo("")==0) throw new Exception("Le nom de l'offre est invalide.");
        if(cost<0) throw new Exception("Le cout de l'offre est invalide.");
        if(validity<0) throw new Exception("La validité de l'offre est invalide.");
        if(validity_unit==null || validity_unit.compareTo("")==0) throw new Exception("L'unité de la validation de l'offre est invalide.");
        return true;
    }
    public static ArrayList<Offer> findAll(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Offer> ls = new ArrayList<Offer>();
        try {
            ps = conn.prepareStatement("SELECT * FROM Offer");
            rs = ps.executeQuery();
            while (rs.next()) {
                Offer tmp = new Offer();
                tmp.setId_offer(rs.getInt("id_offer"));
                tmp.setOffer_name(rs.getString("offer_name"));
                tmp.setCost(rs.getDouble("cost"));
                tmp.setValidity(rs.getInt("validity"));
                tmp.setValidity_unit(rs.getString("validity_unit"));
                tmp.setPriority(rs.getInt("priority"));
                OfferPricing offer_pricing = OfferPricing.findOfferPricingOf(tmp.getId_offer(), conn);
                tmp.setOffer_pricing(offer_pricing);
                ArrayList<OfferItem> offer_items = OfferItem.findOfferItemsOf(tmp.getId_offer(), conn);
                tmp.setOffer_items(offer_items);
                ls.add(tmp);
            }
            return ls;

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static ArrayList<Offer> findAll() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findAll(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static Offer findById(int id) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findById(conn, id);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static Offer findById(Connection conn, int id) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Offer offer = null;

        try {
            ps = conn.prepareStatement("SELECT * FROM Offer WHERE id_offer = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                offer = new Offer();
                offer.setId_offer(rs.getInt("id_offer"));
                offer.setOffer_name(rs.getString("offer_name"));
                offer.setCost(rs.getDouble("cost"));
                offer.setValidity(rs.getInt("validity"));
                offer.setValidity_unit(rs.getString("validity_unit"));
                offer.setPriority(rs.getInt("priority"));
                OfferPricing offer_pricing = OfferPricing.findOfferPricingOf(id, conn);
                offer.setOffer_pricing(offer_pricing);
                ArrayList<OfferItem> offer_items = OfferItem.findOfferItemsOf(id, conn);
                offer.setOffer_items(offer_items);
            }
            return offer;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public static void save(Offer offer) throws Exception {

        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            save(conn, offer);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static void save(Connection conn, Offer offer) throws Exception {
        offer.verify();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn.setAutoCommit(false);
            
            ps = conn.prepareStatement("insert into offer(offer_name, cost, validity, validity_unit,priority) values (?, ?, ? ,? , ?) returning id_offer");
            ps.setString(1, offer.getOffer_name());
            ps.setDouble(2, offer.getCost());
            ps.setInt(3, offer.getValidity());
            ps.setString(4, offer.getValidity_unit());
            ps.setInt(5, offer.getPriority());
            rs = ps.executeQuery();
            rs.next();
            int id_offer = rs.getInt("id_offer");
            OfferPricing.save(id_offer, offer.getOffer_pricing(), conn);
            for(int i=0 ; i<offer.getOffer_items().size(); i++){
                OfferItem.save(id_offer, offer.getOffer_items().get(i), conn);
            }
            conn.commit();
        } catch (Exception ex) {
            conn.rollback();
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    public static void update(int id, Offer offer) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            update(conn, id, offer);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static void update(Connection conn, int id, Offer offer) throws Exception {
        offer.verify();
        PreparedStatement ps = null;
        try {
            conn.setAutoCommit(false);
            if(offer.getOffer_pricing() != null && offer.getOffer_pricing().getId_offer_pricing() != -1){
                OfferPricing.update(id, offer.getOffer_pricing(), conn);
            }
            else{
                OfferPricing.save(id, offer.getOffer_pricing(), conn);
            }
            OfferItem.delete(id, conn);
            for(int i=0 ; i<offer.getOffer_items().size(); i++){
                OfferItem.save(id, offer.getOffer_items().get(i), conn);
            }
            ps = conn.prepareStatement("UPDATE Offer set offer_name=?, cost=?, validity=?, validity_unit=?,priority=? where id_offer = ?");
            
            ps.setString(1, offer.getOffer_name());
            ps.setDouble(2, offer.getCost());
            ps.setInt(3, offer.getValidity());
            ps.setString(4, offer.getValidity_unit());
            ps.setInt(5, offer.getPriority());
            ps.setInt(6, id);
            ps.executeUpdate();
            conn.commit();
        } catch (Exception ex) {
            conn.rollback();
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public static void delete(int id) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            delete(conn, id);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static void delete(Connection conn, int id) throws Exception {

        PreparedStatement ps = null;
        try {
            conn.setAutoCommit(false);
            Offer offer = findById(conn, id);
            OfferPricing.delete(offer.getOffer_pricing(), conn);
            OfferItem.delete(offer.getId_offer(), conn);

            ps = conn.prepareStatement("delete from offer where id_offer = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
            conn.commit();
        } catch (Exception ex) {
            conn.rollback();
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    
    
}
