/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

/**
 *
 * @author admin
 */
public class ConsumptionData {
    private double remaining_value;
    private double consumption_value;

    public ConsumptionData() {
    }

    public ConsumptionData(double remaining_value, double consumption_value) {
        this.remaining_value = remaining_value;
        this.consumption_value = consumption_value;
    }

    public double getRemaining_value() {
        return remaining_value;
    }

    public void setRemaining_value(double remaining_value) {
        this.remaining_value = remaining_value;
    }

    public double getConsumption_value() {
        return consumption_value;
    }

    public void setConsumption_value(double consumption_value) {
        this.consumption_value = consumption_value;
    }
    
}
