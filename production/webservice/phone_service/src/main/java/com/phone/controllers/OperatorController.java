package com.phone.controllers;

import com.phone.entity.Operator;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OperatorController {

    @GetMapping("/Operators")
    public Response findAll() throws Exception {
        try {
            return ResponseBuilder.Success(Operator.findAll());
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @GetMapping("/Operators/{id_operator}")
    public Response findById(@PathVariable(value = "id_operator") int id) throws Exception {
        try {
            return ResponseBuilder.Success(Operator.findById(id));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @PostMapping("/Operators/Add")
    public int save() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @PutMapping("/Operators/Update")
    public int update(@RequestParam(name = "id_operator") int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @DeleteMapping("/Operators/Delete")
    public int delete(@RequestParam(name = "id_operator") int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
