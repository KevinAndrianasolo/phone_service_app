/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.controllers;

import com.phone.entity.AccountOffer;
import com.phone.entity.AccountOfferDetails;
import com.phone.entity.GlobalConsumption;
import com.phone.entity.SimulationData;
import com.phone.exceptions.AuthException;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author admin
 */
@CrossOrigin(origins = "*")
@RestController
public class AccountOfferController extends BaseController{
    @PostMapping(value="/Consumption/Simulate", consumes="application/json")
    public Response simulate(@RequestBody GlobalConsumption globalConsumption,@RequestHeader("Authorization") String bearer_token){
        try{
            /**
             * Seul un administrateur connecté peut inserer une nouvelle offre
             */
            String token = bearer_token.replace("Bearer ", "");
            int id_account = isValidAccountToken(token); 
            globalConsumption.setId_account(id_account);
            SimulationData datas = globalConsumption.SimulateAndInsert();
            return ResponseBuilder.Success(datas);
        }
        catch(AuthException e){
            return ResponseBuilder.Error(e.getError_message(), e.getStatus());
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
    @GetMapping("/AccountOfferDetails")
    public Response findAll(@RequestHeader("Authorization") String bearer_token){
        try{
            String token = bearer_token.replace("Bearer ", "");
            int id_account = isValidAccountToken(token); 
            
            ArrayList<AccountOfferDetails> account_offers = AccountOfferDetails.findAccountOfferDetailsOf(id_account);
            return ResponseBuilder.Success(account_offers);
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
    @PostMapping(value="/AccountOffer", consumes="application/json")
    public Response save(@RequestBody AccountOffer accountOffer,@RequestHeader("Authorization") String bearer_token){
        try{
            /**
             * Seul un administrateur connecté peut inserer une nouvelle offre
             */
            String token = bearer_token.replace("Bearer ", "");
            int id_account = isValidAccountToken(token); 
            accountOffer.setIdAccount(id_account);
            AccountOffer.save(accountOffer);
            return ResponseBuilder.Success(null);
        }
        catch(AuthException e){
            return ResponseBuilder.Error(e.getError_message(), e.getStatus());
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
}
