/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.exceptions;

/**
 *
 * @author admin
 */
public class AuthException extends Exception{
    private String status = "401"; // Unauthorized
    private String error_message;

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    
    public AuthException(String message){
        this.error_message = message;
    }
}
