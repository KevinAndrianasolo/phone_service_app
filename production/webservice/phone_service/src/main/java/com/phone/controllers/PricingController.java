/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.controllers;

import com.phone.entity.Pricing;
import com.phone.exceptions.AuthException;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import org.bson.Document;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author admin
 */

@CrossOrigin(origins = "*")
@RestController
public class PricingController extends BaseController{
    @PostMapping(value="/Pricing", consumes="application/json")
    public Response save(@RequestBody Pricing pricing,@RequestHeader("Authorization") String bearer_token){
        try{
            /**
             * Seul un administrateur connecté peut changer les tarifs
             */
            String token = bearer_token.replace("Bearer ", "");
            isValidAdminToken(token); 
            Pricing.save(pricing);
            return ResponseBuilder.Success(null);
        }
        catch(AuthException e){
            return ResponseBuilder.Error(e.getError_message(), e.getStatus());
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
    @GetMapping(value="/Pricing")
    public Response findCurrentPricing(){
        try{
            Pricing pricing = Pricing.findCurrentPricing();
            return ResponseBuilder.Success(pricing);
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
}
