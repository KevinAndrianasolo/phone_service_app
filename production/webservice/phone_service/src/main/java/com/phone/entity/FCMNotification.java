/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import org.bson.Document;

/**
 *
 * @author admin
 */
public class FCMNotification {
    private FCMNotificationBody notification;
    private String to;
    private String priority = "high";

    public FCMNotification() {
    }

    public FCMNotification(FCMNotificationBody notification, String to) {
        this.notification = notification;
        this.to = to;
    }

    public FCMNotificationBody getNotification() {
        return notification;
    }

    public void setNotification(FCMNotificationBody notification) {
        this.notification = notification;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "FCMNotification{" + "notification=" + notification + ", to=" + to + ", priority=" + priority + '}';
    }
    public Document toDocument(){
        Document doc = new Document();
        Document notification_body_doc = this.notification.toDocument();
        doc.append("notification", notification_body_doc);
        doc.append("to", this.to);
        doc.append("priority", this.priority);

        return doc;
    }
    
}
