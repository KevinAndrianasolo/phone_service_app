package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Operator {

    private int idOperator;
    private String prefix;
    private String OperatorName;

    public Operator() {
    }

    public int getIdOperator() {
        return idOperator;
    }

    public void setIdOperator(int idOperator) {
        this.idOperator = idOperator;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getOperatorName() {
        return OperatorName;
    }

    public void setOperatorName(String OperatorName) {
        this.OperatorName = OperatorName;
    }

    @Override
    public String toString() {
        return "Operator{" + "idOperator=" + idOperator + ", prefix=" + prefix + ", OperatorName=" + OperatorName + '}';
    }

    public boolean isDefaultOperator(Connection conn) throws Exception {
        Informations infos = Informations.find(conn);
        return infos.getIdOperator() == this.getIdOperator();
    }

    public static ArrayList<Operator> findAll(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Operator> ls = new ArrayList<>();
        try {
            ps = conn.prepareStatement("SELECT * FROM Operator");
            rs = ps.executeQuery();
            while (rs.next()) {
                Operator tmp = new Operator();
                tmp.setIdOperator(rs.getInt("id_operator"));
                tmp.setOperatorName(rs.getString("operator_name"));
                tmp.setPrefix(rs.getString("prefix"));
                ls.add(tmp);
            }
            return ls;

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static ArrayList<Operator> findAll() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findAll(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static Operator findById(int id) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findById(conn, id);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static Operator findById(Connection conn, int id) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Operator operator = new Operator();

        try {
            ps = conn.prepareStatement("SELECT * FROM Operator WHERE id_operator = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                operator.setIdOperator(rs.getInt("id_operator"));
                operator.setOperatorName(rs.getString("operator_name"));
                operator.setPrefix(rs.getString("prefix"));
            }
            return operator;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
}
