package com.phone.controllers;

import com.phone.entity.CallStatistics;
import com.phone.entity.Offer;
import com.phone.entity.OfferStatistics;
import com.phone.exceptions.AuthException;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import java.util.List;
import org.bson.Document;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class OfferController extends BaseController {

    @GetMapping("/Offers")
    public Response findAll() {
        try {
            List<Offer> offers = Offer.findAll();
            return ResponseBuilder.Success(offers);
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @PostMapping(value = "/Offers", consumes = "application/json")
    public Response save(@RequestBody Offer offer, @RequestHeader("Authorization") String bearer_token) {
        try {
            /**
             * Seul un administrateur connecté peut inserer une nouvelle offre
             */
            String token = bearer_token.replace("Bearer ", "");
            isValidAdminToken(token);
            Offer.save(offer);
            return ResponseBuilder.Success(null);
        } catch (AuthException e) {
            return ResponseBuilder.Error(e.getError_message(), e.getStatus());
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @GetMapping(value = "/Offers/{id}")
    public Response findById(@PathVariable(value = "id") int id) {
        try {
            Offer offer = Offer.findById(id);
            return ResponseBuilder.Success(offer);
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }

    }

    @PutMapping(value = "/Offers/{id}", consumes = "application/json")
    public Response update(@PathVariable(value = "id") int id, @RequestBody Offer offer, @RequestHeader("Authorization") String bearer_token) {
        try {
            /**
             * Seul un administrateur connecté peut modifer une offre
             */
            String token = bearer_token.replace("Bearer ", "");
            isValidAdminToken(token);
            Offer.update(id, offer);
            return ResponseBuilder.Success(null);
        } catch (AuthException e) {
            return ResponseBuilder.Error(e.getError_message(), e.getStatus());
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @DeleteMapping(value = "/Offers/{id}")
    public Response delete(@PathVariable(value = "id") int id, @RequestHeader("Authorization") String bearer_token) {
        try {
            /**
             * Seul un administrateur connecté peut supprimer une offre
             */
            String token = bearer_token.replace("Bearer ", "");
            isValidAdminToken(token);
            Offer.delete(id);
            return ResponseBuilder.Success(null);
        } catch (AuthException e) {
            return ResponseBuilder.Error(e.getError_message(), e.getStatus());
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @GetMapping("/Statistics/Offer")
    public Response findStatisticsYear(@RequestParam(name = "year") String year) throws Exception {
        try {
            return ResponseBuilder.Success(OfferStatistics.findOfferStatistics(year));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

}
