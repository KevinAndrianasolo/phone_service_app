/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author admin
 */
public class AccountBalanceTransaction {
    private int id_account_balance;
    private double amount;

    public AccountBalanceTransaction() {
    }

    public int getId_account_balance() {
        return id_account_balance;
    }

    public void setId_account_balance(int id_account_balance) {
        this.id_account_balance = id_account_balance;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "AccountBalanceTransaction{" + "id_account_balance=" + id_account_balance + ", amount=" + amount + '}';
    }
    public boolean verify() throws Exception {
        if(amount<0) throw new Exception("Le montant est invalide.");
        return true;
    }
    public void save(Connection conn, AccountBalance account_balance) throws Exception{
        save(conn, account_balance, true);
    }
    public void save(Connection conn, AccountBalance account_balance, boolean perform_verification) throws Exception {
        if(perform_verification) this.verify();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            double remaining = this.getAmount() + account_balance.getAccountBalance();
            if(remaining<0) throw new Exception("Le solde de votre compte principale est insuffisant. Solde actuel : "+account_balance.getAccountBalance()+" Ariary.");
            
            ps = conn.prepareStatement("INSERT INTO account_balance_transaction(id_account_balance, amount) VALUES (?,?)");
            ps.setInt(1, this.getId_account_balance());
            ps.setDouble(2, this.getAmount());
            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void save(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("INSERT INTO account_balance_transaction(id_account_balance, amount) VALUES (?,?)");
            ps.setInt(1, this.getId_account_balance());
            ps.setDouble(2, this.getAmount());
            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void save(AccountBalance account_balance) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            this.save(conn, account_balance);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

}
