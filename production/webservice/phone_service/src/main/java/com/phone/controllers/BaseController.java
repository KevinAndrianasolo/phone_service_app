/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.controllers;

import com.phone.entity.Authentification;
import com.phone.exceptions.AuthException;

/**
 *
 * @author admin
 */
public class BaseController {
    public int isValidAdminToken(String token) throws AuthException, Exception{
        int id_admin = Authentification.findIdAdminWithToken(token);
        if(id_admin == -1) throw new AuthException("La session est expirée. Veuillez vous reconnecter pour continuer.");
        return id_admin;
    }
    public int isValidAccountToken(String token) throws AuthException, Exception{
        int id_account = Authentification.findIdAccountWithToken(token);
        if(id_account == -1) throw new AuthException("La session est expirée. Veuillez vous reconnecter pour continuer.");
        return id_account;
    }
}
