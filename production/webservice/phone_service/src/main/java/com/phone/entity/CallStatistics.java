package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CallStatistics {

    private int month;
    private String year;
    private double callDuration;

    public CallStatistics() {
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public double getCallDuration() {
        return callDuration;
    }

    public void setCallDuration(double callDuration) {
        this.callDuration = callDuration;
    }

    //CallStats(month, year, count, sum_call_duration)
    public static ArrayList<Call> findAllCallsByIdAccount(int idAccount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findAllCallsByIdAccount(conn, idAccount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<Call> findCalls(int month, String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findCalls(conn, month, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<CallStatistics> findDefaultCallStatistics(String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return CallStatistics.findDefaultCallStatistics(conn, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<CallStatistics> findDefaultCallStatistics(Connection conn, String year) throws Exception {
        ArrayList<CallStatistics> result = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            ArrayList<Call> lsCall = findDefaultOperatorCalls(conn, i + 1, year);
            double duration = 0;
            if (lsCall.size() > 0) {
                duration = lsCall.get(lsCall.size() - 1).getTotalDuration();
            }
            CallStatistics tmp = new CallStatistics();
            tmp.setMonth(i + 1);
            tmp.setCallDuration(duration);
            tmp.setYear(year);
            result.add(tmp);
        }
        return result;
    }

    public static ArrayList<CallStatistics> findOtherCallStatistics(String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return CallStatistics.findOtherCallStatistics(conn, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<CallStatistics> findOtherCallStatistics(Connection conn, String year) throws Exception {
        ArrayList<CallStatistics> result = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            ArrayList<Call> lsCall = findOtherOperatorCalls(conn, i + 1, year);
            double duration = 0;
            if (lsCall.size() > 0) {
                duration = lsCall.get(lsCall.size() - 1).getTotalDuration();
            }
            CallStatistics tmp = new CallStatistics();
            tmp.setMonth(i + 1);
            tmp.setCallDuration(duration);
            tmp.setYear(year);
            result.add(tmp);
        }
        return result;
    }

    public static ArrayList<CallStatistics> findInternationalCallStatistics(String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return CallStatistics.findInternationalCallStatistics(conn, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<CallStatistics> findInternationalCallStatistics(Connection conn, String year) throws Exception {
        ArrayList<CallStatistics> result = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            ArrayList<Call> lsCall = findInternationalCalls(conn, i + 1, year);
             double duration = 0;
            if (lsCall.size() > 0) {
                duration = lsCall.get(lsCall.size() - 1).getTotalDuration();
            }
            CallStatistics tmp = new CallStatistics();
            tmp.setMonth(i + 1);
            tmp.setCallDuration(duration);
            tmp.setYear(year);
            result.add(tmp);
        }
        return result;
    }

    public static ArrayList<Call> findCallsByIdAccount(int idAccount, int month, String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findCallsByIdAccount(conn, idAccount, month, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<Call> findCalls(Connection conn, int month, String year) throws Exception {
        ArrayList<Call> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        double totalDuration = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_call_month_year WHERE month = ? AND year = ?");
            ps.setInt(1, month);
            ps.setInt(2, Integer.parseInt(year));
            res = ps.executeQuery();
            while (res.next()) {
                //Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)
                ls.add(new Call(res.getInt("id_call"), res.getInt("id_account"), res.getString("recipient_phone_number"), res.getDouble("duration"), res.getTimestamp("date"), res.getBoolean("is_international")));
                totalDuration += res.getDouble("duration");
            }
            for (Call c : ls) {
                c.setTotalDuration(totalDuration);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return ls;
    }

    public static ArrayList<Call> findCallsByIdAccount(Connection conn, int idAccount, int month, String year) throws Exception {
        ArrayList<Call> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        double totalDuration = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_call_month_year WHERE id_account = ? AND month = ? AND year = ? order by date desc");
            ps.setInt(1, idAccount);
            ps.setInt(2, month);
            ps.setInt(3, Integer.parseInt(year));
            res = ps.executeQuery();
            while (res.next()) {
                //Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)
                ls.add(new Call(res.getInt("id_call"), res.getInt("id_account"), res.getString("recipient_phone_number"), res.getDouble("duration"), res.getTimestamp("date"), res.getBoolean("is_international")));
                totalDuration += res.getDouble("duration");
            }
            for (Call c : ls) {
                c.setTotalDuration(totalDuration);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return ls;
    }

    public static ArrayList<Call> findAllCallsByIdAccount(Connection conn, int idAccount) throws Exception {
        ArrayList<Call> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        double totalDuration = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_call_month_year WHERE id_account = ?");
            ps.setInt(1, idAccount);
            res = ps.executeQuery();
            while (res.next()) {
                //Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)
                ls.add(new Call(res.getInt("id_call"), res.getInt("id_account"), res.getString("recipient_phone_number"), res.getDouble("duration"), res.getTimestamp("date"), res.getBoolean("is_international")));
                totalDuration += res.getDouble("duration");
            }
            for (Call c : ls) {
                c.setTotalDuration(totalDuration);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return ls;
    }

    public static ArrayList<Call> findDefaultOperatorCallsByIdAccount(int idAccount, int month, String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findDefaultOperatorCallsByIdAccount(conn, idAccount, month, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<Call> findDefaultOperatorCallsByIdAccount(Connection conn, int idAccount, int month, String year) throws Exception {
        ArrayList<Call> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        double totalDuration = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_call_month_year WHERE id_account = ? AND month = ? AND year = ? AND recipient_phone_number LIKE CONCAT((SELECT prefix FROM informations),'%')");

            ps.setInt(1, idAccount);
            ps.setInt(2, month);
            ps.setInt(3, Integer.parseInt(year));
            res = ps.executeQuery();
            while (res.next()) {
                //Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)
                ls.add(new Call(res.getInt("id_call"), res.getInt("id_account"), res.getString("recipient_phone_number"), res.getDouble("duration"), res.getTimestamp("date"), res.getBoolean("is_international")));
                totalDuration += res.getDouble("duration");
            }
            for (Call c : ls) {
                c.setTotalDuration(totalDuration);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return ls;
    }

    public static ArrayList<Call> findDefaultOperatorCalls(int month, String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findDefaultOperatorCalls(conn, month, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<Call> findDefaultOperatorCalls(Connection conn, int month, String year) throws Exception {
        ArrayList<Call> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        double totalDuration = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_call_month_year WHERE  month = ? AND year = ? AND recipient_phone_number LIKE CONCAT((SELECT prefix FROM informations),'%')");

            ps.setInt(1, month);
            ps.setInt(2, Integer.parseInt(year));
            res = ps.executeQuery();
            while (res.next()) {
                //Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)
                ls.add(new Call(res.getInt("id_call"), res.getInt("id_account"), res.getString("recipient_phone_number"), res.getDouble("duration"), res.getTimestamp("date"), res.getBoolean("is_international")));
                totalDuration += res.getDouble("duration");
            }
            for (Call c : ls) {
                c.setTotalDuration(totalDuration);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return ls;
    }

    public static ArrayList<Call> findOtherOperatorCallsByIdAccount(int idAccount, int month, String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findOtherOperatorCallsByIdAccount(conn, idAccount, month, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<Call> findOtherOperatorCallsByIdAccount(Connection conn, int idAccount, int month, String year) throws Exception {
        ArrayList<Call> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        double totalDuration = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_call_month_year WHERE id_account = ? AND month = ? AND year = ? AND is_international = false AND recipient_phone_number NOT LIKE CONCAT((SELECT prefix FROM informations),'%')");

            ps.setInt(1, idAccount);
            ps.setInt(2, month);
            ps.setInt(3, Integer.parseInt(year));
            res = ps.executeQuery();
            while (res.next()) {
                //Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)
                ls.add(new Call(res.getInt("id_call"), res.getInt("id_account"), res.getString("recipient_phone_number"), res.getDouble("duration"), res.getTimestamp("date"), res.getBoolean("is_international")));
                totalDuration += res.getDouble("duration");

            }
            for (Call c : ls) {
                c.setTotalDuration(totalDuration);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return ls;
    }

    public static ArrayList<Call> findOtherOperatorCalls(int month, String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findOtherOperatorCalls(conn, month, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<Call> findOtherOperatorCalls(Connection conn, int month, String year) throws Exception {
        ArrayList<Call> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        double totalDuration = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_call_month_year WHERE  month = ? AND year = ? AND is_international = false AND recipient_phone_number NOT LIKE CONCAT((SELECT prefix FROM informations),'%')");

            ps.setInt(1, month);
            ps.setInt(2, Integer.parseInt(year));
            res = ps.executeQuery();
            while (res.next()) {
                //Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)
                ls.add(new Call(res.getInt("id_call"), res.getInt("id_account"), res.getString("recipient_phone_number"), res.getDouble("duration"), res.getTimestamp("date"), res.getBoolean("is_international")));
                totalDuration += res.getDouble("duration");

            }
            for (Call c : ls) {
                c.setTotalDuration(totalDuration);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return ls;
    }

    public static ArrayList<Call> findInternationalCallsByIdAccount(int idAccount, int month, String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findInternationalCallsByIdAccount(conn, idAccount, month, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<Call> findInternationalCallsByIdAccount(Connection conn, int idAccount, int month, String year) throws Exception {
        ArrayList<Call> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        double totalDuration = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_call_month_year WHERE id_account = ? AND month = ? AND year = ? AND is_international = true");

            ps.setInt(1, idAccount);
            ps.setInt(2, month);
            ps.setInt(3, Integer.parseInt(year));
            res = ps.executeQuery();
            while (res.next()) {
                //Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)
                ls.add(new Call(res.getInt("id_call"), res.getInt("id_account"), res.getString("recipient_phone_number"), res.getDouble("duration"), res.getTimestamp("date"), res.getBoolean("is_international")));
                totalDuration += res.getDouble("duration");
            }
            for (Call c : ls) {
                c.setTotalDuration(totalDuration);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return ls;
    }

    public static ArrayList<Call> findInternationalCalls(int month, String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findInternationalCalls(conn, month, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<Call> findInternationalCalls(Connection conn, int month, String year) throws Exception {
        ArrayList<Call> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        double totalDuration = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_call_month_year WHERE  month = ? AND year = ? AND is_international = true");

            ps.setInt(1, month);
            ps.setInt(2, Integer.parseInt(year));
            res = ps.executeQuery();
            while (res.next()) {
                //Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)
                ls.add(new Call(res.getInt("id_call"), res.getInt("id_account"), res.getString("recipient_phone_number"), res.getDouble("duration"), res.getTimestamp("date"), res.getBoolean("is_international")));
                totalDuration += res.getDouble("duration");
            }
            for (Call c : ls) {
                c.setTotalDuration(totalDuration);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return ls;
    }

    @Override
    public String toString() {
        return "CallStatistics{" + "month=" + month + ", year=" + year + ", sumCallDuration=" + callDuration + '}';
    }

  
}
