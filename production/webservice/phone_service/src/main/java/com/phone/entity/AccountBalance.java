package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class AccountBalance {

    private int idAccountBalance;
    private int idAccount;
    private double accountBalance;

    public AccountBalance() {
    }

    public int getIdAccountBalance() {
        return idAccountBalance;
    }

    public void setIdAccountBalance(int idAccountBalance) {
        this.idAccountBalance = idAccountBalance;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Account getAccount(Connection conn) throws Exception {
        return Account.findById(conn, this.getIdAccount());
    }

    public static AccountBalance findByIdAccount(Connection conn, int idAccount) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        AccountBalance accountBalance = null;

        try {
            ps = conn.prepareStatement("SELECT * FROM V_AccoutBalance WHERE id_account = ?");
            ps.setInt(1, idAccount);
            rs = ps.executeQuery();
            while (rs.next()) {
                accountBalance = new AccountBalance();
                accountBalance.setIdAccountBalance(rs.getInt("id_account_balance"));
                accountBalance.setAccountBalance(rs.getDouble("account_balance"));
                accountBalance.setIdAccount(rs.getInt("id_account"));
                accountBalance.setIdAccount(idAccount);

            }
            return accountBalance;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static AccountBalance findByIdAccount(int idAccount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findByIdAccount(conn, idAccount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }


    public Account getAccount() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return getAccount(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public double withDraw(double amount) {
        double overflow = 0;
        if (amount >= this.getAccountBalance()) {
            overflow = amount - this.getAccountBalance();
            this.setAccountBalance(0);
        } else {
            this.setAccountBalance(this.getAccountBalance() - amount);
        }
        return overflow;
    }

    public int update() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.update(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int update(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("UPDATE account_balance SET account_balance = ? WHERE idAccountBalance = ?");
            ps.setDouble(1, this.getAccountBalance());
            ps.setInt(2, this.getIdAccountBalance());
            return ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public int deposit(double amount) throws Exception {

        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.deposit(conn, amount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int deposit(Connection conn, double amount) throws Exception {
        if (amount < 0) {
            throw new Exception("Le montant de dépot ne peut pas être négatif");
        }
        double currentAmount = this.getAccountBalance();
        this.setAccountBalance(currentAmount + amount);

        return this.update();
    }
}
