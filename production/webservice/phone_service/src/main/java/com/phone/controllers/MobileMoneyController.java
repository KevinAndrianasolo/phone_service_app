package com.phone.controllers;

import com.phone.entity.Account;
import com.phone.entity.MobileMoneyAccount;
import com.phone.entity.MobileMoneyTransaction;
import com.phone.entity.MobileMoneyTransactionType;
import com.phone.entity.Offer;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import com.phone.utils.TransactionForm;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class MobileMoneyController extends BaseController{
    @GetMapping(value = "/MobileMoneyAccount")
    public Response findMobileMoneyAccount(@RequestHeader("Authorization") String bearer_token) throws Exception {
        try {
            String token = bearer_token.replace("Bearer ", "");
            int id_account = isValidAccountToken(token); 
            Account account = new Account(id_account);
            MobileMoneyAccount mobile_money_account = account.findMobileMoneyAccount();
            if(mobile_money_account==null) throw new Exception("Vous n'avez pas encore de compte Mobile money.");
            return ResponseBuilder.Success(mobile_money_account);
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }
    @GetMapping("/MobileMoneyTransactionType")
    public Response findAll(){
        try{
            List<MobileMoneyTransactionType> mobileMoneyTransactionType = MobileMoneyTransactionType.findAll();
            return ResponseBuilder.Success(mobileMoneyTransactionType);
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
    @PostMapping(value = "/MobileMoneyTransaction", consumes = "application/json")
    public Response saveMobileMoneyTransaction(@RequestBody MobileMoneyTransaction mobileMoneyTransaction,@RequestHeader("Authorization") String bearer_token) throws Exception {
        try {
            String token = bearer_token.replace("Bearer ", "");
            int id_account = isValidAccountToken(token); 
            Account account = new Account(id_account);
            MobileMoneyAccount mobile_money_account = account.findMobileMoneyAccount();
            if(mobile_money_account==null) throw new Exception("Vous n'avez pas encore de compte Mobile money.");
            int id_mobile_money_account = mobile_money_account.getIdMobileMoneyAccount();
            mobileMoneyTransaction.setIdMobileMoneyAccount(id_mobile_money_account);
            mobileMoneyTransaction.save(mobile_money_account);
            return ResponseBuilder.Success(null);
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }
    @GetMapping("/MobileMoney/{id_account}")
    public Response findAll(@PathVariable(value = "id_account") int id) throws Exception {
        try {
            return ResponseBuilder.Success(MobileMoneyAccount.findByIdAccount(id));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @GetMapping("/MobileMoney/Transactions")
    public Response findTransactions() throws Exception {
        try {
            return ResponseBuilder.Success(MobileMoneyTransaction.findAll());
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @GetMapping("/MobileMoney/{id_account}/Transactions")
    public Response findTransactionsById(@PathVariable(value = "id_account") int id) throws Exception {
        try {
            return ResponseBuilder.Success(MobileMoneyTransaction.findTransactionsByIdAccount(id));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }
        @GetMapping("/MobileMoney/PendingTransactions")
    public Response findPendingTransactions() throws Exception {
        try {
            return ResponseBuilder.Success(MobileMoneyTransaction.findPendingTransactions());
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @PostMapping(value = "/MobileMoney/Deposit", consumes = "application/json")
    public Response deposit(@RequestBody TransactionForm transactionForm) throws Exception {
        try {
            MobileMoneyAccount mobileMoney = MobileMoneyAccount.findByIdAccount(transactionForm.getId_account());
            return ResponseBuilder.Success(mobileMoney.depositTransaction(transactionForm.getAmount()));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @PostMapping(value = "/MobileMoney/Withdraw", consumes = "application/json")
    public Response withdraw(@RequestBody TransactionForm transactionForm) throws Exception {
        try {
            MobileMoneyAccount mobileMoney = MobileMoneyAccount.findByIdAccount(transactionForm.getId_account());
            return ResponseBuilder.Success(mobileMoney.withdrawTransaction(transactionForm.getAmount()));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @PostMapping(value = "/MobileMoney/Transaction/Accept", consumes = "application/json")
    public Response acceptTransaction(@RequestBody MobileMoneyTransaction mobileMoneyTransaction,@RequestHeader("Authorization") String bearer_token) throws Exception {
        try {
            String token = bearer_token.replace("Bearer ", "");
            int id_admin = isValidAdminToken(token); 
            mobileMoneyTransaction.accept();
            
            return ResponseBuilder.Success(null);
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @PostMapping( value = "/MobileMoney/Transaction/Decline")
    public Response declineTransaction(@RequestBody MobileMoneyTransaction mobileMoneyTransaction, @RequestHeader("Authorization") String bearer_token) throws Exception {
        try {
            String token = bearer_token.replace("Bearer ", "");
            int id_admin = isValidAdminToken(token); 
            mobileMoneyTransaction.decline();
            
            return ResponseBuilder.Success(null);
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @DeleteMapping("/MobileMoneyController/Delete")
    public int delete(@RequestParam int id) {
        return -1;
    }
}
