/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import org.bson.Document;

/**
 *
 * @author admin
 */
public class FCMNotificationBody {
    private String title;
    private String body;
    private String sound = "default";
    private String click_action = "FCM_PLUGIN_ACTIVITY";
    private String icon = "fcm_push_icon";

    public FCMNotificationBody() {
    }

    public FCMNotificationBody(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getClick_action() {
        return click_action;
    }

    public void setClick_action(String click_action) {
        this.click_action = click_action;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "FCMBody{" + "title=" + title + ", body=" + body + ", sound=" + sound + ", click_action=" + click_action + ", icon=" + icon + '}';
    }
    
    public Document toDocument(){
        Document doc = new Document();
        doc.append("title", this.title);
        doc.append("body", this.body);
        doc.append("sound", this.sound);
        doc.append("click_action", this.click_action);
        doc.append("icon", this.icon);



        return doc;
    }
}
