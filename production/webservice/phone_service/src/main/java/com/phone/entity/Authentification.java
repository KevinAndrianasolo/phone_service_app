/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author admin
 */
public class Authentification {
    
    public static String insertAccountToken(int id_account) throws Exception{
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return insertAccountToken(conn, id_account);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public static String insertAccountToken(Connection conn, int id_account) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("INSERT INTO account_token (id_account, token, expiration_date) values (?,md5(concat(current_timestamp,' ',?)),current_timestamp + interval '30 day') returning token");
            ps.setInt(1, id_account);
            ps.setInt(2, id_account);
            rs = ps.executeQuery();
            rs.next();
            String token = rs.getString("token");
            return token;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

    }
    
    public static int findIdAccountWithToken(String token) throws Exception{
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findIdAccountWithToken(conn, token);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public static int findIdAccountWithToken(Connection conn, String token) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id_account = -1;
        try {
            ps = conn.prepareStatement("SELECT * from account_token where token=? and expiration_date>=current_timestamp");
            ps.setString(1, token);

            rs = ps.executeQuery();
            if(rs.next()){
                id_account = rs.getInt("id_account");
            }
            return id_account;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    public static String insertAdminToken(int id_admin) throws Exception{
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return insertAdminToken(conn, id_admin);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public static String insertAdminToken(Connection conn, int id_admin) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("INSERT INTO admin_token (id_admin, token, expiration_date) values (?,md5(concat(current_timestamp,' ',?)),current_timestamp + interval '30 day') returning token");
            ps.setInt(1, id_admin);
            ps.setInt(2, id_admin);
            rs = ps.executeQuery();
            rs.next();
            String token = rs.getString("token");
            return token;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

    }
    
    public static int findIdAdminWithToken(String token) throws Exception{
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findIdAdminWithToken(conn, token);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public static int findIdAdminWithToken(Connection conn, String token) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id_admin = -1;
        try {
            ps = conn.prepareStatement("SELECT * from admin_token where token=? and expiration_date>=current_timestamp");
            ps.setString(1, token);

            rs = ps.executeQuery();
            if(rs.next()){
                id_admin = rs.getInt("id_admin");
            }
            return id_admin;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
}
