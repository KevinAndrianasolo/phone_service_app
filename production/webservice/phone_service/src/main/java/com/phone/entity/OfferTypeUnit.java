/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class OfferTypeUnit {
    private int id_offer_type_unit;
    private int id_unit;
    private int id_offer_type;
    private String name;
    public OfferTypeUnit(){
   
    }
    public OfferTypeUnit(int id_offer_type_unit, int id_unit, int id_offer_type, String name) {
        this.id_offer_type_unit = id_offer_type_unit;
        this.id_unit = id_unit;
        this.id_offer_type = id_offer_type;
        this.name = name;
    }

    public int getId_offer_type_unit() {
        return id_offer_type_unit;
    }

    public void setId_offer_type_unit(int id_offer_type_unit) {
        this.id_offer_type_unit = id_offer_type_unit;
    }

    public int getId_unit() {
        return id_unit;
    }

    public void setId_unit(int id_unit) {
        this.id_unit = id_unit;
    }

    public int getId_offer_type() {
        return id_offer_type;
    }

    public void setId_offer_type(int id_offer_type) {
        this.id_offer_type = id_offer_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "OfferTypeUnit{" + "id_offer_type_unit=" + id_offer_type_unit + ", id_unit=" + id_unit + ", id_offer_type=" + id_offer_type + ", name=" + name + '}';
    }
    
    public static ArrayList<OfferTypeUnit> findAll(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<OfferTypeUnit> ls = new ArrayList<OfferTypeUnit>();
        try {
            ps = conn.prepareStatement("SELECT * FROM Offer_Type_unit_details");
            rs = ps.executeQuery();
            while (rs.next()) {
                OfferTypeUnit tmp = new OfferTypeUnit();
                tmp.setId_offer_type_unit(rs.getInt("id_offer_type_unit"));
                tmp.setId_offer_type(rs.getInt("id_offer_type"));
                tmp.setId_unit(rs.getInt("id_unit"));
                tmp.setName(rs.getString("name"));
                ls.add(tmp);
            }
            return ls;

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static ArrayList<OfferTypeUnit> findAll() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findAll(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
}
