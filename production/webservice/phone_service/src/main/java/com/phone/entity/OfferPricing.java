/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author admin
 */
public class OfferPricing {
    private int id_offer_pricing = -1;
    private int id_offer;
    private double call_cost_faf;
    private double call_cost_operator;
    private double call_cost_other_operator;
    private double call_cost_international;
    private double message_cost;
    private double internet_cost;
    private double default_cost;
    public OfferPricing(){
        
    }
    public OfferPricing(int id_offer_pricing, int id_offer, double call_cost_faf, double call_cost_operator, double call_cost_other_operator, double call_cost_international, double message_cost, double internet_cost) {
        this.id_offer_pricing = id_offer_pricing;
        this.id_offer = id_offer;
        this.call_cost_faf = call_cost_faf;
        this.call_cost_operator = call_cost_operator;
        this.call_cost_other_operator = call_cost_other_operator;
        this.call_cost_international = call_cost_international;
        this.message_cost = message_cost;
        this.internet_cost = internet_cost;
    }

    public double getDefault_cost() {
        return default_cost;
    }

    public void setDefault_cost(double default_cost) {
        this.default_cost = default_cost;
    }

    public int getId_offer_pricing() {
        return id_offer_pricing;
    }

    public void setId_offer_pricing(int id_offer_pricing) {
        this.id_offer_pricing = id_offer_pricing;
    }

    public int getId_offer() {
        return id_offer;
    }

    public void setId_offer(int id_offer) {
        this.id_offer = id_offer;
    }

    public double getCall_cost_faf() {
        return call_cost_faf;
    }

    public void setCall_cost_faf(double call_cost_faf)  throws Exception{
        this.call_cost_faf = call_cost_faf;
    }

    public double getCall_cost_operator() {
        return call_cost_operator;
    }

    public void setCall_cost_operator(double call_cost_operator) throws Exception {
        this.call_cost_operator = call_cost_operator;
    }

    public double getCall_cost_other_operator() {
        return call_cost_other_operator;
    }

    public void setCall_cost_other_operator(double call_cost_other_operator) throws Exception {
        this.call_cost_other_operator = call_cost_other_operator;
    }

    public double getCall_cost_international() {
        return call_cost_international;
    }

    public void setCall_cost_international(double call_cost_international)  throws Exception{
        this.call_cost_international = call_cost_international;
    }

    public double getMessage_cost() {
        return message_cost;
    }

    public void setMessage_cost(double message_cost) throws Exception {
        this.message_cost = message_cost;
    }

    public double getInternet_cost() {
        return internet_cost;
    }

    public void setInternet_cost(double internet_cost) throws Exception{
        this.internet_cost = internet_cost;
    }

    @Override
    public String toString() {
        return "Pricing{" + "id_offer_pricing=" + id_offer_pricing + ", id_offer=" + id_offer + ", call_cost_faf=" + call_cost_faf + ", call_cost_operator=" + call_cost_operator + ", call_cost_other_operator=" + call_cost_other_operator + ", call_cost_international=" + call_cost_international + ", message_cost=" + message_cost + ", internet_cost=" + internet_cost + '}';
    }
    public boolean verify() throws Exception{
        if(call_cost_faf<0) throw new Exception("Appel vers FAF est invalide.");
        
        if(call_cost_operator<0) throw new Exception("Appel vers l'opérateur est invalide.");
        
        if(call_cost_other_operator<0) throw new Exception("Appel vers les autres opérateurs est invalide.");
        
        if(call_cost_international<0) throw new Exception("Appel à l'international est invalide.");
        
        if(message_cost<0) throw new Exception("Le cout du message est invalide.");
        
        if(internet_cost<0) throw new Exception("Le cout de l'internet est invalide.");
        
        return true;
    }
    public static OfferPricing findOfferPricingOf(int id_offer, Connection conn) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        OfferPricing pricing = null;

        try {
            ps = conn.prepareStatement("SELECT id_offer_pricing,id_offer, call_cost_faf, call_cost_operator, call_cost_other_operator, call_cost_international, message_cost, internet_cost, default_cost FROM offer_pricing where id_offer=?");
            ps.setInt(1, id_offer);
            rs = ps.executeQuery();
            if (rs.next()) {
                pricing = new OfferPricing();
                pricing.setId_offer_pricing(rs.getInt("id_offer_pricing"));
                pricing.setId_offer(rs.getInt("id_offer"));
                pricing.setCall_cost_faf(rs.getDouble("call_cost_faf"));
                pricing.setCall_cost_operator(rs.getDouble("call_cost_operator"));
                pricing.setCall_cost_other_operator(rs.getDouble("call_cost_other_operator"));
                pricing.setCall_cost_international(rs.getDouble("call_cost_international"));
                pricing.setMessage_cost(rs.getDouble("message_cost"));
                pricing.setInternet_cost(rs.getDouble("internet_cost"));
                pricing.setDefault_cost(rs.getDouble("default_cost"));

            }
            return pricing;
        
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public static void save(int id_offer, OfferPricing offer_pricing, Connection conn) throws Exception {
        if(offer_pricing==null) return;
        offer_pricing.verify();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("insert into offer_pricing(id_offer, call_cost_faf, call_cost_operator, call_cost_other_operator, call_cost_international, message_cost, internet_cost, default_cost) values (?, ?, ?, ?, ?, ?, ?, ?)");
            ps.setInt(1, id_offer);
            ps.setDouble(2, offer_pricing.getCall_cost_faf());
            ps.setDouble(3, offer_pricing.getCall_cost_operator());
            ps.setDouble(4, offer_pricing.getCall_cost_other_operator());
            ps.setDouble(5, offer_pricing.getCall_cost_international());
            ps.setDouble(6, offer_pricing.getMessage_cost());
            ps.setDouble(7, offer_pricing.getInternet_cost());
            ps.setDouble(8, offer_pricing.getDefault_cost());
            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;  
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public static void update(int id,OfferPricing offer_pricing, Connection conn) throws Exception {
        if(offer_pricing==null) return;
        offer_pricing.verify();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("update offer_pricing set id_offer=?, call_cost_faf=?, call_cost_operator=?, call_cost_other_operator=?, call_cost_international=?, message_cost=?, internet_cost=? , default_cost = ? where id_offer_pricing=?");
            
            ps.setInt(1, offer_pricing.getId_offer());
            ps.setDouble(2, offer_pricing.getCall_cost_faf());
            ps.setDouble(3, offer_pricing.getCall_cost_operator());
            ps.setDouble(4, offer_pricing.getCall_cost_other_operator());
            ps.setDouble(5, offer_pricing.getCall_cost_international());
            ps.setDouble(6, offer_pricing.getMessage_cost());
            ps.setDouble(7, offer_pricing.getInternet_cost());
            ps.setDouble(8, offer_pricing.getDefault_cost());

            ps.setInt(9, offer_pricing.getId_offer_pricing());
            
            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    public static void delete(OfferPricing offer_pricing, Connection conn) throws Exception {
        if(offer_pricing == null) return;
        delete(offer_pricing.getId_offer_pricing(), conn);
    }
    public static void delete(int id_offer_pricing, Connection conn) throws Exception {
        
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("delete from offer_pricing where id_offer_pricing=?");
            ps.setInt(1, id_offer_pricing);

            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
}
