/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author admin
 */
public class FCMUser {
    private int id_fcm_user;
    private int id_account;
    private String token;

    public FCMUser() {
    }

    public FCMUser(int id_account, String token) {
        this.id_account = id_account;
        this.token = token;
    }

    public int getId_fcm_user() {
        return id_fcm_user;
    }

    public void setId_fcm_user(int id_fcm_user) {
        this.id_fcm_user = id_fcm_user;
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "FCMUser{" + "id_fcm_user=" + id_fcm_user + ", id_account=" + id_account + ", token=" + token + '}';
    }
    
    
    public void save() throws Exception {

        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            this.save(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void save(Connection conn) throws Exception {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("insert into fcm_user (id_account, token) values (? , ?)");
            ps.setInt(1, this.getId_account());
            ps.setString(2, this.getToken());
            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    
    public static FCMUser findByIdAccount(Connection conn, int idAccount) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        FCMUser fcm_user = null;

        try {
            ps = conn.prepareStatement("SELECT * FROM current_fcm_user where id_account=?");
            ps.setInt(1, idAccount);
            rs = ps.executeQuery();
            while (rs.next()) {
                fcm_user = new FCMUser();
                fcm_user.setId_fcm_user(rs.getInt("id_fcm_user"));
                fcm_user.setId_account(rs.getInt("id_account"));
                fcm_user.setToken(rs.getString("token"));
            }
            return fcm_user;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

    }

    public static FCMUser findByIdAccount(int idAccount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findByIdAccount(conn, idAccount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }


    
}
