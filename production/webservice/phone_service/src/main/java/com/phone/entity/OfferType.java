/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class OfferType {
    private int id_offer_type;
    private String description;
    public static int CREDIT = 1;
    public static int APPEL = 2;
    public static int MESSAGE = 3;
    public static int INTERNET = 4;
    public static int DUREE_APPEL = 7;


    public OfferType(){
        
    }
    public OfferType(int id_offer_type, String description) {
        this.id_offer_type = id_offer_type;
        this.description = description;
    }

    public int getId_offer_type() {
        return id_offer_type;
    }

    public void setId_offer_type(int id_offer_type) {
        this.id_offer_type = id_offer_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "OfferType{" + "id_offer_type=" + id_offer_type + ", description=" + description + '}';
    }
    
    public static ArrayList<OfferType> findAll(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<OfferType> ls = new ArrayList<OfferType>();
        try {
            ps = conn.prepareStatement("SELECT * FROM Offer_Type");
            rs = ps.executeQuery();
            while (rs.next()) {
                OfferType tmp = new OfferType();
                tmp.setId_offer_type(rs.getInt("id_offer_type"));
                tmp.setDescription(rs.getString("description"));
                ls.add(tmp);
            }
            return ls;

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static ArrayList<OfferType> findAll() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findAll(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
}