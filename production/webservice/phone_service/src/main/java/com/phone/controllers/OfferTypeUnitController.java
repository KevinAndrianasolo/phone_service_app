/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.controllers;

import com.phone.entity.OfferTypeUnit;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author admin
 */

@CrossOrigin(origins = "*")
@RestController
public class OfferTypeUnitController {
    @GetMapping("/OfferTypeUnit")
    public Response findAll(){
        try{
            List<OfferTypeUnit> offer_type_unit = OfferTypeUnit.findAll();
            return ResponseBuilder.Success(offer_type_unit);
        }
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
}
