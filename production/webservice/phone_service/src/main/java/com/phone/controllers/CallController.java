package com.phone.controllers;

import com.phone.entity.CallStatistics;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class CallController extends BaseController {

    @GetMapping("/Statistics/Call")
    public Response findCallsByIdAccount(@RequestHeader("Authorization") String bearer_token, @RequestParam(name = "month") int month, @RequestParam(name = "year") String year) throws Exception {
        try {
            String token = bearer_token.replace("Bearer ", "");
            int idAccount = isValidAccountToken(token);
            return ResponseBuilder.Success(CallStatistics.findCallsByIdAccount(idAccount, month, year));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }
    
    @GetMapping("/Statistics/Call/DefaultOperator")
    public Response findDefaultOperatorCalls(@RequestHeader("Authorization") String bearer_token, @RequestParam(name = "month") int month, @RequestParam(name = "year") String year) throws Exception {
        try {
            String token = bearer_token.replace("Bearer ", "");
            int idAccount = isValidAccountToken(token);
            return ResponseBuilder.Success(CallStatistics.findDefaultOperatorCallsByIdAccount(idAccount, month, year));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @GetMapping("/Statistics/Call/OtherOperator")
    public Response findOtherOperatorCalls(@RequestHeader("Authorization") String bearer_token, @RequestParam(name = "month") int month, @RequestParam(name = "year") String year) throws Exception {
        try {
            String token = bearer_token.replace("Bearer ", "");
            int idAccount = isValidAccountToken(token);
            return ResponseBuilder.Success(CallStatistics.findOtherOperatorCallsByIdAccount(idAccount, month, year));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @GetMapping("/Statistics/Call/International")
    public Response findInternationalCalls(@RequestHeader("Authorization") String bearer_token, @RequestParam(name = "month") int month, @RequestParam(name = "year") String year) throws Exception {
        try {
            String token = bearer_token.replace("Bearer ", "");
            int idAccount = isValidAccountToken(token);
            return ResponseBuilder.Success(CallStatistics.findInternationalCallsByIdAccount(idAccount, month, year));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }
    
    @GetMapping("/Statistics/Call/All")
    public Response findAllCalls( @RequestParam(name = "month") int month, @RequestParam(name = "year") String year) throws Exception {
        try {
            return ResponseBuilder.Success(CallStatistics.findCalls(month,year));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

      @GetMapping("/Statistics/Call/All/DefaultOperator")
    public Response findAllDefaultOperatorCalls(@RequestParam(name = "year") String year) throws Exception {
        try {
            return ResponseBuilder.Success(CallStatistics.findDefaultCallStatistics(year));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }
    
          @GetMapping("/Statistics/Call/All/OtherOperator")
    public Response findAllOtherOperatorCalls(@RequestParam(name = "year") String year) throws Exception {
        try {
            return ResponseBuilder.Success(CallStatistics.findOtherCallStatistics(year));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }
    
          @GetMapping("/Statistics/Call/All/International")
    public Response findAllInternationalOperatorCalls( @RequestParam(name = "year") String year) throws Exception {
        try {
            return ResponseBuilder.Success(CallStatistics.findInternationalCallStatistics(year));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

}
