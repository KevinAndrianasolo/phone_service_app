package com.phone.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {

    private static Connection conn = null;
    public static String db_name = "phone_service";
    public static String mongo_uri = "mongodb://localhost:27017";

    public static Connection getOracleConnection()
            throws ClassNotFoundException, SQLException {
        final String host = "localhost";
        final int port = 1521;
        final String dbname = "DBCOURS";
        final String url = "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbname;
        final String user = "ws";
        final String password = "123456";

        Class.forName("oracle.jdbc.driver.OracleDriver");
        conn = DriverManager.getConnection(url, user, password);
        return conn;
    }

    public static Connection getPsqlConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        /**
         * On localhost
         */
        //Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/phone_service_app", "phone_service_user", "123456");
        
        /**
         * With heroku
         */
        Connection connection = DriverManager.getConnection("jdbc:postgresql://ec2-52-1-115-6.compute-1.amazonaws.com:5432/d7phq8dv93c1sr", "rrqjwkdxxupeqp", "58407022b55574dffe7445d979b400137c9e0906cbd919a6846402167c6c5785");
        return connection;
    }
}
