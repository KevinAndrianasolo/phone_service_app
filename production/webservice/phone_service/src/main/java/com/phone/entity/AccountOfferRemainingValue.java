/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author admin
 */

public class AccountOfferRemainingValue {
    private int id_account;
    private int id_account_offer;
    private int id_offer;
    private int id_offer_type;
    private double value;
    private String unit;
    private int priority;
    private String offer_name;
    private String offer_type_description;
    private Timestamp purchase_date;
    private Timestamp expiration_date;
    private AccountOfferRemainingValue(){
        
    }
    public AccountOfferRemainingValue(int id_account, int id_account_offer, int id_offer, int id_offer_type, double value, String unit, int priority, String offer_name, Timestamp purchase_date, Timestamp expiration_date) {
        this.id_account = id_account;
        this.id_account_offer = id_account_offer;
        this.id_offer = id_offer;
        this.id_offer_type = id_offer_type;
        this.value = value;
        this.unit = unit;
        this.priority = priority;
        this.offer_name = offer_name;
        this.purchase_date = purchase_date;
        this.expiration_date = expiration_date;
    }

    public String getOffer_type_description() {
        return offer_type_description;
    }

    public void setOffer_type_description(String offer_type_description) {
        this.offer_type_description = offer_type_description;
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public int getId_account_offer() {
        return id_account_offer;
    }

    public void setId_account_offer(int id_account_offer) {
        this.id_account_offer = id_account_offer;
    }

    public int getId_offer() {
        return id_offer;
    }

    public void setId_offer(int id_offer) {
        this.id_offer = id_offer;
    }

    public int getId_offer_type() {
        return id_offer_type;
    }

    public void setId_offer_type(int id_offer_type) {
        this.id_offer_type = id_offer_type;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getOffer_name() {
        return offer_name;
    }

    public void setOffer_name(String offer_name) {
        this.offer_name = offer_name;
    }

    public Timestamp getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(Timestamp purchase_date) {
        this.purchase_date = purchase_date;
    }

    public Timestamp getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(Timestamp expiration_date) {
        this.expiration_date = expiration_date;
    }

    @Override
    public String toString() {
        return "AccountOfferRemainingValue{" + "id_account=" + id_account + ", id_account_offer=" + id_account_offer + ", id_offer=" + id_offer + ", id_offer_type=" + id_offer_type + ", value=" + value + ", unit=" + unit + ", priority=" + priority + ", offer_name=" + offer_name + ", purchase_date=" + purchase_date + ", expiration_date=" + expiration_date + '}';
    }
    
    public static ArrayList<AccountOfferRemainingValue> findAccountOfferRemainingValueOf(Connection conn, int id_account) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<AccountOfferRemainingValue> ls = new ArrayList<AccountOfferRemainingValue>();
        try {
            ps = conn.prepareStatement("SELECT * FROM current_offers_remaining_value_details");
            rs = ps.executeQuery();
            while (rs.next()) {
                AccountOfferRemainingValue tmp = new AccountOfferRemainingValue();
                tmp.setId_offer(rs.getInt("id_offer"));
                tmp.setId_account(rs.getInt("id_account"));
                tmp.setId_account_offer(rs.getInt("id_account_offer"));
                tmp.setId_offer_type(rs.getInt("id_offer_type"));
                tmp.setValue(rs.getDouble("value"));
                tmp.setUnit(rs.getString("unit"));
                tmp.setPriority(rs.getInt("priority"));
                tmp.setOffer_name(rs.getString("offer_name"));
                tmp.setOffer_type_description(rs.getString("offer_type_description"));
                tmp.setPurchase_date(rs.getTimestamp("purchase_date"));
                tmp.setExpiration_date(rs.getTimestamp("expiration_date"));
                ls.add(tmp);
            }
            return ls;

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static ArrayList<AccountOfferRemainingValue> findAccountOfferRemainingValueOf(int id_account) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findAccountOfferRemainingValueOf(conn, id_account);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public ConsumptionData simulate(double remaining_tmp, Connection conn) throws Exception{
        Offer offer = this.findOffer(conn);
        OfferPricing offer_pricing = offer.getOffer_pricing();
        
        double remaining = 0;
        double consumption = 0;
        double value = remaining_tmp;
        double offer_value = this.getValue();
        double tarif = 1;
        /*if(this.getId_offer_type() == OfferType.APPEL){
            // De type APPEL => 1000 Ariary par exemple, et transaformer les seconde en Ariary
            if(offer_pricing == null) throw new Exception("Le tarif de l'offre n°"+this.getId_offer()+" n'a pas encore été défini.");
            tarif = offer_pricing.getCall_cost_operator(); // ENCORE A CHERCHER ENTRE FAF, OP, AUTRE OP, INTER
            value = remaining_tmp * tarif;
        }*/

        if(this.getValue() <= value){
            consumption = this.getValue();
            remaining = (value - consumption)/tarif;
        }
        else{
            consumption = value;
            remaining = 0;
        }
        
        ConsumptionData res = new ConsumptionData(remaining, consumption);
        return res;
    }
    public Offer findOffer(Connection conn) throws Exception{
        return Offer.findById(conn, this.getId_offer());
    }

    
}
