package com.phone.controllers;

import com.phone.entity.Account;
import com.phone.entity.AccountBalance;
import com.phone.entity.AccountBalanceTransaction;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class AccountController extends BaseController{
    @PostMapping(value = "/AccountBalanceTransaction", consumes = "application/json")
    public Response saveAccountBalanceTransaction(@RequestBody AccountBalanceTransaction accountBalanceTransaction,@RequestHeader("Authorization") String bearer_token) throws Exception {
        try {
            String token = bearer_token.replace("Bearer ", "");
            int id_account = isValidAccountToken(token); 
            Account account = new Account(id_account);
            AccountBalance account_balance = account.findAccountBalance();
            if(account_balance==null) throw new Exception("Vous n'avez pas encore de compte principale.");
            
            int id_account_balance = account_balance.getIdAccountBalance();
            accountBalanceTransaction.setId_account_balance(id_account_balance);
            accountBalanceTransaction.save(account_balance);
            return ResponseBuilder.Success(null);
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }
    @GetMapping(value = "/AccountBalance")
    public Response findMobileMoneyAccount(@RequestHeader("Authorization") String bearer_token) throws Exception {
        try {
            String token = bearer_token.replace("Bearer ", "");
            int id_account = isValidAccountToken(token); 
            Account account = new Account(id_account);
            AccountBalance account_balance = account.findAccountBalance();
            if(account_balance==null) throw new Exception("Vous n'avez pas encore de compte principale.");
            return ResponseBuilder.Success(account_balance);
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }
    @GetMapping("/Accounts")
    public Response findAll() throws Exception {
        try {
            return ResponseBuilder.Success(Account.findAll());
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @GetMapping("/Accounts/{id_account}")
    public Response findById(@PathVariable(value = "id_account") int id) throws Exception {
        try {
            return ResponseBuilder.Success(Account.findById(id));
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @GetMapping("/Accounts/{id_account}/CurrentOffers")
    public Response getCurrentOffers(@PathVariable(value = "id_account") int id) throws Exception {
        try {
            Account account = Account.findById(id);
            return ResponseBuilder.Success(account.findCurrentOffers());
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }

    @PostMapping("/Purchase")
    public int purchaseOffer(@PathVariable(value = "id_account") int id, @PathVariable(value = "is_with_mobile_money") boolean isWithMobileMoney, @PathVariable(value = "secret_code") String secretCode) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @PostMapping("/Accounts/Add")
    public int save() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @PutMapping("/Accounts/Update")
    public int update(@RequestParam(name = "id_account") int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @DeleteMapping("/Accounts/Delete")
    public int delete(@RequestParam(name = "id_account") int id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
