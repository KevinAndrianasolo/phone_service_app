package com.phone.entity;

import com.phone.utils.DBUtils;
import com.phone.utils.Helper;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class PhoneNumber implements Serializable{

    private String number;

    public PhoneNumber() {
    }

    public PhoneNumber(String number) throws Exception {
        init(number);
    }

    private void init(String number) throws Exception {
        this.setNumber(number);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) throws Exception {
        if(number == null) throw new Exception("Format du numéro invalide.");
            
        this.number = Helper.formatNumber(number);
    }

    public boolean isFAF(int idAccount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return isFAF(conn, idAccount);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public boolean isFAF(Connection conn, int idAccount) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String num = this.getNumber().replaceAll(" ", "");
        
        try {
            ps = conn.prepareStatement("SELECT * FROM FAF_PHONE_NUMBER WHERE id_account = ? and faf_phone_number = ?");
            ps.setInt(1, idAccount);
            ps.setString(2, num);

            rs = ps.executeQuery();
            return rs.next();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public boolean isDefaultOperator() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return isDefaultOperator(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public boolean isDefaultOperator(Connection conn) throws Exception {
        Informations infos = Informations.find(conn);
        String num = this.getPrefix();
        return infos.getPrefix().compareTo(num)==0;
    }

    public String getPrefix() {
        return Helper.getNumberPrefix(this.getNumber());
    }

    @Override
    public String toString() {
        return "PhoneNumber{" + "number=" + number + '}';
    }
    

}
