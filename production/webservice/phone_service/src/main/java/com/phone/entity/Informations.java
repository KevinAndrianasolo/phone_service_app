package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Informations{

    private int idOperator;
    private String groupName;
    private String balance;
    private String prefix;
    

    public Informations() {

    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getIdOperator() {
        return idOperator;
    }

    public void setIdOperator(int idOperator) {
        this.idOperator = idOperator;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public static Informations find(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Informations infos = null;

        try {
            ps = conn.prepareStatement("SELECT * FROM Informations LIMIT 1");
            rs = ps.executeQuery();
            if (rs.next()) {
                infos = new Informations();
                infos.setIdOperator(rs.getInt("id_operator"));
                infos.setBalance(rs.getString("balance"));
                infos.setGroupName(rs.getString("group_name"));
                infos.setPrefix(rs.getString("prefix"));

            }
            return infos;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        

    }

    public static Informations find() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return find(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public Operator getOperator() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return getOperator(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public Operator getOperator(Connection conn) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Operator operator = new Operator();

        try {
            ps = conn.prepareStatement("SELECT * FROM Operator WHERE id_operator = ?");
            ps.setInt(1, this.getIdOperator());
            rs = ps.executeQuery();
            while (rs.next()) {
                operator.setIdOperator(rs.getInt("id_operator"));
                operator.setOperatorName(rs.getString("operator_name"));
                operator.setPrefix(rs.getString("prefix"));
            }
            return operator;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    @Override
    public String toString() {
        return "Informations{" + "idOperator=" + idOperator + ", groupName=" + groupName + ", balance=" + balance + '}';
    }

}
