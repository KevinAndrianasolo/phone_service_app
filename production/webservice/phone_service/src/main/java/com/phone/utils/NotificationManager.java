/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.utils;

import com.phone.entity.Account;
import com.phone.entity.FCMNotificationBody;
import com.phone.entity.FCMNotification;
import com.phone.entity.FCMUser;
import java.sql.Connection;
import org.bson.Document;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author admin
 */
public class NotificationManager {
    public static Object sendNotification(){
        String to = "fE1R77RsTiqPYpIfjCArbm:APA91bHz7DS_OVCtI3izZPs2i1r2Yz02KHqZ52orAwQW-p0ExS6QLrsRYdqpa6JZetJCA7t4qKPofQG93PhD3KjBC9kZFDt41hgTl_n3BmTEry7nz880aeHmbdYrx3SZ0Fe3K15a_4Lu";
        
        String title = "Notification venant de PhoneServiceApp";
        String body = "Votre transaction a été validé avec succès.";
        
        FCMNotificationBody notification_body = new FCMNotificationBody(title, body);
        return sendNotification(notification_body, to);
    }
    public static Object sendNotification(FCMNotificationBody notification_body, int id_account) throws Exception{
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return sendNotification(notification_body, id_account, conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public static Object sendNotification(FCMNotificationBody notification_body, int id_account, Connection conn) throws Exception{
        
        FCMUser fcm_user = FCMUser.findByIdAccount(conn, id_account);
        if(fcm_user != null){
            String to = fcm_user.getToken();
            FCMNotification notification = new FCMNotification(notification_body, to);
            return sendNotification(notification);
        }
        return null;
    }
    
    
    public static Object sendNotification(FCMNotificationBody notification_body, String to){
        FCMNotification notification = new FCMNotification(notification_body, to);
        return sendNotification(notification);
    }
    public static Object sendNotification(FCMNotification notification){
        String server_fcm_key = "AAAAP-UuMfw:APA91bGEXWUIYk3FnGiXYAOm8lak71sJX33-cshdORBbB1kUIppBOCqgnqZ4A36U9BRS8wBq9OEGVPCvR2d3deT9sVk4HTGiL4iqZ4rFOF-Drhcvr6cDhOnKaOTXCnbf8U-iYOZCtQEK";
        String URL = "https://fcm.googleapis.com/fcm/send";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "key="+server_fcm_key);
        Document doc = notification.toDocument();
        HttpEntity<Document> request = 
          new HttpEntity<Document>(doc, headers);

        String res = restTemplate.postForObject(URL, request, String.class);
        
        return res;
    }
}
