package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class AccountOffer {

    private int idAccountOffer;
    private int idAccount;
    private int idOffer;
    private double remainingCall;
    private int remainingMessage;
    private double remainingInternet;
    private Timestamp purchaseDate;
    private Timestamp expirationDate;

    private boolean withMobileMoney = false;

    public AccountOffer() {
    }

    public AccountOffer(int idAccountOffer, int idAccount, int idOffer, Timestamp purchaseDate, Timestamp expirationDate) {
        this.idAccountOffer = idAccountOffer;
        this.idAccount = idAccount;
        this.idOffer = idOffer;
        this.purchaseDate = purchaseDate;
        this.expirationDate = expirationDate;
    }

    public double getRemainingCall() {
        return remainingCall;
    }

    public void setRemainingCall(double remainingCall) {
        this.remainingCall = remainingCall;
    }

    public int getRemainingMessage() {
        return remainingMessage;
    }

    public void setRemainingMessage(int remainingMessage) {
        this.remainingMessage = remainingMessage;
    }

    public double getRemainingInternet() {
        return remainingInternet;
    }

    public void setRemainingInternet(double remainingInternet) {
        this.remainingInternet = remainingInternet;
    }

    public int getIdAccountOffer() {
        return idAccountOffer;
    }

    public void setIdAccountOffer(int idAccountOffer) {
        this.idAccountOffer = idAccountOffer;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public int getIdOffer() {
        return idOffer;
    }

    public void setIdOffer(int idOffer) {
        this.idOffer = idOffer;
    }

    public Timestamp getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Timestamp purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Timestamp getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isWithMobileMoney() {
        return withMobileMoney;
    }

    public void setWithMobileMoney(boolean withMobileMoney) {
        this.withMobileMoney = withMobileMoney;
    }

    public static void save(AccountOffer account_offer) throws Exception {

        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            save(conn, account_offer);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    //   id_account_offer, id_account, id_offer, remaining_call, remaining_internet,
    //remaining_message, purchase_date, expiration_date

    public AccountOffer findByIdAccount(int idAccount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findByIdAccount(conn, idAccount);
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public AccountOffer findByIdAccount(Connection conn, int idAccount) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        AccountOffer accountOffer = null;
        try {
            ps = conn.prepareStatement("SELECT * FROM account_offer WHERE id_account = ?");
            ps.setInt(1, idAccount);
            rs = ps.executeQuery();
            while (rs.next()) {
                accountOffer = new AccountOffer();
                accountOffer.setIdAccountOffer(rs.getInt("id_account_offer"));
                accountOffer.setIdAccount(idAccount);
                accountOffer.setIdOffer(rs.getInt("id_offer"));
                accountOffer.setRemainingCall(rs.getDouble("remaining_call"));
                accountOffer.setRemainingInternet(rs.getDouble("remaining_internet"));
                accountOffer.setRemainingMessage(rs.getInt("remaining_message"));
                accountOffer.setPurchaseDate(rs.getTimestamp("purchase_date"));
                accountOffer.setExpirationDate(rs.getTimestamp("expiration_date"));
            }
            return accountOffer;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public int update(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement("UPDATE AccountOffer SET remaining_call = ? , remaining_internet = ? ,"
                    + " remaining_message = ? WHERE id_acount_offer = ?");
            ps.setDouble(1, this.getRemainingCall());
            ps.setDouble(2, this.getRemainingInternet());
            ps.setInt(3, this.getRemainingMessage());
            ps.setInt(4, this.getIdAccountOffer());
            return ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public int update() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return update(conn);
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static void save(Connection conn, AccountOffer account_offer) throws Exception {
        PreparedStatement ps = null;
        try {
            conn.setAutoCommit(false);
            Offer offer = Offer.findById(conn, account_offer.getIdOffer());

            ps = conn.prepareStatement("insert into account_offer(id_account, id_offer, purchase_date, expiration_date) values (?, ?, current_timestamp , current_timestamp + '" + offer.getValidity() + " " + offer.getValidity_unit() + "')");
            ps.setInt(1, account_offer.getIdAccount());
            ps.setInt(2, account_offer.getIdOffer());
            ps.executeUpdate();

            Account account = new Account(account_offer.getIdAccount());
            AccountBalance account_balance = account.findAccountBalance(conn);
            MobileMoneyAccount mobile_money_account = account.findMobileMoneyAccount(conn);
            double amount = offer.getCost();

            if (account_offer.withMobileMoney) {
                int transactionType = 2; // De type RETRAIT
                int status = 1; // Validé
                MobileMoneyTransaction transaction = new MobileMoneyTransaction();
                transaction.setAmount(amount);
                transaction.setIdMobileMoneyTransactionType(transactionType);
                transaction.setStatus(status);
                transaction.setIdMobileMoneyAccount(mobile_money_account.getIdMobileMoneyAccount());
                transaction.save(conn, mobile_money_account);
            } else {
                AccountBalanceTransaction transaction = new AccountBalanceTransaction();
                transaction.setAmount(-1 * amount);
                transaction.setId_account_balance(account_balance.getIdAccountBalance());
                transaction.save(conn, account_balance, false);
            }
            conn.commit();
        } catch (Exception ex) {
            conn.rollback();
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    @Override
    public String toString() {
        return "AccountOffer{" + "idAccountOffer=" + idAccountOffer + ", idAccount=" + idAccount + ", idOffer=" + idOffer + ", remainingCall=" + remainingCall + ", remainingInternet=" + remainingInternet + ", remainingMessage=" + remainingMessage + ", purchaseDate=" + purchaseDate + ", expirationDate=" + expirationDate + '}';
    }

    private double simulateCall(double duration) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
