package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

;

public class MobileMoneyAccount {

    private int idMobileMoneyAccount;
    private int idAccount;
    private double accountBalance;
    private String secreCode;

    public MobileMoneyAccount() {
    }

    public int getIdMobileMoneyAccount() {
        return idMobileMoneyAccount;
    }

    public void setIdMobileMoneyAccount(int idMobileMoneyAccount) {
        this.idMobileMoneyAccount = idMobileMoneyAccount;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public String getSecreCode() {
        return secreCode;
    }

    public void setSecreCode(String secreCode) {
        this.secreCode = secreCode;
    }

    public Account getAccount() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return getAccount(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public Account getAccount(Connection conn) throws Exception {
        return Account.findById(conn, this.getIdAccount());

    }

    public static MobileMoneyAccount findByIdAccount(Connection conn, int idAccount) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        MobileMoneyAccount moneyAccount = null;

        try {
            ps = conn.prepareStatement("SELECT * FROM V_Mobile_money_account WHERE id_account = ?");
            ps.setInt(1, idAccount);
            rs = ps.executeQuery();
            while (rs.next()) {
                moneyAccount = new MobileMoneyAccount();
                moneyAccount.setIdMobileMoneyAccount(rs.getInt("id_mobile_money_account"));
                moneyAccount.setAccountBalance(rs.getDouble("account_balance"));
                moneyAccount.setSecreCode(rs.getString("secret_code"));
                moneyAccount.setIdAccount(idAccount);

            }
            return moneyAccount;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static MobileMoneyAccount findByIdAccount(int idAccount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findByIdAccount(conn, idAccount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static MobileMoneyAccount findById(Connection conn, int idMobileMoneyAccount) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        MobileMoneyAccount moneyAccount = new MobileMoneyAccount();

        try {
            ps = conn.prepareStatement("SELECT * FROM V_Mobile_money_account WHERE id_mobile_money_account = ?");
            ps.setInt(1, idMobileMoneyAccount);
            rs = ps.executeQuery();
            while (rs.next()) {
                moneyAccount.setIdMobileMoneyAccount(rs.getInt("id_mobile_money_account"));
                moneyAccount.setAccountBalance(rs.getDouble("account_balance"));
                moneyAccount.setSecreCode(rs.getString("secret_code"));
                moneyAccount.setIdAccount(idMobileMoneyAccount);

            }
            return moneyAccount;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static MobileMoneyAccount findById(int idMobileMoneyAccount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findById(conn, idMobileMoneyAccount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int save(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("INSERT INTO mobile_money_account(id_account,account_balance,secret_code) VALUES (? , ? , ?)");
            ps.setInt(1, this.getIdAccount());
            ps.setDouble(2, this.getAccountBalance());
            ps.setString(3, this.getSecreCode());
            return ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public int save() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.save(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int update(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("UPDATE mobile_money_account SET account_balance = ? , secret_code = ? WHERE id_mobile_money_account = ?");
            ps.setDouble(1, this.getAccountBalance());
            ps.setString(2, this.getSecreCode());
            ps.setInt(3, this.getIdMobileMoneyAccount());
            return ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public int update() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.update(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    public double withdrawMoney(double amount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.withdrawMoney(conn, amount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public double withdrawMoney(Connection conn, double amount) throws Exception {
  
        if (amount < 0) {
            throw new Exception("Le montant de retrait ne peut pas être négatif");
        }
        double overflow = 0;
        if (amount >= this.getAccountBalance()) {
            overflow = amount - this.getAccountBalance();
            this.setAccountBalance(0);
        } else {
            this.setAccountBalance(this.getAccountBalance() - amount);
        }
        this.update();
        return overflow;
    }

    public int depositMoney(double amount) throws Exception {

        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.depositMoney(conn, amount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int depositMoney(Connection conn, double amount) throws Exception {
        if (amount < 0) {
            throw new Exception("Le montant de dépot ne peut pas être négatif");
        }
        double currentAmount = this.getAccountBalance();
        this.setAccountBalance(currentAmount + amount);

        return this.update();
    }

    public double withdraw(double amount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.withdraw(conn, amount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public double withdraw(Connection conn, double amount) throws Exception {
        MobileMoneyTransaction transaction = new MobileMoneyTransaction();
        transaction.setIdMobileMoneyAccount(this.getIdMobileMoneyAccount());
        transaction.setAmount(amount);
        transaction.setIdMobileMoneyTransactionType(TransactionType.RETRAIT);
        transaction.save(conn);
        return this.withdrawMoney(conn, amount);
    }

    public int createTransaction(Connection conn, int typeTransaction, double amount) throws Exception {
        MobileMoneyTransaction transaction = new MobileMoneyTransaction();
        transaction.setAmount(amount);
        transaction.setIdMobileMoneyAccount(this.getIdMobileMoneyAccount());
        transaction.setIdMobileMoneyTransactionType(typeTransaction);
        return transaction.save(conn);
    }

    public int createTransaction(int typeTransaction, double amount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return createTransaction(conn, typeTransaction, amount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int depositTransaction(double amount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return depositTransaction(conn, amount);
        } catch (Exception e) {
            throw e;

        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int withdrawTransaction(double amount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return withdrawTransaction(conn, amount);
        } catch (Exception e) {
            throw e;

        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int depositTransaction(Connection conn, double amount) throws Exception {
        return createTransaction(TransactionType.DEPOT, amount);
    }

    public int withdrawTransaction(Connection conn, double amount) throws Exception {
        return createTransaction(TransactionType.RETRAIT, amount);
    }

    public int deposit(double amount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.deposit(conn, amount);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int deposit(Connection conn, double amount) throws Exception {
        MobileMoneyTransaction transaction = new MobileMoneyTransaction();
        transaction.setIdMobileMoneyAccount(this.getIdMobileMoneyAccount());
        transaction.setAmount(amount);
        transaction.setIdMobileMoneyTransactionType(TransactionType.DEPOT);
        transaction.save(conn);
        return this.depositMoney(conn, amount);
    }

    @Override
    public String toString() {
        return "MobileMoneyAccount{" + "idMobileMoneyAccount=" + idMobileMoneyAccount + ", idAccount=" + idAccount + ", accountBalance=" + accountBalance + ", secreCode=" + secreCode + '}';
    }

}
