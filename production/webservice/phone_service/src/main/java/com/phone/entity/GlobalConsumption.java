/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class GlobalConsumption extends Consumption{
    private String recipient_phone_number;
    private boolean is_international = false;

    public String getRecipient_phone_number() {
        return recipient_phone_number;
    }

    public void setRecipient_phone_number(String recipient_phone_number) {
        this.recipient_phone_number = recipient_phone_number;
    }

    public boolean isIs_international() {
        return is_international;
    }

    public void setIs_international(boolean is_international) {
        this.is_international = is_international;
    }
    
    
    public SimulationData SimulateAndInsert() throws Exception {
        Connection conn = null;
        try {
            
            conn = DBUtils.getPsqlConnection();
            conn.setAutoCommit(false);
            SimulationData simulationData = this.Simulate(conn);
            ArrayList<Consumption> consumptions = simulationData.getConsumptions();
            for(int i=0; i<consumptions.size();i++){
                consumptions.get(i).save(conn);
            }
            // We can insert the AccountBalanceTransaction
            if(simulationData.getAccount_balance_transaction()!=null){
                simulationData.getAccount_balance_transaction().save(conn);
            }
            conn.commit();
            return simulationData;
        } catch (Exception ex) {
            conn.rollback();
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public SimulationData Simulate() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.Simulate(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public SimulationData Simulate(Connection conn) throws Exception {
        ArrayList<Consumption> res = new ArrayList<Consumption>();
        Account account = this.getAccount(conn);
        AccountBalance account_balance = account.findAccountBalance(conn);
        if(account_balance == null) throw new Exception("Vous n'avez pas encore de Solde principale.");
        
        ArrayList<AccountOfferRemainingValue> current_offers = AccountOfferRemainingValue.findAccountOfferRemainingValueOf(conn, this.getId_account());
        double remaining = this.getValue();
        
        for(int i=0; i<current_offers.size(); i++){
            AccountOfferRemainingValue tmp = current_offers.get(i);
            
            if(tmp.getId_offer_type() == this.getId_offer_type()){
                ConsumptionData consumptionData = tmp.simulate(remaining, conn);
                remaining = consumptionData.getRemaining_value();
                res.add(new Consumption(this.getId_account(), tmp.getId_account_offer(), tmp.getId_offer_type(), consumptionData.getConsumption_value(), this.getId_unit()) );
            }
            if(remaining == 0){
                break;
            }
        }      
        
        AccountBalanceTransaction account_balance_transaction = null;
        if(remaining != 0){
            //throw new Exception("Vous n'avez pas d'offre de type : "+this.getId_offer_type());
            Pricing pricing = Pricing.findCurrentPricing(conn);
            double tarif = this.GetTarif(pricing, conn);
            
            double amount = remaining * tarif;
            amount = account_balance.getAccountBalance() - amount < 0 ? account_balance.getAccountBalance() : amount;
            account_balance_transaction = new AccountBalanceTransaction();
            account_balance_transaction.setId_account_balance(account_balance.getIdAccountBalance());
            account_balance_transaction.setAmount(-1 * amount); // RETRAIT
        }
        
        return new SimulationData(res, account_balance_transaction);
    }
    public double GetTarif(Pricing pricing, Connection conn) throws Exception{
        double tarif = 0;
        if(this.getId_offer_type() == OfferType.APPEL) tarif = 1;
        else if(this.getId_offer_type() == OfferType.DUREE_APPEL) {
            PhoneNumber phone_number = new PhoneNumber(this.getRecipient_phone_number());
            if(phone_number.isFAF(conn, this.getId_account())) tarif = pricing.getCall_cost_faf();
            else if(this.isIs_international()) tarif =  pricing.getCall_cost_international();
            else if(phone_number.isDefaultOperator(conn)) tarif =  pricing.getCall_cost_operator();
            else tarif =  pricing.getCall_cost_other_operator();
            
            Call call = new Call();
            call.setIdAccount(this.getId_account());
            call.setDuration(this.getValue());
            call.setIsInternational(this.isIs_international());
            call.setRecipientPhoneNumber(this.getRecipient_phone_number());
            call.save(conn);
        }
        else if(this.getId_offer_type() == OfferType.MESSAGE) tarif = pricing.getMessage_cost();
        else if(this.getId_offer_type() == OfferType.INTERNET) tarif = pricing.getInternet_cost();
        else tarif = pricing.getDefault_cost();
        return tarif;
    }
}
