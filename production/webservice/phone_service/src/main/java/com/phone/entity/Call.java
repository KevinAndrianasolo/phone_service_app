package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;

public class Call {
//Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)

    private int idCall;
    private int idAccount;
    private String recipientPhoneNumber;
    private double duration;
    private Timestamp date;
    private boolean isInternational;
    private double totalDuration = 0.0;

    public Call() {
    }

    public Call(int idCall, int idAccount, String recipientPhoneNumber, double duration, Timestamp date, boolean isInternational) {
        this.idCall = idCall;
        this.idAccount = idAccount;
        this.recipientPhoneNumber = recipientPhoneNumber;
        this.duration = duration;
        this.date = date;
        this.isInternational = isInternational;
    }

    public int getIdCall() {
        return idCall;
    }

    public void setIdCall(int idCall) {
        this.idCall = idCall;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public String getRecipientPhoneNumber() {
        return recipientPhoneNumber;
    }

    public void setRecipientPhoneNumber(String recipientPhoneNumber) {
        this.recipientPhoneNumber = recipientPhoneNumber;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public boolean isIsInternational() {
        return isInternational;
    }

    public void setIsInternational(boolean isInternational) {
        this.isInternational = isInternational;
    }

    public double getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(double totalDuration) {
        this.totalDuration = totalDuration;
    }

    public Account getAccount() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return getAccount(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public Account getAccount(Connection conn) throws Exception {
        return Account.findById(this.getIdAccount());

    }

    @Override
    public String toString() {
        return "Call{" + "idCall=" + idCall + ", idAccount=" + idAccount + ", recipientPhoneNumber=" + recipientPhoneNumber + ", duration=" + duration + ", date=" + date + ", isInternational=" + isInternational + ", totalDuration=" + totalDuration + '}';
    }
    
    public void save() throws Exception {

        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            this.save(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void save(Connection conn) throws Exception {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("insert into Call(id_account, recipient_phone_number, duration, is_international) values (?,?,?,?)");
            ps.setInt(1, this.getIdAccount());
            ps.setString(2, this.getRecipientPhoneNumber());
            ps.setDouble(3, this.getDuration());
            ps.setBoolean(4, this.isIsInternational());
            
            ps.executeUpdate();
            
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
}
