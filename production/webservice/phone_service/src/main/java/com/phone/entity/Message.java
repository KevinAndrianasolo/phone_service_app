/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author admin
 */
public class Message {
    private int id_message;
    private int id_account;
    private String recipient_phone_number;
    private String message_body;

    public Message() {
    }

    public Message(int id_message, int id_account, String recipient_phone_number, String message_body) {
        this.id_message = id_message;
        this.id_account = id_account;
        this.recipient_phone_number = recipient_phone_number;
        this.message_body = message_body;
    }

    public int getId_message() {
        return id_message;
    }

    public void setId_message(int id_message) {
        this.id_message = id_message;
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public String getRecipient_phone_number() {
        return recipient_phone_number;
    }

    public void setRecipient_phone_number(String recipient_phone_number) {
        this.recipient_phone_number = recipient_phone_number;
    }

    public String getMessage_body() {
        return message_body;
    }

    public void setMessage_body(String message_body) {
        this.message_body = message_body;
    }

    @Override
    public String toString() {
        return "Message{" + "id_message=" + id_message + ", id_account=" + id_account + ", recipient_phone_number=" + recipient_phone_number + ", message_body=" + message_body + '}';
    }
    
    
    public void save() throws Exception {

        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            this.save(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void save(Connection conn) throws Exception {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("insert into Message(id_account, recipient_phone_number, message_body) values (?,?,?)");
            ps.setInt(1, this.getId_account());
            ps.setString(2, this.getRecipient_phone_number());
            ps.setString(3, this.getMessage_body());
            
            ps.executeUpdate();
            
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
}
