/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author admin
 */
public class Admin {

    
    private String username;
    private String password;
    public Admin(){
        
    }
    public Admin(String username, String password) {
        this.username = username;
        this.password = password;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Admin{" + "username=" + username + ", password=" + password + '}';
    }
    
    public String login() throws Exception{
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return login(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public String login(Connection conn) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Account account = new Account();
        String token = null;
        try {
            ps = conn.prepareStatement("SELECT id_admin FROM admin where username=? and password=md5(?)");
            ps.setString(1, this.username);
            ps.setString(2, this.password);

            rs = ps.executeQuery();
            if(!rs.next()){
                throw new Exception("Le nom d'utilidateur ou le mot de passe est incorrect.");
            }
            else{
                int id_admin = rs.getInt("id_admin");
                token = Authentification.insertAdminToken(conn, id_admin);
            }
            return token;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

    }
    
    public static void logout(String token) throws Exception{
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            logout(token, conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    public static void logout(String token, Connection conn) throws Exception{
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("delete from admin_token where token=?");
            ps.setString(1, token);
            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }

    }


}
