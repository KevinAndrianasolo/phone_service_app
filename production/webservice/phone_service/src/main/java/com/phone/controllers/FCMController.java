/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.controllers;

import com.phone.utils.NotificationManager;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author admin
 */

@CrossOrigin(origins = "*")
@RestController
public class FCMController {
    @GetMapping(value="/Notification/Send")
    public Response sendNotification()throws Exception{
        try{
            Object res = NotificationManager.sendNotification();
            return ResponseBuilder.Success(res);
        }   
        catch(Exception e){
            return ResponseBuilder.Error(e);
        }
    }
}
