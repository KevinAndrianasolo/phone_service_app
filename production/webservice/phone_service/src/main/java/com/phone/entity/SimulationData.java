/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class SimulationData {
    private ArrayList<Consumption> consumptions;
    private AccountBalanceTransaction account_balance_transaction;

    public SimulationData() {
    }

    public SimulationData(ArrayList<Consumption> consumptions, AccountBalanceTransaction account_balance_transaction) {
        this.consumptions = consumptions;
        this.account_balance_transaction = account_balance_transaction;
    }

    public ArrayList<Consumption> getConsumptions() {
        return consumptions;
    }

    public void setConsumptions(ArrayList<Consumption> consumptions) {
        this.consumptions = consumptions;
    }

    public AccountBalanceTransaction getAccount_balance_transaction() {
        return account_balance_transaction;
    }

    public void setAccount_balance_transaction(AccountBalanceTransaction account_balance_transaction) {
        this.account_balance_transaction = account_balance_transaction;
    }
    
}
