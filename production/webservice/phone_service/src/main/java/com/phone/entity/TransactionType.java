package com.phone.entity;

public class TransactionType {

    public static final int DEPOT = 1;
    public static final int RETRAIT = 2;
    private int type;
    private String description;

    public TransactionType() {
    }

    public TransactionType(int type, String description) {
        this.type = type;
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static TransactionType findByType(int type) {
        TransactionType tmp = new TransactionType();
        tmp.setType(type);
        if (type == TransactionType.DEPOT) {
            tmp.setDescription("Depot");
        } else if (type == TransactionType.RETRAIT) {
            tmp.setDescription("Retrait");
        }
        return tmp;
    }

    @Override
    public String toString() {
        return "TransactionType{" + "type=" + type + ", description=" + description + '}';
    }

}
