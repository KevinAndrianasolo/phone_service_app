package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class OfferStatistics {

    private int[] offerCount;
    private Offer offer;

    public OfferStatistics() {
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public int[] getOfferCount() {
        return offerCount;
    }

    public void setOfferCount(int[] offerCount) {
        this.offerCount = offerCount;
    }

    public static ArrayList<Offer> findOffersByDate(Connection conn, int month, String year) throws Exception {
        ArrayList<Offer> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        int count = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_offer_date WHERE month = ? AND year = ?");
            ps.setInt(1, month);
            ps.setInt(2, Integer.parseInt(year));
            res = ps.executeQuery();
            while (res.next()) {
                Offer tmp = new Offer();
                tmp.setId_offer(res.getInt("id_offer"));
                tmp.setOffer_name(res.getString("offer_name"));
                tmp.setCost(res.getDouble("cost"));
                tmp.setValidity(res.getInt("validity"));
                tmp.setValidity_unit(res.getString("validity_unit"));
                tmp.setPriority(res.getInt("priority"));
                ls.add(tmp);
                count++;
            }
            for (Offer o : ls) {
                o.setCount(count);
            }
            return ls;
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public static ArrayList<Offer> findOffersByDate(Connection conn, int idOffer, int month, String year) throws Exception {
        ArrayList<Offer> ls = new ArrayList<>();
        ResultSet res = null;
        PreparedStatement ps = null;
        int count = 0;

        try {
            ps = conn.prepareStatement("SELECT * FROM v_offer_date WHERE month = ? AND year = ? AND id_offer = ?");
            ps.setInt(1, month);
            ps.setInt(2, Integer.parseInt(year));
            ps.setInt(3, idOffer);
            res = ps.executeQuery();
            while (res.next()) {
                Offer tmp = new Offer();
                tmp.setId_offer(res.getInt("id_offer"));
                tmp.setOffer_name(res.getString("offer_name"));
                tmp.setCost(res.getDouble("cost"));
                tmp.setValidity(res.getInt("validity"));
                tmp.setValidity_unit(res.getString("validity_unit"));
                tmp.setPriority(res.getInt("priority"));
                ls.add(tmp);
                count++;
            }
            for (Offer o : ls) {
                o.setCount(count);
            }
            return ls;
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public static ArrayList<OfferStatistics> findOfferStatistics(Connection conn, String year) throws Exception {
        ArrayList<OfferStatistics> result = new ArrayList<>();
        ArrayList<Offer> lsAll = Offer.findAll();
        for (Offer o : lsAll) {
            OfferStatistics tmp = new OfferStatistics();
            tmp.setOffer(o);
            int dataOffer[] = new int[12];
            for (int i = 0; i < 12; i++) {
                ArrayList<Offer> lsOffer = findOffersByDate(conn, o.getId_offer(), i + 1, year);
                int count = 0;
                if (lsOffer.size() > 0) {
                    count = lsOffer.get(lsOffer.size() - 1).getCount();
                }
                dataOffer[i]=count;
            }
            tmp.setOfferCount(dataOffer);
            result.add(tmp);
        }
        return result;
    }

    public static ArrayList<OfferStatistics> findOfferStatistics(String year) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return OfferStatistics.findOfferStatistics(conn, year);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

}
