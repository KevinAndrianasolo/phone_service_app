package com.phone.utils;

public class Helper {

    public static String formatNumber(String number) throws Exception {
        String numberFormatted = "";
        if (10 > number.length()) {
            throw new Exception("PhoneNumberException : un numéro doit être composé d'au moins 10 chiffres.");
        } else {
            numberFormatted += number.substring(0, 3) + " " + number.substring(3, 5) + " " + number.substring(5, 8) + " " + number.substring(8);

        }
        return numberFormatted;
    }

    public static String getNumberPrefix(String number) {
        String prefix = Helper.getNumberPrefixAtN(number, 4);
        return prefix;
    }

    public static String get261Prefix(String number) {
        String prefix = Helper.getNumberPrefixAtN(number, 5);
        return prefix;
    }

    public static String getNumberPrefixAtN(String number, int n) {
        String prefix = number.substring(0, n - 1);
        return prefix;
    }

    /*
    Corps d'un find
     */
//        ResultSet res = null;
//        PreparedStatement ps = null;
//        try {
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            if (res != null) {
//                res.close();
//            }
//            if (ps != null) {
//                ps.close();
//            }
//        }
    
    /*
    Corps d'un update/delete
    */
    //        PreparedStatement ps = null;
//        try {
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            if (ps != null) {
//                ps.close();
//            }
//        }
}
