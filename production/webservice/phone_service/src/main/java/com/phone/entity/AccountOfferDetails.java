/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class AccountOfferDetails {
    private int id_account;
    private Offer offer;
    private AccountOffer account_offer;
    private ArrayList<OfferItem> offer_items_remaining_values = new ArrayList<OfferItem>();

    public AccountOfferDetails() {
    }

    public AccountOfferDetails(int id_account, Offer offer, AccountOffer account_offer, ArrayList<OfferItem> offer_items_remaining_values) {
        this.id_account = id_account;
        this.offer = offer;
        this.account_offer = account_offer;
        this.offer_items_remaining_values = offer_items_remaining_values;
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public AccountOffer getAccount_offer() {
        return account_offer;
    }

    public void setAccount_offer(AccountOffer account_offer) {
        this.account_offer = account_offer;
    }

    public ArrayList<OfferItem> getOffer_items_remaining_values() {
        return offer_items_remaining_values;
    }

    public void setOffer_items_remaining_values(ArrayList<OfferItem> offer_items_remaining_values) {
        this.offer_items_remaining_values = offer_items_remaining_values;
    }

    @Override
    public String toString() {
        return "AccountOfferDetails{" + "id_account=" + id_account + ", offer=" + offer + ", account_offer=" + account_offer + ", offer_items_remaining_values=" + offer_items_remaining_values + '}';
    }
    
    public static ArrayList<AccountOfferDetails> findAccountOfferDetailsOf(int id_account) throws Exception{
        ArrayList<AccountOfferRemainingValue> accountOfferRemainingValue = AccountOfferRemainingValue.findAccountOfferRemainingValueOf(id_account);
        return toAccountOfferDetails(accountOfferRemainingValue);
    }
    public static ArrayList<AccountOfferDetails> toAccountOfferDetails(ArrayList<AccountOfferRemainingValue> accountOfferRemainingValue){
        
        ArrayList<AccountOfferDetails> accountOfferDetails = new ArrayList<AccountOfferDetails>();
        for(int i=0; i<accountOfferRemainingValue.size();i++){
            int indice = -1;
            for(int j=0; j<accountOfferDetails.size();j++){
                if(accountOfferDetails.get(j).getAccount_offer().getIdAccountOffer() == accountOfferRemainingValue.get(i).getId_account_offer()){
                    indice = j;
                }
            }
            double value = accountOfferRemainingValue.get(i).getValue();
            String unit = accountOfferRemainingValue.get(i).getUnit();
            String offer_type_description = accountOfferRemainingValue.get(i).getOffer_type_description();
            int id_account = accountOfferRemainingValue.get(i).getId_account();
            String offer_name = accountOfferRemainingValue.get(i).getOffer_name();
            int id_offer = accountOfferRemainingValue.get(i).getId_offer();
            int id_offer_type = accountOfferRemainingValue.get(i).getId_offer_type();
            int id_account_offer = accountOfferRemainingValue.get(i).getId_account_offer();
            Timestamp purchase_date = accountOfferRemainingValue.get(i).getPurchase_date();
            Timestamp expiration_date = accountOfferRemainingValue.get(i).getExpiration_date();

            OfferItem offerItem = new OfferItem();
            offerItem.setValue(value);
            offerItem.setUnit(unit);
            offerItem.setOffer_type_description(offer_type_description);
            offerItem.setId_offer(id_offer);
            offerItem.setId_offer_type(id_offer_type);
            if(indice != -1){
                accountOfferDetails.get(indice).getOffer_items_remaining_values().add(offerItem);
            }
            else{
                
                
                Offer offer = new Offer();
                offer.setId_offer(id_offer);
                offer.setOffer_name(offer_name);
                
                AccountOffer account_offer = new AccountOffer();
                account_offer.setIdAccount(id_account);
                account_offer.setIdAccountOffer(id_account_offer);
                account_offer.setIdOffer(id_offer);
                account_offer.setExpirationDate(expiration_date);
                account_offer.setPurchaseDate(purchase_date);
                
                AccountOfferDetails accountOfferDetailsTmp = new AccountOfferDetails();
                accountOfferDetailsTmp.setId_account(id_account);
                accountOfferDetailsTmp.setOffer(offer);
                accountOfferDetailsTmp.setAccount_offer(account_offer);
                accountOfferDetailsTmp.getOffer_items_remaining_values().add(offerItem);
                accountOfferDetails.add(accountOfferDetailsTmp);
            }
        }
        return accountOfferDetails;
    }
    
}
