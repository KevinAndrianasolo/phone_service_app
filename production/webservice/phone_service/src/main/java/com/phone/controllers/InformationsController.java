package com.phone.controllers;

import com.phone.entity.Informations;
import com.phone.utils.Response;
import com.phone.utils.ResponseBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InformationsController {

    @GetMapping("/Informations")
    public Response find() throws Exception {
        try {
            return ResponseBuilder.Success(Informations.find());
        } catch (Exception e) {
            return ResponseBuilder.Error(e);
        }
    }
}
