/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.utils;

/**
 *
 * @author admin
 */
public class ResponseBuilder {
    public static Response Success(Object data){
        Meta meta = new Meta("200", "OK");
        return new Response(meta, data);
    }
    public static Response Error(Exception e){
        Meta meta = new Meta("500", e.getMessage());
        return new Response(meta, null);
    }
    public static Response Error(String err_message, String status){
        Meta meta = new Meta(status, err_message);
        return new Response(meta, null);
    }
}
