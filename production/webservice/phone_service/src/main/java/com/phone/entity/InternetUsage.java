/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phone.entity;

import com.phone.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author admin
 */
public class InternetUsage {
    private int id_internet_usage;
    private int id_account;
    private String website;
    private double consommation;

    public InternetUsage() {
    }

    public InternetUsage(int id_internet_usage, int id_account, String website, double consommation) {
        this.id_internet_usage = id_internet_usage;
        this.id_account = id_account;
        this.website = website;
        this.consommation = consommation;
    }

    public int getId_internet_usage() {
        return id_internet_usage;
    }

    public void setId_internet_usage(int id_internet_usage) {
        this.id_internet_usage = id_internet_usage;
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public double getConsommation() {
        return consommation;
    }

    public void setConsommation(double consommation) {
        this.consommation = consommation;
    }

    @Override
    public String toString() {
        return "InternetUsage{" + "id_internet_usage=" + id_internet_usage + ", id_account=" + id_account + ", website=" + website + ", consommation=" + consommation + '}';
    }
    
    public void save() throws Exception {

        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            this.save(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void save(Connection conn) throws Exception {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("insert into Internet_usage(id_account, website, consommation) values (?,?,?)");
            ps.setInt(1, this.getId_account());
            ps.setString(2, this.getWebsite());
            ps.setDouble(3, this.getConsommation());
            
            ps.executeUpdate();
            
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
}
