package com.phone.entity;

import com.phone.utils.DBUtils;
import com.phone.utils.NotificationManager;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;

public class MobileMoneyTransaction {

    private int idMobileMoneyTransaction;
    private int idMobileMoneyAccount;
    private int idMobileMoneyTransactionType;
    private double amount;
    private Timestamp transactionDate;
    private int status = 0;

    public MobileMoneyTransaction() {
    }

    public int getIdMobileMoneyTransaction() {
        return idMobileMoneyTransaction;
    }

    public void setIdMobileMoneyTransaction(int idMobileMoneyTransaction) {
        this.idMobileMoneyTransaction = idMobileMoneyTransaction;
    }

    public int getIdMobileMoneyAccount() {
        return this.idMobileMoneyAccount;
    }

    public void setIdMobileMoneyAccount(int idMobileMoneyAccount) {
        this.idMobileMoneyAccount = idMobileMoneyAccount;
    }

    public int getIdMobileMoneyTransactionType() {
        return idMobileMoneyTransactionType;
    }

    public void setIdMobileMoneyTransactionType(int idMobileMoneyTransactionType) {
        this.idMobileMoneyTransactionType = idMobileMoneyTransactionType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Timestamp getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Timestamp transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public TransactionStatus getTransactionStatus() {
        return new TransactionStatus(this.getStatus()
        );
    }

    public Account getAccount() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.getAccount(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public Account getAccount(Connection conn) throws Exception {
        return Account.findById(this.getMobileMoneyAccount().getIdAccount());
    }

    public TransactionType getTransactionType(Connection conn) throws Exception {
        return TransactionType.findByType(this.getIdMobileMoneyTransactionType());
    }

    public MobileMoneyAccount getMobileMoneyAccount() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.findMobileMoneyAccount(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static MobileMoneyTransaction findById(Connection conn, int id) throws Exception {

        ResultSet res = null;
        PreparedStatement ps = null;
        MobileMoneyTransaction transaction = new MobileMoneyTransaction();
        try {
            ps = conn.prepareStatement("SELECT * FROM mobile_money_transaction WHERE id_mobile_money_transaction = ? LIMIT 1");
            ps.setInt(1, id);
            res = ps.executeQuery();
            while (res.next()) {
                transaction.setIdMobileMoneyAccount(res.getInt("id_mobile_money_account"));
                transaction.setAmount(res.getDouble("amount"));
                transaction.setStatus(res.getInt("status"));
                transaction.setIdMobileMoneyTransaction(id);
                transaction.setTransactionDate(res.getTimestamp("transaction_date"));
                transaction.setIdMobileMoneyTransactionType(res.getInt("id_mobile_money_transaction_type"));
            }
            return transaction;
        } catch (Exception e) {
            throw e;
        } finally {
            if (res != null) {
                res.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public MobileMoneyAccount findMobileMoneyAccount(Connection conn) throws Exception {
        return MobileMoneyAccount.findById(conn, this.getIdMobileMoneyAccount());
    }

    public boolean verify() throws Exception {
        if (amount < 0) {
            throw new Exception("Le montant de la transaction est invalide.");
        }
        return true;
    }

    public void save(Connection conn, MobileMoneyAccount mobileMoneyAccount) throws Exception {
        this.verify();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            double amount = this.getIdMobileMoneyTransactionType() == 2 ? -1 * this.getAmount() : this.getAmount();
            double remaining = amount + mobileMoneyAccount.getAccountBalance();
            if (remaining < 0) {
                throw new Exception("Le solde de votre compte Mobile money est insuffisant. Solde actuel : " + mobileMoneyAccount.getAccountBalance() + " Ariary.");
            }

            ps = conn.prepareStatement("INSERT INTO mobile_money_transaction(id_mobile_money_account, id_mobile_money_transaction_type, amount, status) VALUES (?,?,?, ?)");
            ps.setInt(1, this.getIdMobileMoneyAccount());
            ps.setInt(2, this.getIdMobileMoneyTransactionType());
            ps.setDouble(3, this.getAmount());
            ps.setDouble(4, this.getStatus());

            ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public int save(Connection conn) throws Exception {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("INSERT INTO mobile_money_transaction (id_mobile_money_account,id_mobile_money_transaction_type,amount) VALUES (?,?,?)");
            ps.setInt(1, this.getIdMobileMoneyAccount());
            ps.setInt(2, this.getIdMobileMoneyTransactionType());
            ps.setDouble(3, this.getAmount());
            return ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    public int save() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.save(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void save(MobileMoneyAccount mobileMoneyAccount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            this.save(conn, mobileMoneyAccount);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }


    public void accept() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            this.accept(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void decline() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            this.decline(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void accept(Connection conn) throws Exception {
        this.updateStatus(conn, TransactionStatus.VALIDATED);
        MobileMoneyAccount mobileMoneyAccount = this.findMobileMoneyAccount(conn);
        int id_account = mobileMoneyAccount.getIdAccount();
        
        String title = "Notification venant de PhoneServiceApp";
        String body = "Votre transaction a été validé avec succès. Le solde actuel sur votre compte Mobile Money est de : "+mobileMoneyAccount.getAccountBalance()+" Ariary.";
        FCMNotificationBody notification_body = new FCMNotificationBody(title, body);
        
        NotificationManager.sendNotification(notification_body, id_account, conn);
    }

    public void decline(Connection conn) throws Exception {
        this.updateStatus(conn, TransactionStatus.REJECTED);
        MobileMoneyAccount mobileMoneyAccount = this.findMobileMoneyAccount(conn);
        int id_account = mobileMoneyAccount.getIdAccount();
        
        String title = "Notification venant de PhoneServiceApp";
        String body = "Votre transaction a été decliné. Le solde actuel sur votre compte Mobile Money est de : "+mobileMoneyAccount.getAccountBalance()+" Ariary.";
        FCMNotificationBody notification_body = new FCMNotificationBody(title, body);
        
        NotificationManager.sendNotification(notification_body, id_account, conn);
    } 

    public void updateStatus(Connection conn, int status) throws Exception {
        this.setStatus(status);
        this.update(conn);
    }

    public static ArrayList<MobileMoneyTransaction> findPendingTransactions() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findPendingTransactions(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<MobileMoneyTransaction> findPendingTransactions(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<MobileMoneyTransaction> ls = new ArrayList<>();
        try {
            ps = conn.prepareStatement("SELECT * FROM mobile_money_transaction WHERE status = 0 order by transaction_date asc");
            rs = ps.executeQuery();
            while (rs.next()) {
                MobileMoneyTransaction tmp = new MobileMoneyTransaction();
                tmp.setIdMobileMoneyTransaction(rs.getInt("id_mobile_money_transaction"));
                tmp.setIdMobileMoneyAccount(rs.getInt("id_mobile_money_account"));
                tmp.setIdMobileMoneyTransactionType(rs.getInt("id_mobile_money_transaction_type"));
                tmp.setAmount(rs.getDouble("amount"));
                tmp.setStatus(rs.getInt("status"));
                tmp.setTransactionDate(rs.getTimestamp("transaction_date"));
                ls.add(tmp);
            }
            return ls;

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public TransactionType getTransactionType() {
        return TransactionType.findByType(this.getIdMobileMoneyTransactionType());
    }

    public int update(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("UPDATE mobile_money_transaction SET status = ? "
                    + "WHERE id_mobile_money_transaction = ?");
            ps.setInt(1, this.getStatus());
            ps.setInt(2, this.getIdMobileMoneyTransaction());

            return ps.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public int update() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return this.update(conn);
        } catch (Exception e) {
            throw e;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }

    }

    public static ArrayList<MobileMoneyTransaction> findAll() throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findAll(conn);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public static ArrayList<MobileMoneyTransaction> findAll(Connection conn) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<MobileMoneyTransaction> ls = new ArrayList<>();
        try {
            ps = conn.prepareStatement("SELECT * FROM mobile_money_transaction");
            rs = ps.executeQuery();
            while (rs.next()) {
                MobileMoneyTransaction tmp = new MobileMoneyTransaction();
                tmp.setIdMobileMoneyTransaction(rs.getInt("id_mobile_money_transaction"));
                tmp.setIdMobileMoneyAccount(rs.getInt("id_mobile_money_account"));
                tmp.setIdMobileMoneyTransactionType(rs.getInt("id_mobile_money_transaction_type"));
                tmp.setAmount(rs.getDouble("amount"));
                tmp.setStatus(rs.getInt("status"));
                tmp.setTransactionDate(rs.getTimestamp("transaction_date"));
                ls.add(tmp);
            }
            return ls;

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static ArrayList<MobileMoneyTransaction> findTransactionsByIdAccount(Connection conn, int idAccount) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<MobileMoneyTransaction> ls = new ArrayList<>();
        try {
            ps = conn.prepareStatement("SELECT m_transaction.*,m_account.id_account FROM mobile_money_transaction m_transaction JOIN mobile_money_account m_account ON m_account.id_mobile_money_account = m_transaction.id_mobile_money_account WHERE m_account.id_account = ?");
            ps.setInt(1, idAccount);
            rs = ps.executeQuery();
            while (rs.next()) {
                MobileMoneyTransaction tmp = new MobileMoneyTransaction();
                tmp.setIdMobileMoneyTransaction(rs.getInt("id_mobile_money_transaction"));
                tmp.setIdMobileMoneyAccount(rs.getInt("id_mobile_money_account"));
                tmp.setIdMobileMoneyTransactionType(rs.getInt("id_mobile_money_transaction_type"));
                tmp.setAmount(rs.getDouble("amount"));
                tmp.setStatus(rs.getInt("status"));
                tmp.setTransactionDate(rs.getTimestamp("transaction_date"));
                ls.add(tmp);
            }
            return ls;

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public static ArrayList<MobileMoneyTransaction> findTransactionsByIdAccount(int idAccount) throws Exception {
        Connection conn = null;
        try {
            conn = DBUtils.getPsqlConnection();
            return findTransactionsByIdAccount(conn, idAccount);

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

}
