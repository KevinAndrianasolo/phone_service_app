import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  constructor(private http : HttpClient) { }
  public FindAll(){
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');

    return this.http.get(BASE_URL+"/Offers", { headers : headers } );
  }
}
