import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountOfferService {

  constructor(private http : HttpClient) { }

  public Save(account_offer){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.post(BASE_URL+"/AccountOffer", account_offer, { headers : headers } );
  }
  public GetAccountOfferRemaingValues(){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.get(BASE_URL+"/AccountOfferDetails", { headers : headers } );
  }
  
}
