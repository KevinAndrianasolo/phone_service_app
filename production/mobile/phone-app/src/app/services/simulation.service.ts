import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SimulationService {
  constructor(private http : HttpClient) { }

  public Simulate(consumption : any){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.post(BASE_URL+"/Consumption/Simulate", consumption, { headers : headers } );
  }
}
