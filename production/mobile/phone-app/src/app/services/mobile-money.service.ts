import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MobileMoneyService {

  constructor(private http : HttpClient) { }

  public SaveMobileMoneyTransaction(mobile_money_transaction : any){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.post(BASE_URL+"/MobileMoneyTransaction", mobile_money_transaction, { headers : headers } );
  }
  public findAllMobileMoneyTransactionType(){
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');

    return this.http.get(BASE_URL+"/MobileMoneyTransactionType", { headers : headers } );
  }
  public findMobileMoneyAccount(){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.get(BASE_URL+"/MobileMoneyAccount", { headers : headers } );
  }
}
