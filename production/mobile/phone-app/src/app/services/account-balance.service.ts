import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountBalanceService {

  constructor(private http : HttpClient) { }

  public SaveAccountBalanceTransaction(account_balance_transaction : any){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.post(BASE_URL+"/AccountBalanceTransaction", account_balance_transaction, { headers : headers } );
  }
  public findAccountBalance(){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.get(BASE_URL+"/AccountBalance", { headers : headers } );
  }
}
