import { TestBed } from '@angular/core/testing';

import { AccountOfferService } from './account-offer.service';

describe('AccountOfferService', () => {
  let service: AccountOfferService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountOfferService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
