import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
// import FCM
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Inbox',
      url: '/folder/Inbox',
      icon: 'mail'
    },
    {
      title: 'Outbox',
      url: '/folder/Outbox',
      icon: 'paper-plane'
    },
    {
      title: 'Favorites',
      url: '/folder/Favorites',
      icon: 'heart'
    },
    {
      title: 'Archived',
      url: '/folder/Archived',
      icon: 'archive'
    },
    {
      title: 'Trash',
      url: '/folder/Trash',
      icon: 'trash'
    },
    {
      title: 'Spam',
      url: '/folder/Spam',
      icon: 'warning'
    }
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // subscribe to a topic
      // FCM.subscribeToTopic('Deals');

      // get FCM token
      FCM.getToken().then(token => {
        console.log("------ THIS IS THE TOKEN ----------");
        console.log(token);
        localStorage.fcm_token = token;
        console.log("----- END OF TOKEN ---------------");
      });

      // ionic push notification example
      FCM.onNotification().subscribe(data => {
        console.log("------ THIS IS THE DATA ----------");
        console.log(data);
        console.log("------ END OF THE DATA ----------");
        if (data.wasTapped) {
          console.log('Received in background');
        } else {
          console.log('Received in foreground');
        }
      });

      // refresh the FCM token
      FCM.onTokenRefresh().subscribe(token => {
        console.log("------ THIS IS THE NEW TOKEN ----------");
        console.log(token);
        console.log("------ THIS IS THE END OF NEW TOKEN ----------");
      });

      // unsubscribe from a topic
      // FCM.unsubscribeFromTopic('offers');

    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
