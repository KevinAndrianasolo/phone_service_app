import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AccountOfferService } from 'src/app/services/account-offer.service';
import { OfferService } from 'src/app/services/offer.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.page.html',
  styleUrls: ['./offer.page.scss'],
})
export class OfferPage implements OnInit {
  onLoading : boolean = false;
  offers : [] = [];
  constructor(private offerService : OfferService, private alertController : AlertController, private accountOfferService : AccountOfferService) { }

  ngOnInit() {
    this.initAllOffers();
  }

  public initAllOffers(callback=null){
    if(callback==null) this.onLoading = true;
    this.offerService.FindAll().subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.offers = temp['DATA'];
          console.log(this.offers);
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
        if(callback!=null) callback();
        this.onLoading = false;
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
        this.onLoading = false;
      }
    );
  }
  purchase_offer(offer){
    this.confirm_purchase(offer);
  }
  async confirm_purchase(offer) {
    let alert = await this.alertController.create({
      header: 'Confirmation',
      message : `Voulez-vous vraiment acheter l'offre ${offer.offer_name} pour ${offer.cost} Ariary ?`,
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {}
        },
        {
          text: 'Confirmer',
          handler: data => {
            this.insertAccountOffer(offer);
            
          }
        }
      ]
    });
    alert.present();
  }
  public insertAccountOffer(offer){
    let account_offer : any= {
      "idOffer" : offer.id_offer,
      "withMobileMoney" : offer.withMobileMoney == undefined ? false : offer.withMobileMoney
    };
    
    this.accountOfferService.Save(account_offer).subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          let alert = await this.alertController.create({
            header : 'Succès',
            message: "L'offre a été acheté avec succès.",
            buttons: ['OK']
          });
          alert.present();
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
      }
    );
  }
  doRefresh(event) {
    this.initAllOffers(function(){
      event.target.complete();
    });
  }
}
