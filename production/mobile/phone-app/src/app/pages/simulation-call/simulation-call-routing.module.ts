import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SimulationCallPage } from './simulation-call.page';

const routes: Routes = [
  {
    path: '',
    component: SimulationCallPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SimulationCallPageRoutingModule {}
