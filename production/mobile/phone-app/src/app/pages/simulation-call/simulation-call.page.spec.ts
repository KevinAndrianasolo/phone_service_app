import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SimulationCallPage } from './simulation-call.page';

describe('SimulationCallPage', () => {
  let component: SimulationCallPage;
  let fixture: ComponentFixture<SimulationCallPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimulationCallPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SimulationCallPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
