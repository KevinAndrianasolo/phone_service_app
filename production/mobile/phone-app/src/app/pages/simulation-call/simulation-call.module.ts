import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SimulationCallPageRoutingModule } from './simulation-call-routing.module';

import { SimulationCallPage } from './simulation-call.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SimulationCallPageRoutingModule
  ],
  declarations: [SimulationCallPage]
})
export class SimulationCallPageModule {}
