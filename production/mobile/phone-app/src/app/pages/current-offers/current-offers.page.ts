import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AccountOfferService } from 'src/app/services/account-offer.service';

@Component({
  selector: 'app-current-offers',
  templateUrl: './current-offers.page.html',
  styleUrls: ['./current-offers.page.scss'],
})
export class CurrentOffersPage implements OnInit {
  current_offers : any[] = [];
  onLoading : boolean = false;
  constructor(private accountOfferService : AccountOfferService, private alertController : AlertController) { }

  ngOnInit() {
    this.initCurrentOffers();
  }
  public initCurrentOffers(callback = null){
    if(callback==null) this.onLoading = true;
    this.accountOfferService.GetAccountOfferRemaingValues().subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.current_offers = temp['DATA'];
          console.log(this.current_offers);
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
        if(callback!=null) callback();
        this.onLoading = false;
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
        this.onLoading = false;
      }
    );
  }
  doRefresh(event) {
    this.initCurrentOffers(function(){
      event.target.complete();
    });
  }
}
