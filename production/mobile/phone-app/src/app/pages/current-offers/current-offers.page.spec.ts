import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CurrentOffersPage } from './current-offers.page';

describe('CurrentOffersPage', () => {
  let component: CurrentOffersPage;
  let fixture: ComponentFixture<CurrentOffersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentOffersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CurrentOffersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
