import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CurrentOffersPageRoutingModule } from './current-offers-routing.module';

import { CurrentOffersPage } from './current-offers.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CurrentOffersPageRoutingModule
  ],
  declarations: [CurrentOffersPage]
})
export class CurrentOffersPageModule {}
