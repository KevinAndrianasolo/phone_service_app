import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CurrentOffersPage } from './current-offers.page';

const routes: Routes = [
  {
    path: '',
    component: CurrentOffersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrentOffersPageRoutingModule {}
