import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { MobileMoneyService } from 'src/app/services/mobile-money.service';

@Component({
  selector: 'app-mobile-money',
  templateUrl: './mobile-money.page.html',
  styleUrls: ['./mobile-money.page.scss'],
})
export class MobileMoneyPage implements OnInit {
  mobile_money_transaction : any = {amount: 0, id_mobile_money_transaction_type : 1};
  soumettreLoading : boolean = false;
  mobile_money : any = {accountBalance : "-"};
  mobile_money_transaction_type : any[] = [];
  constructor(private mobileMoneyService : MobileMoneyService, private alertController : AlertController) { }

  ngOnInit() {
    this.initMobileMoneyAccount();
    this.initMobileMoneyTransactionType();
  }

  public SaveMobileMoneyTransaction(){
    this.soumettreLoading = true;
    this.mobileMoneyService.SaveMobileMoneyTransaction(this.mobile_money_transaction).subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          let alert = await this.alertController.create({
            header : 'Succès',
            message: "Votre transaction a été enregistré avec succès, et est déjà en attente.",
            buttons: ['OK']
          });
          alert.present();
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
        this.soumettreLoading = false;
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
        this.soumettreLoading = false;
      }
    );
  }
  public initMobileMoneyTransactionType(callback=null){
    this.mobileMoneyService.findAllMobileMoneyTransactionType().subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.mobile_money_transaction_type = temp['DATA'];
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
        if(callback!=null) callback();
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
      }
    );
  }
  public initMobileMoneyAccount(callback=null){
    this.mobileMoneyService.findMobileMoneyAccount().subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.mobile_money = temp['DATA'];
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
        if(callback!=null) callback();
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
      }
    );
  }
  doRefresh(event) {
    this.initMobileMoneyAccount(function(){
      event.target.complete();
    });
  }
}
