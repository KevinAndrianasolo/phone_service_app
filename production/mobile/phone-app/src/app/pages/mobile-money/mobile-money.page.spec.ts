import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MobileMoneyPage } from './mobile-money.page';

describe('MobileMoneyPage', () => {
  let component: MobileMoneyPage;
  let fixture: ComponentFixture<MobileMoneyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileMoneyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MobileMoneyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
