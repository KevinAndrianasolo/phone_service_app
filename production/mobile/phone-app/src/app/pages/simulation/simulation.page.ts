import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { OfferTypeUnitService } from 'src/app/services/offer-type-unit.service';
import { OfferTypeService } from 'src/app/services/offer-type.service';
import { SimulationService } from 'src/app/services/simulation.service';

@Component({
  selector: 'app-simulation',
  templateUrl: './simulation.page.html',
  styleUrls: ['./simulation.page.scss'],
})
export class SimulationPage implements OnInit {
  simulation : any = {};
  offer_type_list : any[] = [];
  offer_type_unit_list : any[] = [];
  current_offer_type_unit : any[] = [];
  onLoading : boolean = false;
  is_call : boolean = false;
  constructor(private offerTypeService : OfferTypeService, private offerTypeUnitService : OfferTypeUnitService, private alertController : AlertController, private simulationService : SimulationService) { }

  ngOnInit() {
    this.initOfferType();
    this.initOfferTypeUnit();
  }
  public Simulate(){
    this.onLoading = true;
    console.log(this.simulation);
    this.simulationService.Simulate(this.simulation).subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          let alert = await this.alertController.create({
            header : 'Succès',
            message: "Votre simulation a été enregistré avec succès.",
            buttons: ['OK']
          });
          alert.present();
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
        this.onLoading = false;
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
        this.onLoading = false;
      }
    );
  }
  public initOfferType(){
    this.offerTypeService.FindAll().subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.offer_type_list = temp['DATA'];
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
      }
    );
  }
  public initOfferTypeUnit(){
    this.offerTypeUnitService.FindAll().subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.offer_type_unit_list = temp['DATA'];
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
      }
    );
  }
  public refresh_offer_type_unit(){
    this.current_offer_type_unit = this.offerTypeUnitService.getOfferTypeMatchesWith(this.simulation.id_offer_type, this.offer_type_unit_list);
    console.log(this.current_offer_type_unit);
  }
  public refresh_is_call(){
    this.is_call = !this.is_call;
  }
}
