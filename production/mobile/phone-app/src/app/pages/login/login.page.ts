import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AccountService } from 'src/app/services/account.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  on_submit : boolean = false;
  account : any = { phoneNumber : "" , password : ""}
  constructor( private router : Router, private accountService : AccountService, private alertController : AlertController) { }

  ngOnInit() {
  }
  Login(){
    console.log(this.account);
    this.on_submit = true;
    this.accountService.Login(this.account).subscribe(async (res : any)=>{
      console.log(res);
      let temp = res as Response;
      if(temp['META']['status'] == "200"){
        localStorage.account_token = temp['DATA'];
        this.router.navigateByUrl("/app/offer");
      }
      else{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: temp['META']['message'],
          buttons: ['OK']
        });
        alert.present();
      }
      this.on_submit = false;
    },
    async (err : any)=>{
      //this.popupService.showError(err.message);
      let alert = await this.alertController.create({
        header : 'Erreur',
        message: err.message,
        buttons: ['OK']
      });
      alert.present();
      this.on_submit = false;
    });
  }
}
