import { Component, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';

@Component({
  selector: 'app-tabs-page',
  templateUrl: './tabs-page.page.html',
  styleUrls: ['./tabs-page.page.scss'],
})
export class TabsPagePage implements OnInit {
  @ViewChild('appTabs') appTabs : IonTabs
  constructor() { }

  ngOnInit() {
  }
  ionViewDidEnter(){
  }

}
