import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyAccountPage } from '../my-account/my-account.page';
import { OfferPage } from '../offer/offer.page';

import { TabsPagePage } from './tabs-page.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPagePage,
    children: [
      {
        path: 'my_account',
        loadChildren: () => import('../my-account/my-account.module').then( m => m.MyAccountPageModule)
      },
      {
        path: 'offer',
        loadChildren: () => import('../offer/offer.module').then( m => m.OfferPageModule)
      },
      {
        path: 'current_offers',
        loadChildren: () => import('../current-offers/current-offers.module').then( m => m.CurrentOffersPageModule)
      },
      {
        path: 'mobile_money',
        loadChildren: () => import('../mobile-money/mobile-money.module').then( m => m.MobileMoneyPageModule)
      },
      {
        path: 'account_balance',
        loadChildren: () => import('../account-balance/account-balance.module').then( m => m.AccountBalancePageModule)
      },
      {
        path: 'simulation_call',
        loadChildren: () => import('../simulation-call/simulation-call.module').then( m => m.SimulationCallPageModule)
      },
      {
        path: 'simulation_message',
        loadChildren: () => import('../simulation-message/simulation-message.module').then( m => m.SimulationMessagePageModule)
      },
      {
        path: 'simulation_internet',
        loadChildren: () => import('../simulation-internet/simulation-internet.module').then( m => m.SimulationInternetPageModule)
      },
      {
        path: 'simulation',
        loadChildren: () => import('../simulation/simulation.module').then( m => m.SimulationPageModule)
      },
      {
        path: 'about',
        loadChildren: () => import('../about/about.module').then( m => m.AboutPageModule)
      },
      {
        path: '',
        redirectTo: '/app/my_account',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPagePageRoutingModule {}
