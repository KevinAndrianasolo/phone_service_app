import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccountBalancePageRoutingModule } from './account-balance-routing.module';

import { AccountBalancePage } from './account-balance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccountBalancePageRoutingModule
  ],
  declarations: [AccountBalancePage]
})
export class AccountBalancePageModule {}
