import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AccountBalanceService } from 'src/app/services/account-balance.service';

@Component({
  selector: 'app-account-balance',
  templateUrl: './account-balance.page.html',
  styleUrls: ['./account-balance.page.scss'],
})
export class AccountBalancePage implements OnInit {

  account_balance_transaction : any = {amount: 0};
  acheterLoading : boolean = false;
  account_balance : any = {accountBalance : "-"};
  constructor(private accountBalanceService : AccountBalanceService, private alertController : AlertController) { }

  ngOnInit() {
    this.initAccountBalance();
  }

  public SaveAccountBalanceTransaction(){
    this.acheterLoading = true;
    this.accountBalanceService.SaveAccountBalanceTransaction(this.account_balance_transaction).subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          await this.initAccountBalance();
          let alert = await this.alertController.create({
            header : 'Succès',
            message: "Votre compte a été crédité avec succès",
            buttons: ['OK']
          });
          alert.present();
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
        this.acheterLoading = false;
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
        this.acheterLoading = false;
      }
    );
  }
  public initAccountBalance(callback=null){
    this.accountBalanceService.findAccountBalance().subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.account_balance = temp['DATA'];
        }
        else{
          let alert = await this.alertController.create({
            header : 'Erreur',
            message: temp['META']['message'],
            buttons: ['OK']
          });
          alert.present();
        }        
        if(callback!=null) callback();
      },
      async (err : any)=>{
        let alert = await this.alertController.create({
          header : 'Erreur',
          message: err.message,
          buttons: ['OK']
        });
        alert.present();
      }
    );
  }
  doRefresh(event) {
    this.initAccountBalance(function(){
      event.target.complete();
    });
  }
}
