import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AccountBalancePage } from './account-balance.page';

describe('AccountBalancePage', () => {
  let component: AccountBalancePage;
  let fixture: ComponentFixture<AccountBalancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountBalancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AccountBalancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
