import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountBalancePage } from './account-balance.page';

const routes: Routes = [
  {
    path: '',
    component: AccountBalancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountBalancePageRoutingModule {}
