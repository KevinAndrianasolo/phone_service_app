import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SimulationInternetPageRoutingModule } from './simulation-internet-routing.module';

import { SimulationInternetPage } from './simulation-internet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SimulationInternetPageRoutingModule
  ],
  declarations: [SimulationInternetPage]
})
export class SimulationInternetPageModule {}
