import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SimulationInternetPage } from './simulation-internet.page';

describe('SimulationInternetPage', () => {
  let component: SimulationInternetPage;
  let fixture: ComponentFixture<SimulationInternetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimulationInternetPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SimulationInternetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
