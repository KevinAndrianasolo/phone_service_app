import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SimulationMessagePage } from './simulation-message.page';

const routes: Routes = [
  {
    path: '',
    component: SimulationMessagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SimulationMessagePageRoutingModule {}
