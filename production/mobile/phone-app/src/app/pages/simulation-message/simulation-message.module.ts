import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SimulationMessagePageRoutingModule } from './simulation-message-routing.module';

import { SimulationMessagePage } from './simulation-message.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SimulationMessagePageRoutingModule
  ],
  declarations: [SimulationMessagePage]
})
export class SimulationMessagePageModule {}
