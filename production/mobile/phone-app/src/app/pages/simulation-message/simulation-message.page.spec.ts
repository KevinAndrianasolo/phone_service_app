import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SimulationMessagePage } from './simulation-message.page';

describe('SimulationMessagePage', () => {
  let component: SimulationMessagePage;
  let fixture: ComponentFixture<SimulationMessagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimulationMessagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SimulationMessagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
