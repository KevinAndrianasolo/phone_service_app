# Pages :
- ionic generate page pages/my_account
- ionic generate page pages/simulation
- ionic generate page pages/offer
- ionic generate page pages/current_offers  
- ionic generate page pages/mobile_money
- ionic generate page pages/account_balance
- ionic generate page pages/simulation_internet
- ionic generate page pages/simulation_message
- ionic generate page pages/simulation_call

# Services :
- ionic generate service services/account
- ionic generate service services/account_offer
- ionic generate service services/mobile_money
- ionic generate service services/offer_type
- ionic generate service services/offer_type_unit

# FIREBASE CLOUD MESSAGING:
ionic cordova plugin add cordova-plugin-fcm-with-dependecy-updated
ionic cordova plugin add cordova-plugin-firebase-messaging
npm install @ionic-native/fcm


cordova platform remove android
cordova platform add android@8.0.0

ionic cordova plugin add cordova-plugin-androidx
ionic cordova plugin add cordova-plugin-androidx-adapter