import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from 'src/app/interfaces/Response';
import { AccountService } from 'src/app/services/account.service';
import { PopupService } from 'src/app/services/popup.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  is_connected : boolean = false;
  constructor(private accountService : AccountService, private popupService : PopupService, private router : Router) { }

  ngOnInit(): void {
    this.init();
  }
  public init(){
    this.is_connected = localStorage.account_token != undefined ? true : false;
  }
  public goToLogin(){
    this.router.navigateByUrl("/login");
  }
  public logout(){
    this.accountService.Logout().subscribe(
      (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          localStorage.clear();
          this.router.navigateByUrl("/login");
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }        
      },
      (err : any)=>{
        this.popupService.showError(err.message);
      }
    );
  }
}
