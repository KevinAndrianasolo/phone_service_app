import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HistoricsService } from 'src/app/services/historics.service';
import { Response } from 'src/app/interfaces/Response';
import { PopupService } from 'src/app/services/popup.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Month } from 'src/app/interfaces/Month';
import { MonthService } from 'src/app/services/month.service';

@Component({
  selector: 'app-historics',
  templateUrl: './historics-list.component.html',
  styleUrls: ['./historics-list.component.scss']
})
export class HistoricsListComponent implements OnInit {
  onLoading: boolean = false;
  historics: any[] = [];
  id_account: any = -1;
  months: Month[] = [];
  form : any = { month : 1, year : 2021, filter : "all"};


  constructor(private activatedRoute: ActivatedRoute, private historicsService: HistoricsService, private monthService: MonthService, private popupService: PopupService, private router: Router) { }

  ngOnInit(): void {
    this.initAllMonths();
    console.log(this.months);
    this.id_account = this.activatedRoute.snapshot.paramMap.get("id_account");

    this.activatedRoute.queryParams.subscribe(params => {
      this.form.month = params['month'] == null ? new Date().getMonth()+ 1 : params['month'];
      this.form.year = params['year'] == "" || params['year'] == undefined ? new Date().getFullYear() : params['year'];
      this.form.filter = params['filter'] == "" || params['filter'] == undefined ? "all" : params['filter'];
    });

    this.initHistorics(this.form.filter);
  }
  public initAllMonths(){
    this.months = this.monthService.GetMonths();
  }
  public initHistorics(filter : string) {
    this.onLoading = true;
    switch(filter){
      case "all":
        this.historicsService.FindCallsByIdAccount(this.form.month, this.form.year).subscribe(
          (res: any) => {
            let temp = res as Response;
            if (temp['META']['status'] == "200") {
              this.historics = temp['DATA'];
            }
            else {
              this.popupService.showError(temp['META']['message']);
            }
            this.onLoading = false;
          },
          (err: any) => {
            this.popupService.showError(err.message);
            this.onLoading = false;
          }
        );
        break;
      case "default" :
        this.historicsService.FindCallsDefaultOperatorByIdAccount(this.form.month, this.form.year).subscribe(
          (res: any) => {
            let temp = res as Response;
            if (temp['META']['status'] == "200") {
              this.historics = temp['DATA'];
            }
            else {
              this.popupService.showError(temp['META']['message']);
            }
            this.onLoading = false;
          },
          (err: any) => {
            this.popupService.showError(err.message);
            this.onLoading = false;
          }
        );
        break;
        case "other" :
          this.historicsService.FindCallsOtherOperatorByIdAccount(this.form.month, this.form.year).subscribe(
            (res: any) => {
              let temp = res as Response;
              if (temp['META']['status'] == "200") {
                this.historics = temp['DATA'];
              }
              else {
                this.popupService.showError(temp['META']['message']);
              }
              this.onLoading = false;
            },
            (err: any) => {
              this.popupService.showError(err.message);
              this.onLoading = false;
            }
          );
          break;
          case "international" :
            this.historicsService.FindCallsInternationalByIdAccount(this.form.month, this.form.year).subscribe(
              (res: any) => {
                let temp = res as Response;
                if (temp['META']['status'] == "200") {
                  this.historics = temp['DATA'];
                }
                else {
                  this.popupService.showError(temp['META']['message']);
                }
                this.onLoading = false;
              },
              (err: any) => {
                this.popupService.showError(err.message);
                this.onLoading = false;
              }
            );
            break;
            default :
            this.historicsService.FindCallsByIdAccount(this.form.month, this.form.year).subscribe(
              (res: any) => {
                let temp = res as Response;
                if (temp['META']['status'] == "200") {
                  this.historics = temp['DATA'];
                }
                else {
                  this.popupService.showError(temp['META']['message']);
                }
                this.onLoading = false;
              },
              (err: any) => {
                this.popupService.showError(err.message);
                this.onLoading = false;
              }
            );
            break;
    }
  
  }
  public onSubmit() {
    const month: number = this.form["month"];
    const year: string = this.form["year"];
    const filter: string = this.form["filter"];
    this.router.navigateByUrl('/historics?month=' + month + '&year=' + year + '&filter=' + filter);
    this.initHistorics(this.form.filter);
  }
}
