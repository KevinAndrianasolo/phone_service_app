import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricsListComponent } from './historics-list.component';

describe('HistoricsComponent', () => {
  let component: HistoricsListComponent;
  let fixture: ComponentFixture<HistoricsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoricsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
