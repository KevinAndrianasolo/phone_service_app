import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from 'src/app/interfaces/Response';
import { AccountService } from 'src/app/services/account.service';
import { PopupService } from 'src/app/services/popup.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  account: any = {};
  onLogin : boolean = false;
  constructor(private accountService : AccountService, private popupService : PopupService, private router : Router) { }

  ngOnInit(): void {
  }

  public Login(){
    this.onLogin = true;
    this.accountService.Login(this.account).subscribe((res : any)=>{
        console.log(res);
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          localStorage.account_token = temp['DATA'];
          this.router.navigateByUrl("/historics");
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }
        this.onLogin = false;
      },
      (err : any)=>{
        this.popupService.showError(err.message);
        this.onLogin = false;
      });
  }
}
