import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from 'src/app/interfaces/Response';
import { AccountService } from 'src/app/services/account.service';
import { PopupService } from 'src/app/services/popup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  account: any = {};
  onSignup : boolean = false;
  constructor(private accountService : AccountService, private popupService : PopupService, private router : Router) { }

  ngOnInit(): void {
  }

  public Signup(){
    this.onSignup = true;
    this.accountService.Signup(this.account).subscribe((res : any)=>{
        console.log(res);
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          localStorage.account_token = temp['DATA'];
          this.router.navigateByUrl("/historics");
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }
        this.onSignup = false;
      },
      (err : any)=>{
        this.popupService.showError(err.message);
        this.onSignup = false;
      });
  }
}
