import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HistoricsService {

  constructor(private http : HttpClient) { }

  public FindCallsByIdAccount(month : number, year : string){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);
    return this.http.get(BASE_URL+"/Statistics/Call?month="+month+"&year="+year+"", { headers : headers } );
 
  }


  public FindCallsDefaultOperatorByIdAccount(month : number, year : string){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);
    return this.http.get(BASE_URL+"/Statistics/Call/DefaultOperator?month="+month+"&year="+year+"", { headers : headers } );
 
  }


  public FindCallsOtherOperatorByIdAccount(month : number, year : string){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);
    return this.http.get(BASE_URL+"/Statistics/Call/OtherOperator?month="+month+"&year="+year+"", { headers : headers } );
 
  }


  public FindCallsInternationalByIdAccount(month : number, year : string){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);
    return this.http.get(BASE_URL+"/Statistics/Call/International?month="+month+"&year="+year+"", { headers : headers } );

  }
}
