import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../components/dialog/dialog.component';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  constructor(public dialog: MatDialog) { }

  public showError(message : string){
    this.dialog.open(DialogComponent ,{
      data : {
        title : "Erreur",
        icon:"error_outline",
        class:"error",
        message : message
      }
    });
  }
  public showSuccess(message : string){
    this.dialog.open(DialogComponent ,{
      data : {
        title : "Succès",
        icon:"check_circle_outline",
        class:"success",
        message : message
      }
    });
  }
}
