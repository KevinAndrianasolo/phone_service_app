import { Injectable } from '@angular/core';
import {Month} from 'src/app/interfaces/Month';
@Injectable({
  providedIn: 'root'
})
export class MonthService {

  constructor() { }

  public GetMonths(){
    var months : Month[]=[];
      months[0] = {
        month_number:1,
        value:'Janvier'
      };
      months[1] = {
        month_number:2,
        value:'Février'
      };
      months[2] = {
        month_number:3,
        value:'Mars'
      };
      months[3] = {
        month_number:4,
        value:'Avril'
      };
      months[4] = {
        month_number:5,
        value:'Mai'
      };
      months[5] = {
        month_number:6,
        value:'Juin'
      };
      months[6] = {
        month_number:7,
        value:'Juillet'
      };
      months[7] = {
        month_number:8,
        value:'Août'
      };
      months[8] = {
        month_number:9,
        value:'Septembre'
      };
      months[9] = {
        month_number:10,
        value:'Octobre'
      };
      months[10] = {
        month_number:11,
        value:'Novembre'
      };
      months[11] = {
        month_number:12,
        value:'Décembre'
      };
    return months;
  }
}
