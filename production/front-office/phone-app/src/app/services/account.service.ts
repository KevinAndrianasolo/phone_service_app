import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private http : HttpClient) { }

  public Login(account : any){
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');

    return this.http.post(BASE_URL+"/Authentification/Account/Login", account, { headers : headers } );
  }
  public Signup(account : any){
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');

    return this.http.post(BASE_URL+"/Authentification/Account/Signup", account, { headers : headers } );
  }
  public Logout(){
    let token = localStorage.account_token==undefined ? "" : localStorage.account_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.post(BASE_URL+"/Authentification/Account/Logout", {}, { headers : headers } );
  }
}
