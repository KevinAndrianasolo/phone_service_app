## APP
ng new phone-app

## MODULES :
- Angular Material : ng add @angular/material

## Pages :
- Login : ng generate component pages/login
- Historics-list : ng generate component pages/historics-list
- SIGNUP : ng generate component pages/signup
- Footer : ng generate component components/footer

## Services : 
- ng generate service services/login
- ng generate service services/popup
- ng generate service services/historics
- ng generate service services/month


Add below specific configuration in package.json and npm install.

{
  "scripts": {
    "postinstall": "ngcc"
  }
}

In tsconfig.json set the enableIvy option to false
{
  ..., 
  "angularCompilerOptions": {
    "enableIvy": false
  }
}