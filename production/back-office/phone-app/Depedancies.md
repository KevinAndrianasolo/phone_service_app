## APP
ng new phone-app

## MODULES :
- Angular Material : ng add @angular/material
- Angular Apex Chart : npm install apexcharts ng-apexcharts --save


## Pages :
- Offer : ng generate component pages/offers
- Pricing : ng generate component pages/pricing
- Login : ng generate component pages/login
- New Offer : ng generate component pages/offers_new
- Update Offer : ng generate component pages/offers_update
- Validation Transaction  : ng generate component pages/transaction
- Statisitic : ng generate component pages/statistics
- Footer : ng generate component components/footer

## Services : 
- ng generate service services/offer
- ng generate service services/pricing
- ng generate service services/admin
- ng generate service services/popup
- ng generate service services/offer_type
- ng generate service services/offer_type_unit

- ng generate service services/transaction
- ng generate service services/statistics



Add below specific configuration in package.json and npm install.

{
  "scripts": {
    "postinstall": "ngcc"
  }
}

In tsconfig.json set the enableIvy option to false
{
  ..., 
  "angularCompilerOptions": {
    "enableIvy": false
  }
}
