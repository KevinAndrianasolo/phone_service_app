import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { OffersNewComponent } from './pages/offers-new/offers-new.component';
import { OffersUpdateComponent } from './pages/offers-update/offers-update.component';
import { OffersComponent } from './pages/offers/offers.component';
import { PricingComponent } from './pages/pricing/pricing.component';
import { StatisticsComponent } from './pages/statistics/statistics.component';
import { TransactionComponent } from './pages/transaction/transaction.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "offers",
    component: OffersComponent
  }, {
    path: "pricing",
    component: PricingComponent
  }, {
    path: "login",
    component: LoginComponent
  },
  {
    path: "offers/new",
    component: OffersNewComponent
  },
  {
    path: "offers/update/:id_offer",
    component: OffersUpdateComponent
  },
  {
    path:"transaction",
    component : TransactionComponent
  },
  {
    path:"statistics",
    component : StatisticsComponent
  }
,
  {
    path: "statistics",
    component: StatisticsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
