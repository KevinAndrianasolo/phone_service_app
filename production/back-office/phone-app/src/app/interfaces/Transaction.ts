export interface Transaction{
  
    "idMobileMoneyTransaction": number,
    "idMobileMoneyAccount": number,
    "idMobileMoneyTransactionType": number,
    "amount": number,
    "transactionDate": Date,
    "status": number,
    "transactionStatus": {
        "status": number,
        "description": string
    },
    "account": {
        "idAccount": number,
        "name": string,
        "cin": string,
        "birthDate": Date,
        "phoneNumber": string,
        "password": string
    },
    "transactionType": {
        "type": number,
        "description": string
    },
    "mobileMoneyAccount": {
        "idMobileMoneyAccount": number,
        "idAccount": number,
        "accountBalance": number,
        "secreCode": string,
        "account": {
            "idAccount": number,
            "name": string,
            "cin": string,
            "birthDate": Date,
            "phoneNumber": string,
            "password": string
        }
    }
}