export interface Pricing{
    value : number,
    call_cost_faf : any,
    call_cost_operator : any,
    call_cost_other_operator : any,
    call_cost_international : any,
    message_cost : any,
    internet_cost : any
}