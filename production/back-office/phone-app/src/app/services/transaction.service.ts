import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BASE_URL } from 'src/environments/environment';
import { Transaction } from '../interfaces/Transaction';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http : HttpClient) { }

  public ValidateStatus(transaction : any ){
    let token = localStorage.admin_token==undefined ? "" : localStorage.admin_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);
      return this.http.post(BASE_URL+"/MobileMoney/Transaction/Accept", transaction, { headers : headers } );

  }

  public DeclineStatus(transaction : any ){
    let token = localStorage.admin_token==undefined ? "" : localStorage.admin_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);
  return this.http.post(BASE_URL+"/MobileMoney/Transaction/Decline", transaction, { headers : headers } );
  }

  public FindPendingTransactions(){
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');
    return this.http.get(BASE_URL+"/MobileMoney/PendingTransactions/", { headers : headers } );
 
  }
}
