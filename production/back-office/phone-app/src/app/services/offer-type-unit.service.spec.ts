import { TestBed } from '@angular/core/testing';

import { OfferTypeUnitService } from './offer-type-unit.service';

describe('OfferTypeUnitService', () => {
  let service: OfferTypeUnitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfferTypeUnitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
