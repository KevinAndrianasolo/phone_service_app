import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';
import { StatisticObject } from '../interfaces/StatisticObject';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  constructor(private http: HttpClient) { }

  public FindCallsDefaultOperator(year: string) {
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');
    return this.http.get(BASE_URL + "/Statistics/Call/All/DefaultOperator?year=" + year, { headers: headers });

  }

  public FindCallsOtherOperator(year: string) {
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');
    return this.http.get(BASE_URL + "/Statistics/Call/All/OtherOperator?year=" + year, { headers: headers });

  }

  public FindCallsInternational(year: string) {
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');
    return this.http.get(BASE_URL + "/Statistics/Call/All/International?year=" + year, { headers: headers });
  }

  public FindOffers(year: string) {
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');
    return this.http.get(BASE_URL + "/Statistics/Offer?year=" + year, { headers: headers });
  }

  public Extract_Duration(tables: any[]) {
    let data: number[] = [];
    for (let i = 0; i < tables.length; i++) {
      data[i] = tables[i].callDuration as number;
    }
    return data;
  }

  public ExtractStatisticObject(tab: any[]) {
    let data: StatisticObject[] = [];
    for (let i = 0; i < tab.length; i++) {
      data[i] = 
      {
         name: tab[i].offer.offer_name, 
        data: tab[i].offerCount 
      };
    }
    return data;
  }
  public MonthValue(own: number[], other: number[], international: number[]) {
    let value: number = 0;
    for (let i = 0; i < 12; i++) {
      value += own[i] + other[i] + international[i];
    }
    return value;
  }
}
