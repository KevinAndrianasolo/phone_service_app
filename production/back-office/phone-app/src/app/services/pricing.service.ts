import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PricingService {
  constructor(private http : HttpClient) { }

  public Save(pricing : any){
    let token = localStorage.admin_token==undefined ? "" : localStorage.admin_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);


    pricing["last_update"] = Date.now();
    return this.http.post(BASE_URL+"/Pricing", pricing, { headers : headers } );
  }
  public GetCurrentPricing(){
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');

    return this.http.get(BASE_URL+"/Pricing", { headers : headers } );
  }
}
