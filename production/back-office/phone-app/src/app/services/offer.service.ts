import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  constructor(private http : HttpClient) { }

  public Save(offer : any){
    let token = localStorage.admin_token==undefined ? "" : localStorage.admin_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.post(BASE_URL+"/Offers", offer, { headers : headers } );
  }
  
  public Update(id_offer:number, offer : any){
    let token = localStorage.admin_token==undefined ? "" : localStorage.admin_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.put(BASE_URL+"/Offers/"+id_offer, offer, { headers : headers } );
  }
  public FindAll(){
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');

    return this.http.get(BASE_URL+"/Offers", { headers : headers } );
  }
  public Delete(id_offer : number){
    let token = localStorage.admin_token==undefined ? "" : localStorage.admin_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.delete(BASE_URL+"/Offers/"+id_offer, { headers : headers } );
  }
  public FindById(id_offer : number){
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');

    return this.http.get(BASE_URL+"/Offers/"+id_offer, { headers : headers } );
  }
}
