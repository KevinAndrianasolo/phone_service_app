import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OfferTypeUnitService {

  constructor(private http : HttpClient ) {}

  public FindAll(){
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');

    return this.http.get(BASE_URL+"/OfferTypeUnit", { headers : headers } );
  }
  public getOfferTypeMatchesWith(id_offer_type : number, offer_type_unit_list : any[]){
    let rep = [];
    for(let i=0;i<offer_type_unit_list.length; i++){
      if(offer_type_unit_list[i].id_offer_type == id_offer_type){
        rep.push(offer_type_unit_list[i]);
      }
    }
    return rep;
  }
}
