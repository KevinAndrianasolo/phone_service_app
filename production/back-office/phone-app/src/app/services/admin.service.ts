import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  constructor(private http : HttpClient) { }

  public Login(admin : any){
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json');

    return this.http.post(BASE_URL+"/Authentification/Admin/Login", admin, { headers : headers } );
  }

  public Logout(){
    let token = localStorage.admin_token==undefined ? "" : localStorage.admin_token;
    let headers = new HttpHeaders()
      .set('Content-type', 'application/json')
      .set('Authorization', token);

    return this.http.post(BASE_URL+"/Authentification/Admin/Logout", {}, { headers : headers } );
  }
}
