import { Component, OnInit } from '@angular/core';
import { Response } from 'src/app/interfaces/Response';
import { OfferService } from 'src/app/services/offer.service';
import { PopupService } from 'src/app/services/popup.service';
import { PricingService } from 'src/app/services/pricing.service';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {
  offers : any[] = [];
  onLoading : boolean = false;
  constructor(private offerService : OfferService ,private popupService : PopupService){}
  ngOnInit(): void {
    this.initAllOffers();
  }
  public initAllOffers(){
    this.onLoading = true;
    this.offerService.FindAll().subscribe(
      (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.offers = temp['DATA'];
          console.log(this.offers);
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }        
        this.onLoading = false;
      },
      (err : any)=>{
        this.popupService.showError(err.message);
        this.onLoading = false;

      }
    );
  }
  public delete(id_offer : number){
    this.offerService.Delete(id_offer).subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          await this.initAllOffers();
          this.popupService.showSuccess(`L'offre n°${id_offer}  a été supprimé avec succès.`);
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }        
        this.onLoading = false;
      },
      (err : any)=>{
        this.popupService.showError(err.message);
        this.onLoading = false;

      }
    );
  }
}
