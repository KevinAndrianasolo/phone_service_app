import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-offers-update',
  templateUrl: './offers-update.component.html',
  styleUrls: ['./offers-update.component.scss']
})
export class OffersUpdateComponent implements OnInit {
  id_offer : any = -1;
  
  constructor(private activatedRoute : ActivatedRoute) { }

  ngOnInit(): void {
    this.id_offer = this.activatedRoute.snapshot.paramMap.get("id_offer");
    console.log(this.id_offer);
  }
}
