import { Component, ViewChild } from "@angular/core";
import { Response } from 'src/app/interfaces/Response';
import { PopupService } from 'src/app/services/popup.service';
import { StatisticObject } from 'src/app/interfaces/StatisticObject';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexStroke,
  ApexMarkers,
  ApexYAxis,
  ApexGrid,
  ApexTitleSubtitle,
  ApexLegend,
  ApexResponsive,
  ApexFill,
  ApexPlotOptions
} from "ng-apexcharts";
import { StatisticsService } from "src/app/services/statistics.service";
import { NgForm } from "@angular/forms";

export type callChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  markers: ApexMarkers;
  colors: string[];
  yaxis: ApexYAxis;
  grid: ApexGrid;
  legend: ApexLegend;
  title: ApexTitleSubtitle;
};
export type offerChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  responsive: ApexResponsive[];
  xaxis: ApexXAxis;
  legend: ApexLegend;
  fill: ApexFill;
};
@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent {
  onLoading: boolean = false;
  year: string = "2021";
  calltitle: string = "Statistiques des appels : " + this.year;
  defaultData: number[] = [];
  otherData: number[] = [];
  internationalData: number[] = [];
  offerData: StatisticObject[] = [];
  @ViewChild("call-chart")
  callchart!: ChartComponent;
  @ViewChild("offer-chart")
  offerChart!: ChartComponent;

  public callchartOptions!: Partial<callChartOptions>;
  public offerchartOptions!: Partial<offerChartOptions>;


  constructor(private statisticsService: StatisticsService, private popupService: PopupService) {
    this.Init();

  }
  public Init() {
    this.InitCall();
    this.InitOffer();
  }

  public async InitCall() {
    try {
      await this.InitInternational();
      await this.InitDefault();
      await this.InitOther();
      this.InitCallChart();
      this.onLoading = true;
    } catch (err) {
      this.popupService.showError(err.message);
      this.onLoading = false;

    }
  }
  public async InitInternational() {
    let res = await this.statisticsService.FindCallsInternational(this.year).toPromise();
    let temp = res as Response;
    if (temp['META']['status'] == "200") {
      this.internationalData = this.statisticsService.Extract_Duration(temp['DATA']);
      console.log(this.internationalData);
    }
    else {
      this.popupService.showError(temp['META']['message']);
    }
  }
  public async InitDefault() {
    let res = await this.statisticsService.FindCallsDefaultOperator(this.year).toPromise();
    let temp = res as Response;
    if (temp['META']['status'] == "200") {
      this.defaultData = this.statisticsService.Extract_Duration(temp['DATA']);
      console.log(this.defaultData);
    }
    else {
      this.popupService.showError(temp['META']['message']);
    }
  }
  public async InitOther() {
    let res = await this.statisticsService.FindCallsOtherOperator(this.year).toPromise();
    let temp = res as Response;
    if (temp['META']['status'] == "200") {
      this.otherData = this.statisticsService.Extract_Duration(temp['DATA']);
      console.log(this.defaultData);
    }
    else {
      this.popupService.showError(temp['META']['message']);
    }
  }

  public InitCallChart() {
    this.callchartOptions = {
      series: [
        {
          name: "Mon opérateur",
          data: this.defaultData
        },
        {
          name: "Autres opérateurs",
          data: this.otherData
        },
        {
          name: "Internationaux",
          data: this.internationalData
        }
      ],
      chart: {
        height: 350,
        type: "line",
        dropShadow: {
          enabled: true,
          color: "#000",
          top: 18,
          left: 7,
          blur: 10,
          opacity: 0.2
        },
        toolbar: {
          show: false
        }
      },
      colors: ["#77B6EA", "#545454"],
      dataLabels: {
        enabled: true
      },
      stroke: {
        curve: "smooth"
      },
      title: {
        text: "",
        align: "left"
      },
      grid: {
        borderColor: "#e7e7e7",
        row: {
          colors: ["#f3f3f3", "transparent"],
          opacity: 0.5
        }
      },
      markers: {
        size: 1
      },
      xaxis: {
        categories: ["Jan", "Fev", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Sept", "Oct", "Nov", "Déc"],
        title: {
          text: "Mois"
        }
      },
      yaxis: {
        title: {
          text: "Durée"
        },
        min: 10,
        max: 4000
      },
      legend: {
        position: "top",
        horizontalAlign: "right",
        floating: true,
        offsetY: -25,
        offsetX: -5
      }
    };
  }
  public async InitOffer() {
    try {
      await this.InitOfferData();
      this.InitOfferChart();
    } catch (err) {
      this.popupService.showError(err.message);
      this.onLoading = false;
    }
  }
  public async InitOfferData() {
    let res = await this.statisticsService.FindOffers(this.year).toPromise();
    let temp = res as Response;
    if (temp['META']['status'] == "200") {
      this.offerData = this.statisticsService.ExtractStatisticObject(temp['DATA']);
      console.log(this.defaultData);
    }
    else {
      this.popupService.showError(temp['META']['message']);
    }
  }
  public InitOfferChart() {
    this.offerchartOptions = {
      series: this.offerData,
      chart: {
        type: "bar",
        height: 350,
        stacked: true,
        toolbar: {
          show: true
        },
        zoom: {
          enabled: true
        }
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: "bottom",
              offsetX: -10,
              offsetY: 0
            }
          }
        }
      ],
      plotOptions: {
        bar: {
          horizontal: false
        }
      },
      xaxis: {
        type: "category",
        categories: ["Jan", "Fev", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Sept", "Oct", "Nov", "Déc"],
      },
      legend: {
        position: "right",
        offsetY: 40
      },
      fill: {
        opacity: 1
      }
    };
  }
  public onSubmit(form: NgForm) {
    const year = form.value['year'];
    this.year = year;
    this.Init();
  }
}
