import { Component, OnInit } from '@angular/core';
import { Response } from 'src/app/interfaces/Response';
import { OfferService } from 'src/app/services/offer.service';
import { PopupService } from 'src/app/services/popup.service';
import { PricingService } from 'src/app/services/pricing.service';

@Component({
  selector: 'app-offers-new',
  templateUrl: './offers-new.component.html',
  styleUrls: ['./offers-new.component.scss']
})
export class OffersNewComponent implements OnInit {
  constructor(private offerService : OfferService, private pricingService : PricingService, private popupService : PopupService){}
  ngOnInit(): void {
  }
}
