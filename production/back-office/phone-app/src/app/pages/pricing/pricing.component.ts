import { Component, OnInit } from '@angular/core';
import { Response } from 'src/app/interfaces/Response';
import { PopupService } from 'src/app/services/popup.service';
import { PricingService } from 'src/app/services/pricing.service';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {
  pricingLoaded : boolean = false;
  onSave : boolean = false;
  pricing : any = {
    "call_cost_faf": 0,
    "call_cost_operator":0,
    "call_cost_other_operator" : 0,
    "call_cost_international": 0,
    "message_cost" : 0,
    "internet_cost" : 0,
    "default_cost" : 0
  };
  constructor(private pricingService : PricingService, private popupService : PopupService) { }

  ngOnInit(): void {
    this.initCurrentPricing();
  }
  public initCurrentPricing(){
    this.pricingService.GetCurrentPricing().subscribe(
      (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.pricing = temp['DATA'];
          this.pricingLoaded = true;
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }   
      },
      (err : any)=>{
        console.log(err);
      }
    );
  }
  public savePricing(){
    console.log(this.pricing);
    this.onSave = true;
    this.pricingService.Save(this.pricing).subscribe(
      (res)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          // Popup success
          let message = "Les modifications ont été enregistrés.";
          this.popupService.showSuccess(message);
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }
        this.onSave = false;
      },
      (err)=>{
        this.popupService.showError(err.message);
        this.onSave = false;
      });
  }
}
