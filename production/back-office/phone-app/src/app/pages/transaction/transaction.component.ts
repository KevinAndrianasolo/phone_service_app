import { Component, OnInit } from '@angular/core';
import { TransactionService } from 'src/app/services/transaction.service';
import {Transaction} from 'src/app/interfaces/Transaction';
import { Response } from 'src/app/interfaces/Response';
import { PopupService } from 'src/app/services/popup.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
  transactions : Transaction [] = [];
  onLoading : boolean = false;
  constructor(private transactionService : TransactionService,private popupService : PopupService) { }

  ngOnInit(): void {
    this.initTransaction();
  }
  public initTransaction(){
    this.onLoading = true;
    this.transactionService.FindPendingTransactions().subscribe(
      (res:any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.transactions = temp['DATA'];
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }        
        this.onLoading = false;
      },
      (err:any)=>{
        this.popupService.showError(err.message);
        this.onLoading = false;
      }
    );
  }

  public decline(transaction : any){
    this.transactionService.DeclineStatus(transaction).subscribe(
      (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          // Popup success
          let message = "La transaction a été rejeté avec succès.";
          
          this.initTransaction();
          this.popupService.showSuccess(message);
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }
      },
      (err : any)=>{
        this.popupService.showError(err.message);
      });
    
  }

  public validate(transaction : any){
    this.transactionService.ValidateStatus(transaction).subscribe(
      (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          // Popup success
          let message = "La transaction a été validé avec succès.";
          
          this.initTransaction();
          this.popupService.showSuccess(message);
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }
      },
      (err : any)=>{
        this.popupService.showError(err.message);
      });
  }
}
