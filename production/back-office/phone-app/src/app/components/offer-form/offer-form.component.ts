import { Component, Input, OnInit } from '@angular/core';
import { Response } from 'src/app/interfaces/Response';
import { OfferTypeUnitService } from 'src/app/services/offer-type-unit.service';
import { OfferTypeService } from 'src/app/services/offer-type.service';
import { OfferService } from 'src/app/services/offer.service';
import { PopupService } from 'src/app/services/popup.service';
import { PricingService } from 'src/app/services/pricing.service';

@Component({
  selector: 'app-offer-form',
  templateUrl: './offer-form.component.html',
  styleUrls: ['./offer-form.component.scss']
})
export class OfferFormComponent implements OnInit {
  @Input() type : string = "";
  @Input() id_offer : number = -1;
 
  pricingLoaded : boolean = false;
  offer : any = {};
  onSave : boolean = false;
  offer_type_list : any[] = [];
  offer_type_unit_list : any[] = [];
  offer_item_offer_type_unit_list : any[]= [];
  showOffer : boolean = true;
  constructor(private offerService : OfferService, private pricingService : PricingService, private popupService : PopupService, private offerTypeService : OfferTypeService, private offerTypeUnitService : OfferTypeUnitService){}
  ngOnInit(): void {

    this.initOffer();
    this.initOfferTypeUnit();
    this.initOfferType();

    if(this.type === "update") {
      this.initOfferAndPricing(this.id_offer, () => {
        if(this.offer.offer_pricing == null) {
          this.initCurrentPricing();
        }
        else this.pricingLoaded = true;
      });
      
      
    }else{
      this.initCurrentPricing();
    }
  }
  public isUpdate(){
    return this.type === "update";
  }
  
  public isNew(){
    return this.type === "new";
  }
  public initOfferType(){
    this.offerTypeService.FindAll().subscribe(
      (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.offer_type_list = temp['DATA'];
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }        
      },
      (err : any)=>{
        this.popupService.showError(err.message);
      }
    );
  }
  public initOfferTypeUnit(){
    this.offerTypeUnitService.FindAll().subscribe(
      (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.offer_type_unit_list = temp['DATA'];
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }        
      },
      (err : any)=>{
        this.popupService.showError(err.message);
      }
    );
  }
  public initOfferAndPricing(id_offer : number, callback : any){
    this.offerService.FindById(id_offer).subscribe(
      async (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          if(temp['DATA'] != null){
            this.offer = temp['DATA'];
            this.refresh_offer_type_unit();
            if(callback != null) await callback();
            
            this.showOffer = true;
          }
          else{
            this.showOffer = false;
          }
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }       
      },
      (err : any)=>{
        this.popupService.showError(err.message);
        this.showOffer = false; 
      }
    );
  }
  public initOffer(){
    this.offer = {
        "id_offer" : 0,
        "offer_name" : "",
        "cost" : 0,
        "validity" : 0,
        "validity_unit" : "day",
        "priority" : 0, 
        "offer_pricing" : {
          "call_cost_faf": 0.5,
          "call_cost_operator":1,
          "call_cost_other_operator" : 3,
          "call_cost_international": 20,
          "message_cost" : 200,
          "internet_cost" : 50
      },
      "offer_items" : []
    };
  }
  public addOfferItem(){
    this.offer.offer_items.push({"id_offer_type":0, "value":0, "unit":""});
    this.offer_item_offer_type_unit_list.push([]);
  }
  public deleteOfferItem(indice : number){
    this.offer.offer_items.splice(indice, 1);
    this.offer_item_offer_type_unit_list.splice(indice, 1);
  }
  public initCurrentPricing(){
    this.pricingService.GetCurrentPricing().subscribe(
      (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          this.offer.offer_pricing = temp['DATA'];
          this.pricingLoaded = true;
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }        
      },
      (err : any)=>{
        this.popupService.showError(err.message);
      }
    );
  }
  public saveOffer(){
    this.onSave = true;
    this.offerService.Save(this.offer).subscribe(
      (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          // Popup success
          let message = "L'offre a été enregistré avec succès.";
          
          this.initOffer();
          this.popupService.showSuccess(message);
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }
        this.onSave = false;
      },
      (err : any)=>{
        this.popupService.showError(err.message);
        this.onSave = false;
      });
  }
  public updateOffer(){
    this.onSave = true;
    this.offerService.Update(this.id_offer, this.offer).subscribe(
      (res : any)=>{
        let temp = res as Response;
        if(temp['META']['status'] == "200"){
          // Popup success
          let message = "Les modifications été enregistré avec succès.";
          this.popupService.showSuccess(message);
        }
        else{
          this.popupService.showError(temp['META']['message']);
        }
        this.onSave = false;
      },
      (err : any)=>{
        this.popupService.showError(err.message);
        this.onSave = false;
      });
  }
  trackByFn(index : number, item : any) {
    return index;  
  }
  public refresh_offer_type_unit_of(i : number){
    let id_offer_type = this.offer.offer_items[i].id_offer_type;
    this.offer_item_offer_type_unit_list[i] = this.offerTypeUnitService.getOfferTypeMatchesWith(id_offer_type, this.offer_type_unit_list);
  }
  public  refresh_offer_type_unit(){
    for(let i=0;i<this.offer.offer_items.length;i++){
      this.offer_item_offer_type_unit_list.push([]);
      this.refresh_offer_type_unit_of(i);
    }
  }
}
