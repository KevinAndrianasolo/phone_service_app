drop table consumption cascade;

create table consumption(
    id_consumption serial not null,
    id_account serial not null,
    id_account_offer serial not null,
    id_offer_type serial not null,
    value numeric(10,3) not null,
    id_unit serial not null,
    consumption_date timestamp not null,
    constraint pk_consumption primary key(id_consumption),
    constraint fk_consumption_id_acc foreign key (id_account) references account(id_account),
    constraint fk_consumption_id_acc_offer foreign key (id_account_offer) references account_offer(id_account_offer),
    constraint fk_consumption_id_offer_type foreign key (id_offer_type) references offer_type(id_offer_type),
    constraint fk_consumption_id_unit foreign key (id_unit) references unit(id_unit)

);

CREATE OR REPLACE VIEW consumption_per_account_offer as 
    select id_account_offer, id_offer_type,sum(value) as value, id_unit from consumption group by id_account_offer, id_offer_type, id_unit;

CREATE OR REPLACE VIEW current_offers_remaining_value as
    select current_offers_initial_value.id_account ,
            current_offers_initial_value.id_account_offer ,
            current_offers_initial_value.id_offer  ,
            current_offers_initial_value.id_offer_type  ,
            current_offers_initial_value.value  - coalesce(consumption_per_account_offer.value, 0) as value,
            current_offers_initial_value.unit   ,
            current_offers_initial_value.priority 
     from current_offers_initial_value
        left join consumption_per_account_offer 
        on consumption_per_account_offer.id_account_offer = current_offers_initial_value.id_account_offer
        and consumption_per_account_offer.id_offer_type = current_offers_initial_value.id_offer_type;

create or replace view current_offers_remaining_value_details as
    select current_offers_remaining_value.*, offer.offer_name, account_offer.purchase_date, account_offer.expiration_date, offer_type.description as offer_type_description from current_offers_remaining_value 
    join offer on current_offers_remaining_value.id_offer = offer.id_offer
    join account_offer on current_offers_remaining_value.id_account_offer = account_offer.id_account_offer
    join offer_type on offer_type.id_offer_type = current_offers_remaining_value.id_offer_type;

-- Data pour test statistiques offres : 
insert into offer(id_offer,offer_name, cost, validity, validity_unit,priority) values (100,'Offre Test 1', 100, 1 , 'day', 3);
insert into offer_item(id_offer, id_offer_type, value, unit) values (100, 3, 20, 'Message');
insert into offer_item(id_offer, id_offer_type, value, unit) values (100, 4, 20, 'Mo');

insert into offer(id_offer,offer_name, cost, validity, validity_unit,priority) values (101,'Offre Test 2', 1000, 1 , 'day', 0);
insert into offer_item(id_offer, id_offer_type, value, unit) values (101, 1, 1000, 'Ariary');
insert into offer_item(id_offer, id_offer_type, value, unit) values (101, 2, 500, 'Ariary');
insert into offer_item(id_offer, id_offer_type, value, unit) values (101, 3, 20, 'Message');
insert into offer_item(id_offer, id_offer_type, value, unit) values (101, 4, 20, 'Mo');

-- Offre avec mois et date d'achat (si achat)
CREATE VIEW v_offer_date AS
SELECT o.*,month,year from offer o LEFT JOIN (SELECT id_offer,EXTRACT(MONTH FROM purchase_date) AS month, EXTRACT(YEAR FROM purchase_date) AS year
FROM account_offer) AS ao ON ao.id_offer = o.id_offer; 
alter table pricing add column default_cost numeric(10, 3) not null default 10;
alter table offer_pricing add column default_cost numeric(10, 3) not null default 10;

CREATE OR REPLACE View CurrentPricing as
    select Pricing.* from Pricing join (select max(last_update) as last_update from pricing) as t on Pricing.last_update = t.last_update;


select * from (select prefix from informations)as tmp cross join (select '03465' like CONCAT(tmp.prefix, '%')) as tmp2;

