create table fcm_user(
    id_fcm_user serial not null,
    id_account serial not null,
    token varchar not null,
    last_update TIMESTAMP not null default current_timestamp,
    constraint pk_fcm_user primary key(id_fcm_user),
    constraint fk_fcm_user_id_account foreign key (id_account) references account(id_account)
);

create or replace view current_fcm_user as
    select fcm_user.* from fcm_user join (select id_account, max(last_update) as last_update from fcm_user group by id_account) as tmp on fcm_user.id_account = tmp.id_account;


