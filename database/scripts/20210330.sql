create table operator(
    id_operator serial not null,
    prefix varchar(10) not null,
    operator_name varchar(50) not null,
    constraint pk_operator primary key(id_operator),
    constraint uk_operator_prefix unique (prefix),
    constraint uk_operator_name unique (operator_name)
);


create table informations(
    id_operator serial not null,
    group_name varchar(50) not null,
    balance varchar(50) not null,
    prefix varchar(50) not null, 
    constraint fk_informations_id_op foreign key (id_operator) references operator(id_operator),
    constraint uk_informations_id_operator unique (id_operator),
    constraint uk_informations_group_name unique (group_name)
);


create table admin(
    id_admin serial not null,
    name varchar(100) not null,
    birth_date date not null,
    username varchar(50) not null,
    password varchar(32) not null,
    constraint pk_admin primary key(id_admin),
    constraint uk_user_name unique (username)
);


create table admin_token(
    id_admin_token serial not null,
    id_admin serial not null,
    token varchar(32) not null,
    expiration_date timestamp not null,
    constraint pk_admin_token primary key(id_admin_token),
    constraint fk_acc_token_id_admin foreign key (id_admin) references admin(id_admin)

);

create table account(
    id_account serial not null,
    name varchar(100) not null,
    birth_date date not null,
    cin varchar(100) not null,
    phone_number varchar(50) not null,
    password varchar(32) not null,
    constraint pk_account primary key(id_account),
    constraint uk_acc_phone_number unique (phone_number)
);


create table account_token(
    id_account_token serial not null,
    id_account serial not null,
    token varchar(32) not null,
    expiration_date timestamp not null,
    constraint pk_account_token primary key(id_account_token),
    constraint fk_acc_token_id_acc foreign key (id_account) references account(id_account)

);


create table call(
    id_call serial not null,
    id_account serial not null,
    recipient_phone_number varchar(50) not null,
    duration bigint not null,
    date timestamp not null default current_timestamp,
    is_international boolean not null,
    constraint pk_call primary key(id_call),
    constraint fk_call_id_acc foreign key (id_account) references account(id_account)
);

create table message(
    id_message serial not null,
    id_account serial not null,
    recipient_phone_number varchar(50) not null ,
    message_body varchar(1000), 
    date timestamp not null default current_timestamp, 
    constraint pk_message primary key(id_message),
    constraint fk_message_id_acc foreign key (id_account) references account(id_account)
);

create table internet_usage(

    id_internet_usage serial not null,
    id_account serial not null,
    website varchar(50) not null,
    consommation numeric(10,3) not null,
    date timestamp not null default current_timestamp,
    constraint pk_internet_usage primary key(id_internet_usage),
    constraint fk_internet_usage_id_acc foreign key (id_account) references account(id_account)
);


create table faf_phone_number(
    id_faf_phone_number serial not null,
    id_account serial not null,
    faf_phone_number varchar(50) not null,
    constraint pk_faf primary key(id_faf_phone_number),
    constraint fk_faf_id_acc foreign key (id_account) references account(id_account)
);

create table account_balance(
    id_account_balance serial not null,
    id_account serial not null,
    account_balance numeric(15, 3) not null default 0,
    constraint pk_account_balance primary key(id_account_balance),
    constraint fk_acc_balance_id_acc foreign key (id_account) references account(id_account)
);

create table account_balance_transaction(
    id_account_balance_transaction serial not null,
    id_account_balance serial not null,
    amount numeric(15, 3) not null,
    transaction_date timestamp not null default current_timestamp,
    constraint pk_account_balance_transaction primary key(id_account_balance_transaction),
    constraint fk_acc_balance_id_acc_bal foreign key (id_account_balance) references account_balance(id_account_balance)
);
create table mobile_money_account(
    id_mobile_money_account serial not null,
    id_account serial not null,
    account_balance numeric(15, 3) not null default 0,
    secret_code varchar(32) not null,
    constraint pk_mobile_money_account primary key(id_mobile_money_account),
    constraint fk_mobile_money_id_acc foreign key (id_account) references account(id_account),
    constraint uk_mobileMoneyAcc unique (id_account)
);

create table mobile_money_transaction_type(
    id_mobile_money_transaction_type serial not null,
    mobile_money_transaction_type varchar(50) not null,
    constraint pk_mobile_money_transaction_type primary key(id_mobile_money_transaction_type),
    constraint uk_mobileMoneyTransType unique (mobile_money_transaction_type)
);

create table mobile_money_transaction(
    id_mobile_money_transaction serial not null,
    id_mobile_money_account serial not null,
    id_mobile_money_transaction_type  serial not null,
    amount numeric(15, 3) not null,
    transaction_date timestamp not null default current_timestamp,
    status int not null default 0,
    constraint pk_mobile_money_transaction primary key(id_mobile_money_transaction),
    constraint fk_mobile_money_transaction_id_acc foreign key (id_mobile_money_account) references mobile_money_account(id_mobile_money_account),
    constraint fk_mobile_money_transaction_type foreign key (id_mobile_money_transaction_type) references mobile_money_transaction_type(id_mobile_money_transaction_type)
);

create table offer_type(
    id_offer_type serial not null,
    description varchar(100) not null,
    constraint pk_offer_type primary key(id_offer_type),
    constraint uk_offer_type unique (description)
);  

create table offer(
    id_offer serial not null,
    offer_name varchar(50) not null ,
    cost decimal(10, 3) not null,
    validity int not null,
    validity_unit VARCHAR(25) not null,
    priority int not null default 0,
    constraint pk_offer primary key(id_offer),
    constraint uk_offer unique(offer_name)
);

create table offer_item(
    id_offer_item serial not null,
    id_offer serial not null,
    id_offer_type serial not null,
    value decimal(10, 3) not null,
    unit varchar(25) not null,
    constraint pk_offer_item primary key(id_offer_item),
    constraint fk_offer_item_id_offer foreign key (id_offer) references offer(id_offer),
    constraint fk_offer_item_id_offer_type foreign key (id_offer_type) references offer_type(id_offer_type)
);


create table account_offer(
    id_account_offer serial not null,
    id_account serial not null,
    id_offer serial not null,
    purchase_date timestamp not null,
    expiration_date timestamp not null,
    constraint pk_account_offer primary key(id_account_offer),
    constraint fk_account_offer_id_acc foreign key (id_account) references account(id_account),
    constraint fk_account_offer_id_offer foreign key (id_offer) references offer(id_offer)
);
create table consumption(
    id_consumption serial not null,
    id_account serial not null,
    id_account_offer serial not null,
    id_offer_type serial not null,
    value numeric(10,3) not null,
    unit varchar(25) not null,
    consumption_date timestamp not null,
    constraint pk_consumption primary key(id_consumption),
    constraint fk_consumption_id_acc foreign key (id_account) references account(id_account),
    constraint fk_consumption_id_acc_offer foreign key (id_account_offer) references account_offer(id_account_offer),
    constraint fk_consumption_id_offer_type foreign key (id_offer_type) references offer_type(id_offer_type)
);

create table offer_pricing(
    id_offer_pricing serial not null,
    id_offer serial not null,
    call_cost_faf numeric(10,3) not null,
    call_cost_operator numeric(10,3) not null,
    call_cost_other_operator numeric(10,3) not null,
    call_cost_international numeric(10,3) not null,
    message_cost numeric(10,3) not null,
    internet_cost numeric(10,3) not null,
    constraint pk_offer_pricing primary key(id_offer_pricing),
    constraint fk_offer_pricing_id_offer foreign key (id_offer) references offer(id_offer)
);

create table pricing(
    id_pricing serial not null,
    call_cost_faf numeric(10,3) not null,
    call_cost_operator numeric(10,3) not null,
    call_cost_other_operator numeric(10,3) not null,
    call_cost_international numeric(10,3) not null,
    message_cost numeric(10,3) not null,
    internet_cost numeric(10,3) not null,
    last_update timestamp not null default current_timestamp,
    constraint pk_pricing primary key(id_pricing)
);

create table unit(
    id_unit serial not null,
    name varchar(50) not null,
    constraint pk_unit primary key(id_unit),
    constraint uk_unit unique(name)
);

create table offer_type_unit(
    id_offer_type_unit serial not null,
    id_offer_type serial not null,
    id_unit serial not null,
    constraint pk_offer_type_unit primary key(id_offer_type_unit),
    constraint fk_offer_type_unit_id_offer_type foreign key (id_offer_type) references offer_type(id_offer_type),
    constraint fk_offer_type_unit_id_unit foreign key (id_unit) references unit(id_unit)
);



CREATE OR REPLACE VIEW offer_type_unit_details as 
    select offer_type_unit.*, unit.name from offer_type_unit join unit on offer_type_unit.id_unit = unit.id_unit;


create or replace view offer_item_details as 
    select offer_item.*, offer.priority, offer_type.description as offer_type_description from offer_item 
        join offer on offer_item.id_offer = offer.id_offer
        join offer_type on offer_item.id_offer_type = offer_type.id_offer_type;

-- Select current_offers à une date donnée: 
CREATE OR REPLACE VIEW current_offers as
    select * from account_offer where purchase_date<=current_timestamp and  expiration_date>=current_timestamp;

CREATE OR REPLACE VIEW current_offers_initial_value as
    select current_offers.id_account,current_offers.id_account_offer, offer_item.*, current_offers.purchase_date,current_offers.expiration_date  
        from  current_offers 
        join offer_item_details as offer_item on  offer_item.id_offer = current_offers.id_offer;

CREATE OR REPLACE VIEW consumption_per_account_offer as 
    select id_account_offer, id_offer_type,sum(value) as value, unit from consumption group by id_account_offer, id_offer_type, unit;

CREATE OR REPLACE VIEW current_offers_remaining_value as
    select current_offers_initial_value.id_account ,
            current_offers_initial_value.id_account_offer ,
            current_offers_initial_value.id_offer  ,
            current_offers_initial_value.id_offer_type  ,
            current_offers_initial_value.value  - coalesce(consumption_per_account_offer.value, 0) as value,
            current_offers_initial_value.unit   ,
            current_offers_initial_value.priority 
     from current_offers_initial_value
        left join consumption_per_account_offer 
        on consumption_per_account_offer.id_account_offer = current_offers_initial_value.id_account_offer
        and consumption_per_account_offer.id_offer_type = current_offers_initial_value.id_offer_type;

create or replace view current_offers_remaining_value_details as
    select current_offers_remaining_value.*, offer.offer_name, account_offer.purchase_date, account_offer.expiration_date, offer_type.description as offer_type_description from current_offers_remaining_value 
    join offer on current_offers_remaining_value.id_offer = offer.id_offer
    join account_offer on current_offers_remaining_value.id_account_offer = account_offer.id_account_offer
    join offer_type on offer_type.id_offer_type = current_offers_remaining_value.id_offer_type;


CREATE OR REPLACE View CurrentPricing as
    select Pricing.* from Pricing join (select max(last_update) as last_update from pricing) as t on Pricing.last_update = t.last_update;

CREATE VIEW v_call_month_year AS 
(SELECT c.*,month,year FROM Call c JOIN (SELECT (EXTRACT(YEAR FROM date)) AS year, id_call FROM Call) call_year ON call_year.id_call = c.id_call 
JOIN (SELECT (EXTRACT(MONTH FROM date)) AS month , id_call FROM Call) call_month ON call_month.id_call = c.id_call)
;
CREATE OR REPLACE View V_AccoutBalance as 
    select account_balance.id_account_balance, account_balance.id_account, account_balance.account_balance + coalesce(tmp.amount, 0) as account_balance from (
     select id_account_balance, sum(amount) as amount from account_balance_transaction group by id_account_balance
    ) as tmp
     right join account_balance on account_balance.id_account_balance = tmp.id_account_balance;



CREATE OR REPLACE View V_mobile_money_transaction as 
    select id_mobile_money_transaction, id_mobile_money_account, 
        CASE 
            WHEN id_mobile_money_transaction_type = 1 THEN amount
            WHEN id_mobile_money_transaction_type = 2 THEN -1*amount
            ELSE null 
        END as amount 
        from Mobile_money_transaction where status = 1;

CREATE OR REPLACE View V_Mobile_money_account as 
    select mobile_money_account.id_mobile_money_account, mobile_money_account.id_account, mobile_money_account.secret_code, mobile_money_account.account_balance + coalesce(tmp.amount, 0) as account_balance from (
     select id_mobile_money_account, sum(amount) as amount from V_mobile_money_transaction group by id_mobile_money_account
    ) as tmp
     right join mobile_money_account on mobile_money_account.id_mobile_money_account = tmp.id_mobile_money_account;
