
insert into operator(id_operator, prefix, operator_name) values (1, '034', 'Telma');
insert into operator(id_operator, prefix, operator_name) values (2, '032', 'Orange');
insert into operator(id_operator, prefix, operator_name) values (3, '033', 'Airtel');
insert into operator(id_operator, prefix, operator_name) values (4, '039', 'Bip');

insert into informations(id_operator, group_name, balance,prefix) values (1, 'Telma Group', '100 000 000 MGA','034');


insert into admin(id_admin, name, birth_date, username, password) values (1, 'Andrianasolo lala Kevin', '2001-09-18', 'KevinAndrianasolo', md5('123456'));
insert into admin(id_admin, name, birth_date, username, password) values (2, 'Ryan Andriamahery', '2001-09-17', 'afryan', md5('123456'));

INSERT INTO mobile_money_transaction_type VALUES(1,'Depot');
INSERT INTO mobile_money_transaction_type VALUES(2,'Retrait');

insert into offer_type(id_offer_type, description) values (1, 'Credit');
insert into offer_type(id_offer_type, description) values (2, 'Appel');
insert into offer_type(id_offer_type, description) values (3, 'Message');
insert into offer_type(id_offer_type, description) values (4, 'Internet');
insert into offer_type(id_offer_type, description) values (5, 'Facebook');
insert into offer_type(id_offer_type, description) values (6, 'Instagram');
insert into offer_type(id_offer_type, description) values (7, 'Duree appel');

insert into unit(id_unit, name) values (1, 'Ariary');
insert into unit(id_unit, name) values (2, 's');
insert into unit(id_unit, name) values (3, 'Message');
insert into unit(id_unit, name) values (4, 'Mo');


insert into offer_type_unit( id_offer_type_unit, id_offer_type, id_unit) values (1, 1, 1);
insert into offer_type_unit( id_offer_type_unit, id_offer_type, id_unit) values (2, 2, 1);
insert into offer_type_unit( id_offer_type_unit, id_offer_type, id_unit) values (3, 3, 3);
insert into offer_type_unit( id_offer_type_unit, id_offer_type, id_unit) values (4, 4, 4);
insert into offer_type_unit( id_offer_type_unit, id_offer_type, id_unit) values (5, 5, 4);
insert into offer_type_unit( id_offer_type_unit, id_offer_type, id_unit) values (6, 6, 4);
insert into offer_type_unit( id_offer_type_unit, id_offer_type, id_unit) values (7, 7, 2);


insert into pricing( call_cost_faf, call_cost_operator, call_cost_other_operator, call_cost_international, message_cost, internet_cost) values (0.5, 1, 3, 5, 100, 10);

-- Tous les appels d'un mois et d'une année :
SELECT * FROM v_call_month_year WHERE id_account = 1 AND month = 1 AND year = 2021;

-- Tous les appels de mon opérateur :
SELECT * FROM v_call_month_year WHERE id_account = 1 AND month = 1 AND year = 2021 AND recipient_phone_number LIKE CONCAT((SELECT prefix FROM informations),'%') ;

-- Tous les appels internationaux :
SELECT * FROM v_call_month_year WHERE id_account = 1 AND month = 1 AND year = 2021 AND is_international = true;

-- Tous les appels d'autres opérateurs :
SELECT * FROM v_call_month_year WHERE id_account = 1 AND month = 1 AND year = 2021 AND is_international = false AND recipient_phone_number NOT LIKE CONCAT((SELECT prefix FROM informations),'%') ;

-- Liste des transactions par idAccount : 
SELECT m_transaction.*,m_account.id_account FROM mobile_money_transaction m_transaction 
JOIN mobile_money_account m_account ON m_account.id_mobile_money_account = m_transaction.id_mobile_money_account
 WHERE m_account.id_account = 1;

-- Données de transactions pour test validation de transaction
insert into
    mobile_money_transaction(
        id_mobile_money_transaction,
        id_mobile_money_account,
        id_mobile_money_transaction_type,
        amount
    )
values
    (1, 1, 1, 100000);

insert into
    mobile_money_transaction(
        id_mobile_money_transaction,
        id_mobile_money_account,
        id_mobile_money_transaction_type,
        amount
    )
values
    (2, 1, 2, 50000);

insert into
    mobile_money_transaction(
        id_mobile_money_transaction,
        id_mobile_money_account,
        id_mobile_money_transaction_type,
        amount
    )
values
    (3, 2, 1, 200000);
    