show dbs;
use phone_service;

db.createCollection('pricing');
db.pricing.insert({
    "call_cost_faf": {
        "value" : 0.5 ,
        "unity" : "s"
    },
    "call_cost_operator":{
        "value" : 1,
        "unity" : "s"
    },
    "call_cost_other_operator" : {
        "value" : 3,
        "unity" : "s"
    },
    "call_cost_international": {
        "value" : 10,
        "unity" : "s"
    },
    "free_message_per_day" : 1,
    "message_cost" : {
        "value" : 100,
        "character_length" : 256
    },
    "internet_cost" : {
        "value" : 50,
        "unity" : "Mo"
    } ,
    "last_update": 0
});

db.createCollection('offer');
db.offer.createIndex({ 'id_offer' : 1 }, { unique: true });
db.offer.insert({
    "id_offer" : "1",
    "offer_name" : "Stay In",
    "cost" : 500,
    "validity" : {
        "value" :3,
        "unity" : "jour"
    },
    "priority" : 2, 
    "call" : {
        "value" : 15,
        "unity" : "min"
    },
    "internet" : {
        "value" : 1000,
        "unity" : "Mo"
    },
    "message" : {
        "value" : 20
    },
    "allowed_website" : [
        { "name" :"www.facebook.com"},
        { "name" :"www.instagram.com"}
    ],
    "frequency" : {
        "occurrence" : 2,
        "periodicity" : "week"
    },"pricing" : {
        "value" : 0,
        "call_cost_faf": {
            "value" : 0.5 ,
            "unity" : "s"
        },
        "call_cost_operator":{
            "value" : 1,
            "unity" : "s"
        },
        "call_cost_other_operator" : {
            "value" : 1,
            "unity" : "s"
        },
        "call_cost_international": {
            "value" : 5,
            "unity" : "s"
        },
        "message_cost" : {
            "value" : 100,
            "character_length" : 256
        },
        "internet_cost" : {
            "value" : 40,
            "unity" : "Mo"
        }
    }
});

db.offer.insert({
    "id_offer" : "2",
    "offer_name" : "Yellow 100",
    "cost" : 100,
    "validity" : {
        "value" :1,
        "unity" : "jour"
    },
    "priority" : 0, 
    "call" : {
        "value" : 0,
        "unity" : "min"
    },
    "internet" : {
        "value" : 20,
        "unity" : "Mo"
    },
    "message" : {
        "value" : 20
    },
    "frequency" : {
        "occurrence" : 10,
        "periodicity" : "day"
    },"pricing" : {
        "value" : 0,
        "call_cost_faf": {
            "value" : 0.5 ,
            "unity" : "s"
        },
        "call_cost_operator":{
            "value" : 1,
            "unity" : "s"
        },
        "call_cost_other_operator" : {
            "value" : 1,
            "unity" : "s"
        },
        "call_cost_international": {
            "value" : 5,
            "unity" : "s"
        },
        "message_cost" : {
            "value" : 100,
            "character_length" : 256
        },
        "internet_cost" : {
            "value" : 40,
            "unity" : "Mo"
        }
    }
});

db.offer.insert({
    "id_offer" : "3",
    "offer_name" : "One night",
    "cost" : 100,
    "validity" : {
        "value" :1,
        "unity" : "jour"
    },
    "priority" : 1, 
    "call" : {
        "value" : 0,
        "unity" : "min"
    },
    "internet" : {
        "value" : 40,
        "unity" : "Mo"
    },
    "message" : {
        "value" : 0
    },
    "frequency" : {
        "occurrence" : 4,
        "periodicity" : "day"
    },"pricing" : {
        "value" : 0,
        "call_cost_faf": {
            "value" : 0.5 ,
            "unity" : "s"
        },
        "call_cost_operator":{
            "value" : 1,
            "unity" : "s"
        },
        "call_cost_other_operator" : {
            "value" : 1,
            "unity" : "s"
        },
        "call_cost_international": {
            "value" : 5,
            "unity" : "s"
        },
        "message_cost" : {
            "value" : 100,
            "character_length" : 256
        },
        "internet_cost" : {
            "value" : 40,
            "unity" : "Mo"
        }
    }
});

db.offer.insert({
    "id_offer" : "4",
    "offer_name" : "Telma Mora",
    "cost" : 500,
    "validity" : {
        "value" :1,
        "unity" : "jour"
    },
    "priority" : 0, 
    "call" : {
        "value" : 15,
        "unity" : "min"
    },
    "internet" : {
        "value" : 20,
        "unity" : "Mo"
    },
    "message" : {
        "value" : 20
    },"pricing" : {
        "value" : 0,
        "call_cost_faf": {
            "value" : 0.5 ,
            "unity" : "s"
        },
        "call_cost_operator":{
            "value" : 1,
            "unity" : "s"
        },
        "call_cost_other_operator" : {
            "value" : 1,
            "unity" : "s"
        },
        "call_cost_international": {
            "value" : 5,
            "unity" : "s"
        },
        "message_cost" : {
            "value" : 100,
            "character_length" : 256
        },
        "internet_cost" : {
            "value" : 40,
            "unity" : "Mo"
        }
    },
    "frequency" : {
        "occurrence" : 2,
        "periodicity" : "day"
    }
});

db.offer.insert({
    "id_offer" : "5",
    "offer_name" : "Telma Yes",
    "cost" : 1000,
    "validity" : {
        "value" :1,
        "unity" : "jour"
    },
    "priority" : 0, 
    "pricing" : {
        "value" : 1000,
        "call_cost_faf": {
            "value" : 0.5 ,
            "unity" : "s"
        },
        "call_cost_operator":{
            "value" : 1,
            "unity" : "s"
        },
        "call_cost_other_operator" : {
            "value" : 1,
            "unity" : "s"
        },
        "call_cost_international": {
            "value" : 5,
            "unity" : "s"
        },
        "message_cost" : {
            "value" : 100,
            "character_length" : 256
        },
        "internet_cost" : {
            "value" : 40,
            "unity" : "Mo"
        }
    },
    "call" : {
        "value" : 0,
        "unity" : "min"
    },
    "internet" : {
        "value" : 0,
        "unity" : "Mo"
    },
    "message" : {
        "value" : 0
    },
    "frequency" : {
        "occurrence" : 1,
        "periodicity" : "day"
    }
});

db.pricing.find().sort({last_update : -1}).limit(1);

 db.offer.find().pretty();