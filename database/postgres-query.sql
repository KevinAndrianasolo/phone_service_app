-- Tous les appels d'un mois et d'une année :
SELECT * FROM v_call_month_year WHERE id_account = 1 AND month = 1 AND year = 2021

-- Tous les appels de mon opérateur :
SELECT * FROM v_call_month_year WHERE id_account = 1 AND month = 1 AND year = 2021 AND recipient_phone_number LIKE CONCAT((SELECT prefix FROM informations),'%') ;

-- Tous les appels internationaux :
SELECT * FROM v_call_month_year WHERE id_account = 1 AND month = 1 AND year = 2021 AND is_international = true;

-- Tous les appels d'autres opérateurs :
SELECT * FROM v_call_month_year WHERE id_account = 1 AND month = 1 AND year = 2021 AND is_international = false AND recipient_phone_number NOT LIKE CONCAT((SELECT prefix FROM informations),'%') ;

-- Liste des transactions par idAccount : 
SELECT m_transaction.*,m_account.id_account FROM mobile_money_transaction m_transaction 
JOIN mobile_money_account m_account ON m_account.id_mobile_money_account = m_transaction.id_mobile_money_account
 WHERE m_account.id_account = 1;