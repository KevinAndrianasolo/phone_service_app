
insert into operator values (1,'034', 'Telma');
insert into operator values (2,'032', 'Orange');
insert into operator values (3,'033', 'Airtel');
insert into operator values (4,'039', 'Bip');

insert into informations values (1,1, 'Telma Group', '100 000 000 MGA','034');

insert into account values (1,'Andrianasolo lala Kevin', '2001-09-18', '101251100220', '0345356471', md5('123456'));
insert into account_balance values (1,1, 0.0);
insert into mobile_money_account values (1,1, 0, md5('0000'));

insert into account values (2,'Andriamahery Fihariantsoa Ryan','2021-09-17','101251011190','03494112390',md5('123456'));
insert into account_balance values (2,2, 0.0);
insert into mobile_money_account values (2,2, 0, md5('0000'));

INSERT INTO mobile_money_transaction_type VALUES(1,'Depot');
INSERT INTO mobile_money_transaction_type VALUES(2,'Retrait');

insert into admin values (1,'Andrianasolo lala Kevin', '2001-09-18', 'KevinAndrianasolo', md5('123456'));
insert into admin values (2,'Andriamahery Fihariantsoa Ryan', '2001-09-17', 'afryan', md5('123456'));

insert into offer_type values (1,'Credit');
insert into offer_type values (2,'Appel');
insert into offer_type values (3,'Message');
insert into offer_type values (4,'Internet');
insert into offer_type values (5,'Facebook');
insert into offer_type values (6,'Instagram');

insert into unit values (1,'Ariary');
insert into unit values (2,'s');
insert into unit values (3,'Message');
insert into unit values (4,'Mo');

insert into offer_type_unit values (1,1, 1);
insert into offer_type_unit values (2,2, 1);
insert into offer_type_unit values (3,3, 3);
insert into offer_type_unit values (4,4, 4);
insert into offer_type_unit values (5,5, 4);
insert into offer_type_unit values (6,6, 4);


insert into offer(offer_name, cost, validity, validity_unit,priority) values ('Telma Mora 1000', 1000, 1 , 'day', 0);
insert into offer_item(id_offer, id_offer_type, value, unit) values (1, 1, 1000, 'Ariary');
insert into offer_item(id_offer, id_offer_type, value, unit) values (1, 2, 500, 'Ariary');
insert into offer_item(id_offer, id_offer_type, value, unit) values (1, 3, 20, 'Message');
insert into offer_item(id_offer, id_offer_type, value, unit) values (1, 4, 20, 'Mo');


insert into offer(offer_name, cost, validity, validity_unit,priority) values ('Faceboobaka', 500, 3 , 'day', 5);
insert into offer_item(id_offer, id_offer_type, value, unit) values (2, 5, 1000, 'Mo');

insert into offer(offer_name, cost, validity, validity_unit,priority) values ('Yellow 100', 100, 1 , 'day', 3);
insert into offer_item(id_offer, id_offer_type, value, unit) values (3, 3, 20, 'Message');
insert into offer_item(id_offer, id_offer_type, value, unit) values (3, 4, 20, 'Mo');

insert into account_offer(id_account, id_offer, purchase_date, expiration_date) values (1, 1, CURRENT_TIMESTAMP , CURRENT_TIMESTAMP + '1 day');
insert into account_offer(id_account, id_offer, purchase_date, expiration_date) values (1, 2, CURRENT_TIMESTAMP , CURRENT_TIMESTAMP + '3 day');
insert into account_offer(id_account, id_offer, purchase_date, expiration_date) values (1, 3, CURRENT_TIMESTAMP , CURRENT_TIMESTAMP + '1 day');


insert into consumption(id_account, id_account_offer, id_offer_type, value, unit, consumption_date) values (1, 1, 4, 15, 'Mo', CURRENT_TIMESTAMP);
insert into consumption(id_account, id_account_offer, id_offer_type, value, unit, consumption_date) values (1, 1, 4, 5, 'Mo', CURRENT_TIMESTAMP);
insert into consumption(id_account, id_account_offer, id_offer_type, value, unit, consumption_date) values (1, 2, 5, 400, 'Mo', CURRENT_TIMESTAMP);

insert into pricing( call_cost_faf, call_cost_operator, call_cost_other_operator, call_cost_international, message_cost, internet_cost) values (0.5, 1, 3, 5, 100, 10);
insert into offer_pricing(id_offer, call_cost_faf, call_cost_operator, call_cost_other_operator, call_cost_international, message_cost, internet_cost) values (1, 0.5, 1, 3, 5, 100, 10);

insert into call values (1,1,'0341298734',300,'2021-01-01 10:00:00',false);
insert into call values (2,1,'0331298734',600,'2021-01-10 11:30:00',false);
insert into call values (3,1,'+3311298734',900,'2021-01-02 10:00:00',true);
insert into call values (4,1,'0340122234',120,'2021-01-02 10:00:00',false);
insert into call values (5,2,'0349812310',900,'2021-02-03 12:01:00',false);
insert into call values (6,2,'0341293734',600,'2021-02-01 10:00:00',false);
insert into call values (7,2,'0321294734',550,'2021-03-10 11:30:00',false);
insert into call values (8,1,'+3311211734',500,'2021-04-02 10:00:00',true);
insert into call values (9,2,'0344112224',300,'2021-05-02 10:00:00',false);
insert into call values (10,2,'0349812319',1000,'2021-11-03 12:01:00',false);
insert into call values (11,1,'0321128734',300,'2021-12-11 10:00:00',false);
insert into call values (12,1,'0321898734',600,'2021-06-10 11:30:00',false);
insert into call values (13,1,'+3311290334',900,'2021-07-09 10:00:00',true);
insert into call values (14,1,'0320122834',120,'2021-08-08 10:00:00',false);
insert into call values (15,2,'0349819010',30,'2021-09-07 12:01:00',false);

