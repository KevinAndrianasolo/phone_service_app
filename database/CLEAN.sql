DROP SCHEMA public cascade;

CREATE SCHEMA public
    AUTHORIZATION rrqjwkdxxupeqp;

COMMENT ON SCHEMA public
    IS 'standard public schema';

GRANT ALL ON SCHEMA public TO rrqjwkdxxupeqp;

GRANT ALL ON SCHEMA public TO PUBLIC;