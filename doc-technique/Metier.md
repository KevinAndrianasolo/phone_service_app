## Class Offer(id_offer, offer_name, cost, validity, call, internet, message, allowed_website, frequency, availability)
- Methodes : 

    . static FindAll() => List&lt;Offer&gt;     <span style="color:green"> /Offers</span>

    . static FindById(id_offer) => Offer     <span style="color:green"> /Offers/{id}</span>

    . Save() => void     <span style="color:green"> /Offers/Add</span>


## Class Operator(id_operator, prefix, operator_name)
- Methodes : 

    . IsDefaultOperator() => Boolean 

    . static FindAll() => List&lt;Operator&gt; 
    <span style="color:green"> /Operators</span>

## Class PhoneNumber (phone_number)
- Methodes : 

    . IsFAF(id_account) => Boolean

    . GetOperator() => Operator // RegExp avec son prefixe

## Class AccountOffer(id_account_offer, id_account, id_offer, remaining_call, remaining_internet, remaining_message, purchase_date, expiration_date)
- Methodes:

    . Update() => void
 <span style="color:green"> /AccountOffer/Update</span>

    . Save() => void
 <span style="color:green"> /AccountOffer/Add</span>

    . SimulateCall(duration) : Double

        Offer offer = Offer.GetOffer(this.id_offer);
        Double remaining_duration = duration;
        if(duration >= this.remaining_call){
            remaining_duration = duration - this.remaining_call;
            this.remaining_call = 0;
        }
        else{
            remaining_duration = 0;
            this.remaining_call = this.remaining_call - duration;
        }
        return remaining_duration;

    . WithdrawMessage(message_count) : int

        Offer offer = Offer.GetOffer(this.id_offer);
        Double remaining = message_count;
        // We should withdraw also the internet usage is message and internet are linked
        if(message_count >= this.remaining_message){
            remaining = message_count - this.remaining_message;
            this.remaining_message = 0;
        }
        else{
            remaining = 0;
            this.remaining_message = this.remaining_message - message_count;
        }
        return remaining;

    . WithdrawInternetUsage(consommation, website) : double

        Offer offer = Offer.GetOffer(this.id_offer);
        Double remaining = consommation;

        if(offer.allowed_website == '*' || offer.allowed_website.contains(website)){
            // We should withdraw also the message usage is message and internet are linked
            if(consommation >= this.remaining_internet){
                remaining = consommation - this.remaining_internet;
                this.remaining_internet = 0;
            }
            else{
                remaining = 0;
                this.remaining_internet = this.remaining_internet - consommation;
            }
        }
        return remaining;


## Class Account (id_account, phone_number, password)
- Methodes : 

    . GetMobileMoneyAccount() => MobileMoneyAccount 

    . static FindById(idAccount) => Account
 <span style="color:green"> /Accounts/{id}</span>
 
    . GetCurrentOffers() => List&lt;AccountOffer&gt; // Par ordre de priorité
 <span style="color:green"> /Accounts/{id}/CurrentOffers</span>

    . GetAccountBalance() => AccountBalance

    . purchaseOffer(id_offer, is_with_mobile_money,secret_code == null) => void
 <span style="color:green"> /Purchase</span>

        Account account = Account.FindById(this.id_account);
        Offer offer = Offer.FindById(id_offer);
        Double remaining_call = offer.call.value != null ? offer.call.value : 0;
        Double remaining_internet = offer.internet.value != null ? offer.internet.value : 0;
        Double remaining_message = offer.message.value != null ? offer.message.value : 0;
        AccountOffer account_offer = new AccountOffer(-1, this.id_account, remaining_call, remaining_internet, remaining_message);
        account_offer.Save();

        if(is_with_mobile_money){
            MobileMoneyAccount mobile_money_account = account.GetMobileMoneyAccount();
            if(secret_code != mobile_money_account.GetSecret_Code) throw new Exception ("Le code que vous avez entré ne correspond pas à votre code secret")
            double overflow = mobile_money_account.Withdraw(offer.cost);
            if(overflow != 0) throw new Exception("Le solde de votre compte Mobile money est insuffisant. Votre solde actuel est de "+ mobile_money_account.account_balance+" Ariary. Montant manquant :+"overflow+" Ariary");
            mobile_money_account.Update();
        }
        else{
            AccountBalance account_balance = account.GetAccountBalance();
            double overflow = account_balance.Withdraw(offer.cost);
            if(overflow != 0) throw new Exception("Le solde de votre compte principale est insuffisant. Votre solde actuel est de "+ account_balance.account_balance+" Ariary. Montant manquant :+"overflow+" Ariary");
            account_balance.Update();
        }



## Class DefaultCost(call_cost_faf, call_cost_operator, call_cost_other_operator, call_cost_international, free_message_per_day, message_cost, internet_cost)
- Methodes:

    . EvaluateCall(duration, is_faf, is_default_operator, is_international) : Double 

        Double cost = 0;
        if(is_faf) cost = call_cost_faf * duration;
        else{
            if(is_default_operator) cost = call_cost_operator * duration;
            else cost = call_cost_other_operator * duration;
        }
        if(call_cost_international) = call_cost_international * duration;
        return cost;
    
    . EvaluateMessage(character_length) : Double

        int nb_message = Math.Ceil(character_length/256);
        Double cost = this.message_cost * nb_message;
        return cost;

    . EvaluateInternetUsage(remaining_conso) : Double

        Double cost = this.internet_cost * remaining_conso;
        return cost;
    

## Class AccountBalance(id_account_balance,id_account,account_balance)
- Methodes :

    . static GetDefaultCost() => DefaultCost
 
    . Deposit(amount) : int
<span style="color:green"> /Account/{id}/Deposit</span>

    . Withdraw(amount) : Double
<span style="color:green"> /Account/{id}/WithDraw</span>

        Double overflow = 0;
        if(amount >= this.account_balance) {
            overflow = amount - this.account_balance;
            this.acccount_balance = 0;
        }
        else this.account_balance = this.account_balance - amount;
        return overflow; 

    . Update()
    

## Class Call (id_call, id_account, recipient_phone_number, duration, date, is_international)
- Methode : 

    . Simulate() => void

        DefaultCost defaultCost = DefaultCost.GetDefaultCost();
        PhoneNumber recipient_phone = new PhoneNumber(this.recipient_phone_number);
        Boolean is_faf = recipient_phone.IsFaF(id_account);
        Operator recipient_operator = recipient_phone.GetOperator(); 
        Boolean is_default_operator = recipient_operator.IsDefaultOperator();
        Account account = Account.FindById(this.id_account);
        AccountBalance account_balance = account.GetAccountBalance();
        List<AccountOffer> current_offers = account.GetCurrentOffers();
        Double remaining_duration = this.duration;

        FOREACH account_offer IN current_offers {
            remaining_duration = account_offer.SimulateCall(remaining_duration);
            account_offer.Update();
            if (remaining_duration == 0) break;
        }

        if (remaining_duration != 0) { // On débite mtn avec le solde principale
            Double cost = defaultCost.EvaluateCall(remaining_duration, is_faf, is_default_operator, is_international);
            Double overflow = account_balance.Withdraw(cost); // Retirer
            account_balance.Update();
            if (overflow != 0) {
                throw new Exception("Solde insuffisant. Manquant : "+ overflow +" Ariary");
            }
        }
        COMMIT()

## Class Message (id_message, id_account, recipient_phone_number, message_body, date)

- Methode :

    . Simulate() => void
        
        int character_lenght = message_body.length;

        DefaultCost defaultCost = DefaultCost.GetDefaultCost();
        PhoneNumber recipient_phone = new PhoneNumber(this.recipient_phone_number);
        Boolean is_faf = recipient_phone.IsFaF();
        Operator recipient_operator = recipient_phone.GetOperator(); 
        Boolean is_default_operator = recipient_operator.IsDefaultOperator();
        Account account = Account.FindById(this.id_account);
        AccountBalance account_balance = account.GetAccountBalance();
        List<AccountOffer> current_offers = account.GetCurrentOffers();

        int message_count = 1; // Un message à décrementer
        FOREACH account_offer IN current_offers {
            message_count = account_offer.WithdrawMessage(message_count);
            account_offer.Update();
            if (message_count == 0) break;
        }

        if (message_count != 0) { // On débite mtn avec le solde principale
            Double cost = defaultCost.EvaluateMessage(character_lenght);
            Double overflow = account_balance.Withdraw(cost); // Retirer
            account_balance.Update();
            if (overflow != 0) {
                throw new Exception("Solde insuffisant. Manquant : "+ overflow +" Ariary");
            }
        }
        COMMIT()

## Class InternetUsage (id_internet_usage, id_account, website, consommation, date)
- Methode : 

    . Simulate() => void

        DefaultCost defaultCost = DefaultCost.GetDefaultCost();
        Account account = Account.FindById(this.id_account);
        AccountBalance account_balance = account.GetAccountBalance();
        List<AccountOffer> current_offers = account.GetCurrentOffers();
        Double remaining_conso = this.consommation;

        FOREACH account_offer IN current_offers {
            remaining_conso = account_offer.WithdrawInternetUsage(remaining_conso);
            account_offer.Update();
            if (remaining_conso == 0) break;
        }

        if (remaining_conso != 0) { // On débite mtn avec le solde principale
            Double cost = defaultCost.EvaluateInternetUsage(remaining_conso);
            Double overflow = account_balance.Withdraw(cost); // Retirer
            account_balance.Update();
            if (overflow != 0) {
                throw new Exception("Solde insuffisant. Manquant : "+ overflow +" Ariary");
            }
        }
        COMMIT()

## Class MobileMoneyAccount(id_mobile_money_account, id_account, account_balance)
- Methodes :

    . Deposit(amount) => void  <span style="color:green"> /MobileMoney/{idAccount}/Deposit</span>

    . FindByIdAccount(idAccount) => MobileMoneyAccount <span style="color:green"> /MobileMoney/{idAccount}</span>

    . Withdraw(amount) : Double <span style="color:green"> /MobileMoney/{idAccount}/WithDraw</span>

        Double overflow = 0;
        if(amount >= this.account_balance) {
            overflow = amount - this.account_balance;
            this.acccount_balance = 0;
        }
        else this.account_balance = this.account_balance - amount;
        return overflow; 

    . Update() => void
<span style="color:green"> /MobileMoney/{idAccount}/Update</span>
## Class TransactionType
- Attributes :

    . static int DEPOT = 1;

    . static int RETRAIT = 2;


## Class MobileMoneyTransaction(id_mobile_money_transaction, id_mobile_money_account, transaction_type, amount, transaction_date, transactionStatus)
- Methodes:

    . Save() => int

    . Update () => int

    . updateStatus(status) => void // Set attribute transactionStatus to status

        this.transactionStatus = status;
        this.Update();
        MobileMoneyAccount mobile_money_account = MobileMoneyAccount.FindById(this.id_mobile_money_account);
        if(transaction_type == TransactionType.RETRAIT){
            mobile_money_account.Withdraw(this.amount);
        }
        else if(transaction_type == TransactionType.DEPOT){
            mobile_money_account.Deposit(this.amount);
        }
        else {throw new Exception("Le type de la transaction est invalide.");}
        mobile_money_account.Update();


## Class CallStats(month, year, count, sum_call_duration)

## Class MessageStats(month , year, count, sum_character_length)

## Class InternetUsageStats(month, year, sum_consommation)

## Class Stats
- Methodes :

    . GetGlobalCallStats(year) => List&lt;CallStats&gt; // Toutes les appels passées Nombre et Durée d'appel Par Mois ( Janvier , Fev, Mars .. , Décembre de l'année donnée)

    . GetInternationalCallStats(year) => List&lt;CallStats&gt; // Appel à l'internationale

    . GetOtherOperatorCallStats(year) => List&lt;CallStats&gt; // Autres opérateurs

    . GetHomeCallStats(year) => List&lt;CallStats&gt; // Du même opérateur que l'application

    . GetMessageStats(year) => List&lt;MessageStats&gt;

    . GetInternetUsageStats(year) => List&lt;InternetUsageStats&gt;

## Class Call(id_call, id_account, recipient_phone_number, duration, date, is_international)

## Class History
- Methode :

    . GetCallHistory() => List<Call> // Order By Date

## Class Authentification
- Methode : 

    . static Login(phone_number, password) => string (Token)

        Insert an account_token with the id_account , a generated token and an expiration_date, IF LOGGED

    . static Logout(token) => void

        Delete account_token in the base with the token below

    . static IsValidToken(token) => Boolean

        Check if the given token is valid, or already expired or does not exists. 
    
    . static GetIdAccount(token) => int (id_account)

        if(id_account == -1) throw new Exception("Session is expired.");
